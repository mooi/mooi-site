'use strict';
// Base
var gulp = require('gulp');
var concat = require('gulp-concat');
var replace = require('gulp-replace-task');
var rename = require('gulp-rename');
var q = require('q');
// JS
var uglify = require('gulp-uglify');
// Nomes de arquivos e pastas, para modificar facilmente
var config = {
    // Entrada

    jsInFolder  : [
        '!client/source/config/config.provider.js', // Gerado automaticamente pelo gulp do arquivo config.raw.js
        '!client/source/**/*.spec.js',              // Arquivos de teste
        'client/source/**/*.js'
    ],
    jsInOrder   : [
        '!client/source/config/config.raw.js',  // Gera o arquivo config.provider.js
        'client/source/mooiweb.module.js',      // Módulo principal, chama todas as dependências
        'client/source/core/core.module.js',    // Contém as libs
        'client/source/**/*.module.js',         // O resto dos módulos
        'client/source/**/*.provider.js',       // Providers
        'client/source/**/*.service.js',        // Serviços
        'client/source/**/*.config.js',         // Arquivos de configuração que precisam dos serviços para o método config
        'client/source/**/*.run.js',            // Arquivos de configuração na estapa run
        'client/source/**/*.filter.js',         // Filtros
        'client/source/**/*.directive.js',      // Diretivas
        'client/source/layout/layout.route.js', // Rota principal
        'client/source/**/*.route.js',          // Restante das rotas
        'client/source/**/*.controller.js',     // Controladores
        'client/source/**/!(*.spec).js'         // Eventual arquivo js que não seja teste
    ],
    // Saída
    jsOutFolder : 'client/js/',
    jsMinOut    : 'mooi.min.js',
    jsOut       : 'mooi.js'
};

gulp.task('jsMin', jsMin);
gulp.task('observeJS', observeJS);
gulp.task('jsFull', jsFull);
gulp.task('js', js);

function setEnvironment(env) {
    return gulp.src('client/source/config/config.raw.js')
        .pipe(replace({
            patterns : [
                {
                    match       : 'environment',
                    replacement : env
                }
            ]
        }))
        .pipe(rename("config.provider.js"))
        .pipe(gulp.dest('client/source/config'));
}

function jsMin() {
    var deferred = q.defer();
    setEnvironment('production').on('end', minimize);
    function minimize() {
        gulp.src(config.jsInOrder)
            .pipe(concat(config.jsMinOut)) // Concatena todos os arquivos .jsFull em um só antes de minificar
            .pipe(uglify()) // Minifica e simplifica nomes de variáveis não-globais
            .pipe(gulp.dest(config.jsOutFolder))
            .on('end', function(){
                deferred.resolve();
            });
    }
    return deferred.promise;
}

function jsFull() {
    var deferred = q.defer();
    setEnvironment('development').on('end', concatenate);
    function concatenate() {
        gulp.src(config.jsInOrder)
            .pipe(concat(config.jsOut)) // Concatena todos os arquivos .jsFull em um só antes de minificar
            .pipe(gulp.dest(config.jsOutFolder))
            .on('end', function(){
                deferred.resolve();
            });
    }
    return deferred.promise;
}

function js() {
    return jsFull().then(function() {
        return jsMin();
    });
}

function observeJS() {
    gulp.watch(config.jsInFolder, ['js']);
    js();
}
