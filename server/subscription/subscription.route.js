(function() {
    'use strict';
    var express = require('express');
    var router = express.Router();
    var mongoose = require("mongoose");
    var Subscription = mongoose.model("Subscription");

    router.post('/subscribe', subscribe);

    router.post('/unsubscribe', unsubscribe);

    function subscribe(req, res, next) {
        var subsription = req.body;
        var email = subsription.email;
        var ret;
        var emailRegex = /^[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,63}$/;
        if(email === undefined || email === null || email.length < 6 || !emailRegex.test(email)) {
            ret = {
                success : false,
                data    : "Email inválido"
            };
            return res.send(ret);
        }
        Subscription.findOne({'email' : email}, function(err, subscription) {
            if(!!err) {
                throw(err);
            }
            if(!subscription) {
                // OK
                var newSubscription = new Subscription();
                newSubscription.email = email;
                newSubscription.save(function(err) {
                    if(!!err) {
                        ret = {
                            success : false,
                            data    : "Houve um erro no registro, tente novamente mais tarde"
                        };
                        return res.send(ret);
                    }
                    ret = {
                        success : true
                    };
                    return res.send(ret);
                });
            }
            else {
                ret = {
                    success : false,
                    data    : 'Email já cadastrado'
                };
                // Já existe, retornar um erro
                res.send(ret);
            }
        });
    }

    function unsubscribe(req, res, next) {
        //
    }

    module.exports = router;

})();