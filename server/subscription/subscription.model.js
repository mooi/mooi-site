"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var subscriptionSchema = new Schema({
    email: String,
    added    : {
        type    : Date,
        default : Date.now
    }
});

//subscriptionSchema.methods.confirm = function() {
//    return true;
//};

mongoose.model("Subscription", subscriptionSchema, "subscriptions");