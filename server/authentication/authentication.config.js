module.exports = {

    development : {
        "facebook" : {
            "clientID"      : "1529509513976185", // your App ID
            "clientSecret"  : "4b2bc0d4a5d57b61535841a57ca0a9f1", // your App Secret
            "callbackURL"   : "http://localhost/auth/facebook/callback"
        },

        "twitter" : {
            "consumerKey"       : "your-consumer-key-here",
            "consumerSecret"    : "your-client-secret-here",
            "callbackURL"       : "http://localhost:8080/auth/twitter/callback"
        },

        "google" : {
            "clientID"      : "your-secret-clientID-here",
            "clientSecret"  : "your-client-secret-here",
            "callbackURL"   : "http://localhost:8080/auth/google/callback"
        }
    },
    production : {
        "facebook" : {
            "clientID"      : "1470679093192561", // your App ID
            "clientSecret"  : "f1044b22bfa33e20c13b0259e0b2bed7", // your App Secret
            "callbackURL"   : "http://www.mooi.me/auth/facebook/callback"
        },

        "twitter" : {
            "consumerKey"       : "your-consumer-key-here",
            "consumerSecret"    : "your-client-secret-here",
            "callbackURL"       : "http://www.mooibrasil.com.br/auth/twitter/callback"
        },

        "google" : {
            "clientID"      : "your-secret-clientID-here",
            "clientSecret"  : "your-client-secret-here",
            "callbackURL"   : "http://www.mooibrasil.com.br/auth/google/callback"
        }
    }

};