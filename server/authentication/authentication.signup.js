var passport = require('passport');

module.exports = function(req, res, next) {
    //Autenticação
    var email = req.body.email;
    var password = req.body.password;
    var name = req.body.name;
    var gender = req.body.gender;
    var age = parseInt(req.body.age);
    var ret;

    // Verifica o email
    var emailRegex = /^[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,63}$/;
    if(email === undefined || email === null || email.length < 6 || !emailRegex.test(email)) {
        ret = {
            success : false,
            data    : "Email inválido"
        };
        return res.send(ret);
    }
    // Verifica a senha
    var passwordRegex = /^[a-zA-Z0-9,!_.-@#%*]+$/; // Letras, números e {,!_.-@#%*}
    if(password === undefined || password === null || password.length < 5 || !passwordRegex.test(password)) {
        ret = {
            success : false,
            data    : "Senha inválida"
        };
        return res.send(ret);
    }
    // Verifica nome
    var nameRegex = /[a-zA-Z \u00C0-\u024f]+$/; // Letras
    if(name === undefined || name === null || name.length < 3 || !nameRegex.test(name)) {
        ret = {
            success : false,
            data    : "Nome inválido"
        };
        return res.send(ret);
    }
    // Verifica gênero
    if(gender === undefined || gender === null || (gender !== 'Feminino' && gender !== 'Masculino'
        && gender !== 'Andrógino' && gender !== '-')) {
        ret = {
            success : false,
            data    : "Gênero inválido"
        };
        return res.send(ret);
    }
    // Verifica a idade
    if(age === undefined || age === null || isNaN(age) || age < 8 || age > 120) {
        ret = {
            success : false,
            data    : "Idade inválida"
        };
        return res.send(ret);
    }
    passport.authenticate("local-signup", function(err, user, info) {
        if(err) {
            return next(err); // will generate a 500 error
        }
        if(!user) {
            ret = {
                success : false,
                data    : info.message || "Falha na criação do usuário"
            };
            return res.send(ret);
        }

        req.logIn(user, function(err) {
            if(err) {
                return next(err); // 500?
            }
            var userObject = user.toObject();
            delete userObject.local.password;
            ret = {
                success : true,
                data    : userObject
            };
            return res.send(ret);
        });
    })(req, res, next);
};