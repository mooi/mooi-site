var passport = require('passport');

exports.authenticate = passport.authenticate("facebook", {scope : "email"});

exports.callback = passport.authenticate("facebook", {
    successRedirect : "/usuario/",
    failureRedirect : "/"
});

exports.error = function(err, req, res, next) {
    // Pra quando dá erro "FacebookTokenError: This authorization code has been used."
    // Agora, COMO isso acontece e por que caralhos isso resolve o problema está além do meu conhecimento atual, uma vez
    // que essa função não é chamada!
    // TODO: Entender essa merda.
    // https://github.com/jaredhanson/passport-facebook/issues/93
    // You could put your own behavior in here, fx: you could force auth again...
    // res.redirect("/auth/facebook/");
    if(err) {
        res.status(400);
        res.redirect("/home?facebookLogin=2");
    }
};