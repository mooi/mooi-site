var signup = require('./authentication.signup.js');
var localLogin = require('./authentication.localLogin.js');
var facebook = require('./authentication.facebook.js');
var security = require('./authentication.security.js');

var express = require('express');

var router = express.Router();

// --- Local --- //
router.post('/signup', signup);

router.post('/localLogin', localLogin);

// --- Local --- //

// --- Facebook --- //

router.get("/facebook", facebook.authenticate);

router.get("/facebook/callback", facebook.callback, facebook.error);

// --- Facebook --- //

router.post('/logout', security.auth, security.logout);

router.get('/isLoggedIn', security.isLoggedIn);

router.get('/userProfile', security.auth, security.userProfile);

router.get('/isNotAuthorized', security.notAuth, function(req, res, next) {
    return res.send({
        success : true
    });
});

module.exports = router;