"use strict";

// load all the things we need
var LocalStrategy = require("passport-local").Strategy;
var FacebookStrategy = require("passport-facebook").Strategy;
var mongoose = require("mongoose");

// load up the user model
var User = mongoose.model("User");

// load the auth variables
var env = process.env.NODE_ENV;
var config = require("./authentication.config")[env];

// expose this function to our app using module.exports
module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called "local"

    passport.use("local-signup", new LocalStrategy({
            passwordField     : "password",
            usernameField     : 'email',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, email, password, done) {
            // asynchronous
            // User.findOne wont fire unless data is sent back
            process.nextTick(function() {
                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login(errata: register) already exists
                User.findOne({'local.email' : email}, function(err, user) {
                    // if there are any errors, return the error
                    if(err) {
                        return done(err);
                    }
                    // check to see if theres already a user with that email
                    if(user) {
                        return done(null, false, {message : "Este email já está sendo usado"});
                    }
                    else {
                        // if there is no user with that email
                        // create the user
                        var newUser = new User();
                        // set the user"s local credentials
                        newUser.local.email = email;
                        newUser.local.password = newUser.generateHash(password);
                        newUser.local.name = req.body.name;
                        newUser.local.gender = req.body.gender;
                        newUser.local.age = req.body.age;

                        // save the user
                        newUser.save(function(err) {
                            if(err) {
                                return done(null, false, {message : "Houve um erro no registro, tente novamente mais tarde"});
                            }
                            //delete newUser.local.password;
                            return done(null, newUser);
                        });
                    }
                });
            });
        }));

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called "local"
    passport.use("local-login", new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField     : "email",
            passwordField     : "password",
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, email, password, done) { // callback with email and password from our form
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            User.findOne({"local.email" : email}, function(err, user) {
                // if there are any errors, return the error before anything else
                if(err) {
                    return done(err);
                }

                // if no user is found, return the message
                if(!user) {
                    return done(null, false, {message : "Usuário não existe"});
                }
                // if the user is found but the password is wrong
                if(!user.validPassword(password)) {
                    return done(null, false, {message : "Senha incorreta"});
                }
                // all is well, return successful user
                //delete user.local.password;
                return done(null, user);
            });
        }
    ));


    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({
            // pull in our app id and secret from our auth.js file
            clientID          : config.facebook.clientID,
            clientSecret      : config.facebook.clientSecret,
            callbackURL       : config.facebook.callbackURL,
            passReqToCallback : true

        },
        // facebook will send back the token and profile
        function(req, token, refreshToken, profile, done) {
            process.nextTick(function() {
                if(!req.user) {
                    // find the user in the database based on their facebook id
                    User.findOne({'facebook.id' : profile.id}, function(err, user) {
                        // if there is an error, stop everything and return that
                        // ie an error connecting to the database
                        if(err) {
                            return done(err);
                        }
                        // if the user is found, then log them in
                        if(user) {
                            if(user.visits === undefined) {
                                user.visits = 0;
                            }
                            user.visits += 1;
                            user.save(function(err) {
                                if(err) {
                                    throw err;
                                }
                                return done(null, user);
                            });
                        }
                        else {
                            // if there is no user found with that facebook id, create them
                            var newUser = new User();

                            // set all of the facebook information in our user model
                            newUser.visitas = 1;

                            newUser.facebook.id = profile.id; // set the users facebook id
                            newUser.facebook.token = token; // we will save the token that facebook provides to the user
                            newUser.facebook.name = profile.name.givenName; // look at the passport user profile to see
                                                                            // how names are returned
                            //newUser.facebook.sobrenome = profile.name.familyName;
                            if(profile.emails) {
                                newUser.facebook.email = profile.emails[0].value; // facebook can return multiple
                                                                                  // emails so we"ll take the first
                            }
                            else {
                                done(new Error("Sem email"));
                            }
                            newUser.facebook.gender = profile.gender;
                            // save our user to the database
                            newUser.save(function(err) {
                                if(err) {
                                    throw err;
                                }
                                // if successful, return the new user
                                return done(null, newUser);
                            });
                        }
                    });
                }
                else {
                    // user already exists and is logged in, we have to link accounts
                    var user = req.user; // pull the user out of the session
                    // TODO: Verificar se req.user possui facebook! Falha de segurança.

                    // update the current users facebook credentials
                    user.facebook.id = profile.id;
                    user.facebook.token = token;
                    user.facebook.name = profile.name.givenName;
                    //user.facebook.sobrenome = profile.name.familyName;
                    if(profile.emails) {
                        user.facebook.email = profile.emails[0].value;
                    }
                    user.facebook.gender = profile.gender;
                    // save the user
                    user.save(function(err) {
                        if(err) {
                            throw err;
                        }
                        return done(null, user);
                    });
                }
            });
        }));
};