module.exports = function(app, passport) {
    "use strict";
    require('../user/user.model.js');
    require('./authentication.passport.js')(passport);
    var route = require('./authentication.route.js');
    app.use('/auth', route);
};