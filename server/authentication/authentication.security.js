"use strict";

exports.auth = function auth(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.send(401);
};

exports.softAuth = function auth(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.send({
        success : false
    });
};

exports.notAuth = function notAuth(req, res, next) {
    if(!req.isAuthenticated()) {
        return next();
    }
    res.send(401);
};

exports.logout = function logout(req, res) {
    req.logout();
    res.redirect('/');
};

exports.isLoggedIn = function isLoggedIn(req, res, next) {
    var user = false;
    if(req.isAuthenticated()) {
        user = req.user.toObject();
        if(user.local) {
            delete user.local.password;
        }
    }
    res.send({
        success : true,
        data    : user
    });
};

exports.userProfile = function userProfile(req, res, next) {
    var user = req.user.toObject();
    if(user.local) {
        delete user.local.password;
    }
    return res.send({
        success : true,
        data    : user
    });
};