var passport = require('passport');

module.exports = function(req, res, next) {
    if(req.body.email === undefined || req.body.password === undefined) {
        var ret = {
            success : false,
            data    : "Ambos os campos precisam ser preenchidos"
        };
        return res.send(ret);
    }
    passport.authenticate("local-login", function(err, user, info) {
        var ret;
        if(err) {
            return next(err); // will generate a 500 error
        }
        if(!user) {
            ret = {
                success : false,
                data    : info.message || "Falha no login"
            };
            return res.send(ret);
        }

        req.logIn(user, function(err) {
            if(err) {
                return next(err); // 500?
            }
            var userObject = user.toObject();
            delete userObject.local.password;
            ret = {
                success : true,
                data    : userObject
            };
            return res.send(ret);
        });
    })(req, res, next);
};