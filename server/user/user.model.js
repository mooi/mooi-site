"use strict";

var mongoose = require("mongoose");
var bcrypt = require("bcrypt-nodejs");
var Schema = mongoose.Schema;

var userSchema = new Schema({
    local    : {
        email    : String,
        password : String,
        name     : String,
        gender   : String,
        age      : Number
    },
    facebook : {
        id     : String,
        token  : String,
        email  : String,
        name   : String,
        gender : String
    },
    likes    : [
        {
            id_externo : String,
            loja       : String,
            _id        : false
        }
    ],
    dislikes : [
        {
            id_externo : String,
            loja       : String,
            _id        : false
        }
    ],
    added    : {
        type    : Date,
        default : Date.now
    }
});

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

mongoose.model("User", userSchema, "users");