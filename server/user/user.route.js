(function() {
    'use strict';
    var security = require('../authentication/authentication.security.js');
    var express = require('express');
    var router = express.Router();

    router.post('/likeItem', security.softAuth, likeItem);

    router.post('/dislikeItem', security.softAuth, dislikeItem);

    function likeItem(req, res, next) {
        return itemSentiment(req, res, 'likes', 'dislikes');
    }

    function dislikeItem(req, res, next) {
        return itemSentiment(req, res, 'dislikes', 'likes');
    }

    function itemSentiment(req, res, listName, complementaryName) {
        // listName = 'likes', complementaryName = 'dislikes', por exemplo
        var user = req.user;
        var id = req.body;
        //var likes = user.likes || [];
        //var dislikes = user.dislikes || [];
        var list = user[listName] || [];
        var complementary = user[complementaryName] || [];
        var exists = false;
        // Tirar da lista complementar (e.g. dislike se for like), se estiver lá
        for(var i = 0; i < complementary.length; i += 1) {
            if(complementary[i].id_externo === id.id_externo && complementary[i].loja === id.loja) {
                complementary.splice(i, 1);
            }
        }
        // Adicionar à lista ou retirar se já estava
        for(i = 0; i < list.length; i += 1) {
            if(list[i].id_externo === id.id_externo && list[i].loja === id.loja) {
                // Já existe. Retirar.
                list.splice(i, 1);
                exists = true;
            }
        }
        if(!exists) {
            list.push(id);
        }
        user[listName] = list; //user['likes']
        user[complementaryName] = complementary;
        user.save(function(err) {
            if(err) {
                res.send({
                    success : false
                });
            }
            res.send({
                data : user,
                success : true
            });
        });
    }

    module.exports = router;

})();