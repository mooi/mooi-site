var express = require('express'); // Framework
var morgan = require('morgan'); // Logger de requisições HTTP
var debug = require('debug')('Express'); // Logger manual
var path = require('path'); // Filesystem
var cookieParser = require('cookie-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var bodyParser = require('body-parser');
var passport = require('passport');
var mongooseConnection = require('./mongodb/mongodb.connection.js');
var cors = require('cors');
// https://github.com/pillarjs/cookies
// https://github.com/helmetjs/helmet

var app = express();
var whitelist = [
    'localhost',
    'http://localhost',
    'http://mooi.me',
    'mooi.me',
    'http://www.mooi.me',
    'www.mooi.me',
    'http://www.mooibrasil.com.br',
    'www.mooibrasil.com.br',
    'http://mooibrasil.com.br',
    'mooibrasil.com.br',
    'http://www.mooibrasil.com',
    'http://mooibrasil.com',
    'www.mooibrasil.com',
    'mooibrasil.com',
    'http://192.168.1.102',
    '192.168.1.102',
    'http://54.232.244.140',
    '54.232.244.140'
];
var corsOptions = {
    origin: function(origin, callback){
        var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
        callback(null, originIsWhitelisted);
    }
};
app.use(cors(corsOptions));
app.use(require('prerender-node').set('prerenderServiceUrl', 'http://localhost:18185'));
app.use(express.static(__dirname + '/../client'));
if(process.env.NODE_ENV === 'production') {
    // É possível criar um log por dia ( https://github.com/expressjs/morgan )
    app.use(morgan('common', {
        skip : function(req, res) {
            return res.statusCode < 400 || req.headers['user-agent'] === 'Jasmine';
        }
    }));
}
else {
    app.use(morgan('dev', {
        skip : function(req, res) {
            return req.headers['user-agent'] === 'Jasmine';
        }
    }));
}
app.use(cookieParser('parabensaiferanaboa')); // Lê cookies. Necessário pra autenticação
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended : false
}));
app.use(session({
    secret            : 'parabensaiferanaboa',
    resave            : false,
    saveUninitialized : true,
    store             : new MongoStore({mongooseConnection : mongooseConnection})
}));
app.use(passport.initialize());
app.use(passport.session()); // Sessões persistentes de login

// Rotas
require('./authentication/authentication.js')(app, passport);
require('./subscription/subscription.model.js');
var testRoute = require('./spec/support/test.route.js');
app.use('/test', testRoute);
var userRoute = require('./user/user.route.js');
app.use('/user', userRoute);
var subscriptionRoute = require('./subscription/subscription.route.js');
app.use('/subscription', subscriptionRoute);
var itemRoute = require('./postgreSQLService/postgreSQLService.route.js');
app.use('/item', itemRoute);

// Não achou nada, enviar para o Angular
app.use(function(req, res, next) {
    var index = path.join(__dirname, "/../client/source/layout/", "index.html");
    res.sendFile(index);
});

// Tratamento de erros

// Desenvolvimento
// Manda a stack trace
if(process.env.NODE_ENV === 'development') {
    app.use(function(error, req, res, next) {
        if(req.headers['user-agent'] !== "Jasmine") {
            debug(error.stack);
        }
        res.status(error.status || 500);
        res.send(error.stack);
    });
}

// Produção
// Sem a stack trace
app.use(function(error, req, res, next) {
    res.status(error.status || 500);
    res.send(error.message);
});

module.exports = app;