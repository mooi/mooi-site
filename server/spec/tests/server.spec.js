"use strict";
var request = require("request");

describe("O servidor do site", function() {

    var options;

    beforeAll(function() {
        options = function(path) {
            path = path || "";
            return {
                url: 'http://localhost:' + process.env.PORT + '/test/' + path, //URL to hit
                headers: { //We can define headers too
                    'User-Agent': 'Jasmine'
                }
            }
        };
    });

    it("deve retornar o index em uma rota inexistente", function(done) {
        request.get(options('undefined'), function(error, response, body) {
            try {
                expect(response.statusCode).not.toBe(404);
                expect(response.body).toMatch(/ng-app/);
                done();
            }
            catch(error) {
                done.fail(error);
            }
        });
    }, 3000);

    it("deve mandar OK em uma rota de teste", function(done) {
        request.get(options(), function(error, response, body) {
            try {
                expect(response.statusCode).toBe(200);
                done();
            }
            catch(error) {
                done.fail(error);
            }
        });
    }, 3000);

    it("deve receber status 500 em uma rota com erro, junto da explicação", function(done) {
        request.get(options('errorLocal'), function(error, response, body) {
            try {
                expect(response.statusCode).toBe(500);
                expect(response.body).toMatch(/Simulated error/);
                done();
            }
            catch(error) {
                done.fail(error);
            }
        });
    }, 3000);

});