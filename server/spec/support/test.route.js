var express = require('express');

var router = express.Router();

router.get('/', function(req, res, next) {
    res.send("OK");
});

router.get('/errorLocal', function(req, res, next) {
    throw new Error('Simulated error');
});

module.exports = router;