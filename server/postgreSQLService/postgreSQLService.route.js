var express = require('express');
var router = express.Router();
var request = require("request");

router.get('/getMany', getMany);

router.get('/getManyById', getManyById);

function options(path, query) {
    path = path || "";
    return {
        url  : 'http://localhost:7900/item/' + path,
        form : query
    }
}

function getMany(req, res, next) {
    var query = req.query;
    request.get(options('getMany', query), function getManyCallback(error, response, body) {
        if(error || body.error) {
            res.status(-1);
            res.send({
                success : false,
                data    : error || body.error
            });
        }
        else {
            try {
                body = JSON.parse(body);
            }
            catch(e) {
                res.status(-1);
                res.send({
                    success : false,
                    data    : e.message || e.error || e
                });
            }
            var data = body.data;
            res.send({
                success : true,
                data    : data
            })
        }
    });
}

function getManyById(req, res, next) {
    var query = req.query;
    request.get(options('getManyById', query), function getManyByIdCallback(error, response, body) {
        if(error || body.error) {
            res.status(-1);
            res.send({
                success : false,
                data    : error || body.error
            });
        }
        else {
            try {
                body = JSON.parse(body);
            }
            catch(e) {
                res.status(-1);
                res.send({
                    success : false,
                    data    : e.message || e.error || e
                });
            }
            var data = body.data;
            res.send({
                success : true,
                data    : data
            })
        }
    });
}

module.exports = router;