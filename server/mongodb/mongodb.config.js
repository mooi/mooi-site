module.exports = {
    development: {
        db: "mongodb://localhost:27017/mooidb"
    },
    test: {
        db: "mongodb://localhost:27017/mooidb"
    },
    production: {
        db: "mongodb://localhost:27017/mooidb"
    }
};