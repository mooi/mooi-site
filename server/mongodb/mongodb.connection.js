var mongoose = require("mongoose");
var env = process.env.NODE_ENV;
var config = require("./mongodb.config.js")[env];
mongoose.connect(config.db);
mongoose.connection.on("connected", function () {
    if(env === "development") {
        console.log("Mongoose conectado em " + config.db);
    }
});
mongoose.connection.on("error", function (err) {
    if(env === "development") {
        console.log(err);
    }
});
mongoose.connection.on("disconnected", function () {
    if(env === "development") {
        console.log("Mongoose desconectado");
    }
});
process.on("SIGINT", function() {
    mongoose.connection.close(function () {
        if(env === "development") {
            console.log("Mongoose desconectado por término do processo");
        }
        process.exit(0);
    });
});

module.exports = mongoose.connection;