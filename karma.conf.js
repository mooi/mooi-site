// Karma configuration
// Generated on Sat Sep 26 2015 02:03:14 GMT-0300 (Hora oficial do Brasil)

module.exports = function(config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath : '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks : ['jasmine'],


        // list of files / patterns to load in the browser
        files : [
            // Libs
            'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.6/angular.js',
            'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.6/angular-animate.min.js',
            'client/components/angular-ui-router/angular-ui-router.min.mod.js',
            'client/components/angular/angular-mocks.js',
            'client/components/angular-toastr/angular-toastr.tpls.min.js',

            // app
            //'client/source/**/*(*.js|!*.spec.js)',

            'client/source/ui/js/ui.js',
            'client/source/mooiweb.module.js',
            'client/source/core/core.module.js',
            'client/source/**/*.module.js',
            'client/source/**/*.service.js',
            'client/source/**/*.config.js',
            'client/source/**/*.controller.js',
            'client/source/layout/layout.route.js',
            'client/source/**/*.route.js',
            'client/source/**/!(*.spec).js',
            'client/source/**/*.spec.js'
        ],


        // list of files to exclude
        exclude : [
            'client/source/db/db.spec.js'
        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors : {
            'client/source/**/!(*.spec).js': 'coverage'
        },


        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters : ['progress', 'coverage'],


        // web server port
        port : 7199,


        // enable / disable colors in the output (reporters and logs)
        colors : true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel : config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch : true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers : [
            'Chrome',
            'Firefox',
            'IE'
        ],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun : false
    })
};
