(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('currency', currency);

    currency.$inject = [];
    function currency() {
        return function(input) {
            var response = 'R$';
            var parsedInput = parseInt(input, 10); // 6999
            if(isNaN(parsedInput)) {
                return response + parsedInput;
            }
            else {
                var real = Math.floor(parsedInput/100); // 69
                var cents = parsedInput - real * 100; // 6999 - 6900 = 99
                if(cents < 10) { // 0
                    cents = '0' + cents; // 00 -> 10,00
                }
                return response + real + ',' + cents;
            }
        }
    }

})();