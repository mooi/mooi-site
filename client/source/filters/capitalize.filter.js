(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('capitalize', capitalize);

    capitalize.$inject = [];
    function capitalize() {
        var reg;
        return function(input, all) {
            if(all) {
                reg = /([^\W_]+[^\s-]*) */g;
            }
            else {
                reg = /([^\W_]+[^\s-]*)/;
            }
            if(!!input) {
                var capitalized = input.replace(reg, function(txt) {
                    var capitalizedWord = txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    return capitalizedWord;
                });
                return capitalized;
            }
            return input;
        }
    }
})();
