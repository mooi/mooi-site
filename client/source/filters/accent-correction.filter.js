(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('accentCorrection', accentCorrection);

    accentCorrection.$inject = [];
    function accentCorrection() {
        var dictionary = [
            {from: 'acessorios', to: 'acessórios'},
            {from: 'calcados', to: 'calçados'},
            {from: 'vestuario', to: 'vestuário'}
        ];

        return function(input) {
            var correctEntry = dictionary.filter(function(entry) {
                return entry.from === input;
            })[0];
            if(correctEntry !== undefined) {
                return correctEntry.to;
            }
            return input;
        }
    }

})();