(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('genderCorrection', genderCorrection);

    genderCorrection.$inject = [];
    function genderCorrection() {
        var dictionary = [
            {from: 'f', to: 'Feminino'},
            {from: 'm', to: 'Masculino'},
            {from: 'u', to: 'Unissex'},
            {from: 'x', to: '-'}
        ];

        return function(input) {
            if(!!input) {
                var correctEntry = dictionary.filter(function(entry) {
                    return entry.from === input.toLowerCase();
                })[0];
                if(correctEntry !== undefined) {
                    return correctEntry.to;
                }
            }
            return input;
        }
    }
})();