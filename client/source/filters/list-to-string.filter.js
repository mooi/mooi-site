(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('listToString', listToString);

    listToString.$inject = ['$filter'];
    function listToString($filter) {
        return function(input) {
            var result;
            if(!!input) {
                if(input.constructor !== Array) {
                    input = [input];
                }
                result = input.join(', ');
                result = $filter('capitalize')(result, true);
            }
            return result;
        }
    }
})();