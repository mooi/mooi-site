(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('material', material);

    material.$inject = ['$filter'];
    function material($filter) {
        return function(input) {
            if(!!input && input.constructor === Array && input.length > 0) {
                var entriesArray = [];
                for(var i = 0; i < input.length; i += 1) {
                    var entry = '';
                    if(!!input[i].valor) {
                        if(!!input[i].porcentagem) {
                            var percentage = input[i].porcentagem + '% ';
                            entry += percentage;
                        }
                        var value = $filter('capitalize')(input[i].valor, true);
                        entry += value;
                        entriesArray.push(entry);
                    }
                }
                var response = entriesArray.join(', ');
                return response;
            }
            return '';
        }
    }
})();