(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('percentage', percentage);

    percentage.$inject = [];
    function percentage() {
        return function(input) {
            var value = Math.floor(parseFloat(input));
            if(isNaN(value)) {
                value = input;
            }
            return value + '%';
        }
    }

})();