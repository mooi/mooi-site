(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('lineBreakCorrection', lineBreakCorrection);

    lineBreakCorrection.$inject = [];
    function lineBreakCorrection() {
        var regex = /(<br[\s]*[/]*>)/gi;

        return function(input, nonewline) {
            nonewline = nonewline || false;
            if(input !== undefined) {
                input = input.toString();
                if(!nonewline) {
                    input = input.replace(regex, "\n");
                }
                else {
                    input = input.replace(regex, " ");
                }
            }
            return input;
        }
    }

})();