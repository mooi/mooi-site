(function() {
    'use strict';
    angular.
        module('mooiweb.core', [
            //do angular
            'ngAnimate',

            //módulos cross-app reutilizaveis

            // 3rd party
            'toastr',
            'ui.router',
            'ui.slider',
            'imagesLoaded',
            'angularUtils.directives.dirPagination',
            'djds4rce.angular-socialshare'
        ])
})();