(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .run(run);

    run.$inject = ['$FB', 'config'];
    function run($FB, config) {
        var appId = config.facebook.appId;
        $FB.init(appId);
    }

})();