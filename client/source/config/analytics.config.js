(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .config(configure);

    configure.$inject = ['$windowProvider', 'configProvider'];
    function configure($windowProvider, configProvider){
        var $window = $windowProvider.$get();
        var config = configProvider.$get();
        var id = config.analytics.id;
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        $window.ga('create', id, 'auto');
    }

})();