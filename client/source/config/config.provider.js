(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .provider('config', configProvider);

    configProvider.$inject = [];
    function configProvider() {

        this.$get = config;

        config.$inject = [];
        function config() {
            var service = {
                mooiweb           : mooiweb(),
                postgreSQLService : postgreSQLService(),
                mongodb           : mongodb(),
                facebook          : facebook(),
                analytics         : analytics(),
                produtos          : produtos(),
                dirPagination     : dirPagination()
            };

            return service;

            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            function mooiweb() {
                var host;
                if('production' == 'production') {
                    host = 'www.mooi.me';
                }
                else {
                    host = 'localhost';
                }
                return {
                    port                  : 80,
                    environment           : 'production',
                    localStorageAvailable : Modernizr.localstorage,
                    searchEnabled         : false,
                    host                  : host
                }
            }

            function postgreSQLService() {
                var address = mooiweb().host;//'localhost'; // 'mooi.me'
                var port = 80;
                var url = 'http://' + address + ':' + port + '/'; // 'http://localhost:12354/'

                var item = {
                    getMany     : url + 'item/getMany', // 'http://localhost:12354/item/getMany'
                    getManyById : url + 'item/getManyById'
                };

                var test = {
                    ok       : url + 'test/ok', // 'http://localhost:12354/test/ok',
                    error    : url + 'test/error',
                    notFound : url + 'test/notfound'
                };

                return {
                    address : address,
                    port    : port,
                    url     : url,
                    item    : item,
                    //test    : test
                }
            }

            function mongodb() {
                var address = mooiweb().host;//'localhost';
                var port = 80;
                var url = 'http://' + address + ':' + port + '/'; // 'http://localhost:80/'

                var user = {
                    signup      : url + 'auth/signup', // 'http://localhost:80/auth/signup'
                    login       : url + 'auth/localLogin',
                    isLoggedIn  : url + 'auth/isLoggedIn',
                    logout      : url + 'auth/logout',
                    likeItem    : url + 'user/likeItem',
                    dislikeItem : url + 'user/dislikeItem'
                };

                var subscription = {
                    subscribe : url + 'subscription/subscribe'
                };

                return {
                    address      : address,
                    port         : port,
                    url          : url,
                    user         : user,
                    subscription : subscription
                }
            }

            function facebook() {
                var appId;
                if(mooiweb().environment === 'production') {
                    appId = '1470679093192561'; // Mooi!
                }
                else {
                    appId = '1529509513976185'; // Mooi! Test 1
                }
                return {
                    appId        : appId,
                    version      : 'v2.2',
                    permissions  : 'email',
                    pixelId      : '6021327954984',
                    loginEnabled : true,
                    pixelEnabled : true
                }
            }

            function analytics() {
                return {
                    id      : 'UA-67282577-1',
                    enabled : true
                }
            }

            function produtos() {
                return {
                    params : {
                        // Filtros
                        sexo          : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        infantil      : {
                            value  : 'false',
                            squash : true
                        },
                        tipos         : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        marcas        : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        lojas         : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        cores         : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        tamanhos      : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        menorDesconto : {
                            value  : '0',
                            squash : true
                        },
                        maiorDesconto : {
                            value  : '100',
                            squash : true
                        },
                        menorPreco    : {
                            value  : '0',
                            squash : true
                        },
                        maiorPreco    : {
                            value  : '200000',
                            squash : true
                        },
                        // extrainfo
                        temporada     : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        comprimento   : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        gola          : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        manga         : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        estilo        : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        cano          : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        saltoAltura   : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        salto         : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        ocasiao       : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        // Opções
                        ordem         : {
                            value  : 'maiorDesconto',
                            squash : true
                        },
                        porPagina     : {
                            value  : '80',
                            squash : true
                        },
                        pagina        : {
                            value  : '1',
                            squash : true
                        },
                        // UI
                        filtros       : {
                            value : true
                        }
                    }
                }
            }

            function dirPagination() {
                return {
                    path : '/components/angular-utils-pagination/dirPagination.tpl.html'
                }
            }
        }

    }

})();
