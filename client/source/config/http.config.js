(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .config(configure);

    configure.$inject = ['$httpProvider'];
    function configure($httpProvider) {
        $httpProvider.interceptors.push(httpUnauthorizedInterceptor);

        httpUnauthorizedInterceptor.$inject = ['$q', '$location'];
        function httpUnauthorizedInterceptor($q, $location) {
            return {
                response      : function(response) {
                    return response;
                },
                responseError : function(response) {
                    if(response.status === 401) {
                        $location.url('/home');
                    }
                    return $q.reject(response);
                }
            };
        }
    }
})();