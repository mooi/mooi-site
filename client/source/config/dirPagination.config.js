(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .config(configure);

    configure.$inject = ['paginationTemplateProvider', 'configProvider'];
    function configure(paginationTemplateProvider, configProvider) {
        var config = configProvider.$get();
        var templatePath = config.dirPagination.path;
        paginationTemplateProvider.setPath(templatePath);
    }

})();