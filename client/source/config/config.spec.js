(function() {
    'use strict';

    describe("The configuration file", function() {

        var config;

        beforeEach(module('mooiweb'));

        beforeEach(inject(function(_config_) {
            config = _config_;
        }));

        it("should return the port where the server is listening", function() {
            var port = config.mooiweb.port;
            expect(port).toEqual(jasmine.any(Number));
            expect(port).toBeGreaterThan(0);
            expect(Math.trunc(port)).toEqual(port);
        });

        it("should return PostgreSQLService's connection details", function() {
            var port = config.postgreSQLService.port;
            var address = config.postgreSQLService.address;
            var url = config.postgreSQLService.url;
            var item = config.postgreSQLService.item;
            expect(port).toEqual(jasmine.any(Number));
            expect(port).toBeGreaterThan(0);
            expect(Math.trunc(port)).toEqual(port);
            expect(address).toBeDefined();
            expect(url).toBeDefined();
            expect(item.getMany).toBeDefined();
        });

    });

})();