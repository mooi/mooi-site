(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .run(run);

    run.$inject = ['$rootScope', '$state', 'bi'];
    function run($rootScope, $state, bi) {
        changeTitle($rootScope, $state);
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            //changeTitle($rootScope, $state);
            bi.analytics.pageView();
        })
    }

    function changeTitle($rootScope, $state) {
        var defaultTitle = 'A sua vitrine da moda na internet';
        if(!!$state.current.data) {
            $rootScope.title = $state.current.data.title || defaultTitle;
        }
        else {
            $rootScope.title = defaultTitle;
        }
    }

})();
