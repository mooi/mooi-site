(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .config(configure);

    configure.$inject = ['toastrConfig'];
    function configure(toastrConfig) {
        angular.extend(toastrConfig, {
            positionClass         : 'toast-bottom-right'
        });
    }

})();