(function() {
    'use strict';
    angular.
        module('mooiweb.bi').
        factory('bi', bi);

    bi.$inject = ['$window', '$location', 'config'];

    function bi($window, $location, config) {
        var service = {
            analytics     : analytics(),
            facebookPixel : facebookPixel()
        };

        return service;
        /////////////////////

        function analytics() {
            var methods = {
                sendEvent : function sendEvent(category, action, label) {
                    if(config.analytics.enabled) {
                        $window.ga('send', 'event', category, action, label, {
                            page      : $location.url(),
                            useBeacon : true
                        });
                    }
                },
                pageView  : function pageView() {
                    if(config.analytics.enabled) {
                        $window.ga('send', 'pageview', {
                            page      : $location.url(),
                            useBeacon : true
                        });
                    }
                }
            };
            return methods;
        }

        function facebookPixel() {
            var methods = {
                conversion: function conversion() {
                    if(config.facebook.pixelEnabled) {
                        $window._fbq.push(['track', config.facebook.pixelId, {
                            'value'    : '1.00',
                            'currency' : 'BRL'
                        }]);
                    }
                }
            };
            return methods;
        }
    }
})();