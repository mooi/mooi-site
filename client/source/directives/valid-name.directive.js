(function() {
    'use strict';
    angular
        .module('mooiweb.directives')
        .directive('mooiValidName', mooiValidName);

    mooiValidName.$inject = [];
    function mooiValidName() {
        var nameRegex = /[a-zA-Z \u00C0-\u024f]+$/;
        return {
            require: 'ngModel',
            restrict: '',
            link: function(scope, elm, attrs, ctrl) {
                if (ctrl) {
                    ctrl.$validators.name = function(modelValue) {
                        var isEmpty = ctrl.$isEmpty(modelValue);
                        var isValid = false;
                        if(!isEmpty) {
                            isValid = nameRegex.test(modelValue) && modelValue.length >= 3;
                        }
                        return isEmpty || isValid;
                    };
                }
            }
        };
    }

})();