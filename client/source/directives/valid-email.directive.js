(function() {
    'use strict';
    angular
        .module('mooiweb.directives')
        .directive('mooiValidEmail', mooiValidEmail);

    mooiValidEmail.$inject = [];
    function mooiValidEmail() {
        var emailRegex = /^[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,63}$/;
        return {
            require: 'ngModel',
            restrict: '',
            link: function(scope, elm, attrs, ctrl) {
                // only apply the validator if ngModel is present and Angular has added the email validator
                if (ctrl && ctrl.$validators.email) {

                    // this will overwrite the default Angular email validator
                    ctrl.$validators.email = function(modelValue) {
                        var isEmpty = ctrl.$isEmpty(modelValue);
                        var isValid = false;
                        if(!isEmpty) {
                            isValid = emailRegex.test(modelValue.toLowerCase());
                        }
                        return isEmpty || isValid;
                    };
                }
            }
        };
    }

})();