(function() {
    'use strict';
    angular
        .module('mooiweb.directives')
        .directive('mooiValidPassword', mooiValidPassword);

    mooiValidPassword.$inject = [];
    function mooiValidPassword() {
        var passwordRegex = /^[a-zA-Z0-9,!_.-@#%*]+$/;
        return {
            require: 'ngModel',
            restrict: '',
            link: function(scope, elm, attrs, ctrl) {
                // only apply the validator if ngModel is present
                if (ctrl) {
                    ctrl.$validators.password = function(modelValue) {
                        var isEmpty = ctrl.$isEmpty(modelValue);
                        var isValid = false;
                        if(!isEmpty) {
                            isValid = passwordRegex.test(modelValue.toLowerCase()) && modelValue.length >= 5;
                        }
                        return isEmpty || isValid;
                    };
                }
            }
        };
    }

})();