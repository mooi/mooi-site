(function() {
    'use strict';
    angular
        .module('mooiweb.directives')
        .directive('mooiOnFinishRender', mooiOnFinishRender);

    mooiOnFinishRender.$inject = ['$timeout'];
    function mooiOnFinishRender($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit(attr.mooiOnFinishRender);
                    });
                }
            }
        }
    }

})();