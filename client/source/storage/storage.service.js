(function() {
    'use strict';

    angular
        .module('mooiweb.storage')
        .factory('storage', storage);

    storage.$inject = ['config'];
    function storage(config) {
        var service = {
            // Itens
            set        : set,
            setViewed  : setViewed,
            setRelated : setRelated,
            get        : get,
            getViewed  : getViewed,
            getRelated : getRelated
        };

        return service;
        /////////////////////

        // Itens

        function get(key) {
            if(config.mooiweb.localStorageAvailable) {
                if(!!key) {
                    var value = localStorage.getItem(key);
                    try {
                        var parsedValue = JSON.parse(value);
                        return parsedValue;
                    }
                    catch(e) {
                        return value;
                    }
                }
                else {
                    throw new Error('storage.service.js:get - No item passed');
                }
            }
            else {
                return null;
            }
        }

        function getViewed() {
            if(config.mooiweb.localStorageAvailable) {
                var viewedList = get('viewedList') || [];
                return viewedList;
            }
            else {
                return null;
            }
        }

        function set(key, value) {
            if(config.mooiweb.localStorageAvailable) {
                if(!!key && !!value) {
                    var valueString = JSON.stringify(value);
                    localStorage.setItem(key, valueString);
                    var valueSaved = localStorage.getItem(key);
                    try {
                        var parsedValue = JSON.parse(valueSaved);
                        return parsedValue;
                    }
                    catch(e) {
                        return valueSaved;
                    }
                }
                else {
                    throw new Error('storage.service.js:get - No key or value passed');
                }
            }
            else {
                return null;
            }
        }

        function setViewed(item) {
            if(config.mooiweb.localStorageAvailable) {
                if(!!item) {
                    var viewedList = get('viewedList') || [];
                    for(var i = 0; i < viewedList.length; i += 1) {
                        if(viewedList[i].id_externo === item.id_externo) {
                            viewedList.splice(i, 1);
                        }
                    }
                    viewedList.unshift(item);
                    if(viewedList.length > 8) {
                        viewedList.pop();
                    }
                    return set('viewedList', viewedList) === viewedList;
                }
                else {
                    return new Error('storage.service.js:setViewed - No item passed');
                }
            }
            else {
                return null;
            }
        }

        function setRelated(item, related) {
            if(config.mooiweb.localStorageAvailable) {
                if(!!item && !!related) {
                    var relatedList = related.slice(0, 8); // 8 primeiras posições
                    var id = item.loja + item.id_externo;
                    return set('related_' + id, relatedList) === relatedList;
                }
                else {
                    throw new Error('storage.service.js:setRelated - No item or related list passed');
                }
            }
            else {
                return null;
            }
        }

        function getRelated(item) {
            if(config.mooiweb.localStorageAvailable) {
                var id = item.loja + item.id_externo;
                var relatedList = get('related_' + id) || [];
                return relatedList;
            }
            else {
                return null;
            }
        }
    }
})();
