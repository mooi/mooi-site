(function() {
    'use strict';
    angular
        .module('mooiweb.layout', ['mooiweb.security']);
})();