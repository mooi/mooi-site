(function() {
    'use strict';
    angular.module('mooiweb.layout')
        .controller('Header', Header);

    Header.$inject = ['$filter', '$scope', '$state', 'helper', 'config', 'db', 'usuario', 'ui', 'user'];
    function Header($filter, $scope, $state, helper, config, db, usuario, ui, user) {
        var vm = this;
        vm.user = user;
        vm.userName = usuario.getUserName(user);
        vm.logout = logout;
        vm.cadastro = cadastro;
        vm.searchEnabled = config.mooiweb.searchEnabled;
        vm.menus = menus();

        init();

        ///////////////////////////////

        function init() {
            helper.log('Header Loaded');
            $scope.$on('userChanged', function(event, user) {
                vm.user = user;
                vm.userName = usuario.getUserName(user);
            });

            $scope.$on('headerMenusRendered', function(event) {
                ui.loadUI('Header');
            });
        }

        function logout() {
            return db.logout();
        }

        function cadastro() {
            return $state.go('root.cadastro', {}, {inherit : false});
        }

        function menus() {
            var feminine = {
                name : 'FEMININO',
                gender : 'f',
                infantile : false,
                submenus : [
                    {
                        name  : 'ACESSÓRIOS FEMININOS',
                        category : 'acessorios',
                        items : [
                            "acessórios",
                            "bolsas",
                            "bijuterias",
                            "relógios",
                            "cintos",
                            "carteiras",
                            "óculos",
                            "mochilas",
                            "necessaires",
                            "lenços",
                            "gorros",
                            "chapéus",
                            "bonés",
                            "fitness",
                            "luvas",
                            "cachecóis",
                            "meias"
                        ]
                    },
                    {
                        name : 'ROUPAS FEMININAS',
                        category : 'vestuario',
                        items : [
                            "blusas",
                            "vestidos",
                            "camisetas",
                            "calças",
                            "saias",
                            "shorts",
                            "camisas",
                            "moda-praia",
                            "casacos",
                            "bermudas",
                            "calcinhas",
                            "tops",
                            "macacões",
                            "biquinis",
                            "regatas",
                            "jaquetas",
                            "fitness",
                            "suéteres",
                            "pijamas",
                            "leggings",
                            "camisolas",
                            "blazers",
                            "conjuntos",
                            "meias",
                            "coletes",
                            "bodies",
                            "short",
                            "moletons",
                            "lingeries",
                            "modeladores e cintas",
                            "maiôs",
                            "moda praia",
                            "agasalhos",
                            "sobretudos",
                            "polos",
                            "tunicas",
                            "gestante",
                            "boleros",
                            "bolsas",
                            "sapatilhas",
                            "cintos",
                            "sutiãs",
                            "carteiras"
                        ]
                    },
                    {
                        name : 'CALÇADOS FEMININOS',
                        category : 'calcados',
                        items : [
                            "calçados",
                            "sapatilhas",
                            "rasteiras",
                            "tênis",
                            "scarpins",
                            "botas",
                            "sapatos",
                            "sandálias",
                            "mocassins",
                            "chinelos",
                            "anabelas",
                            "alpargatas",
                            "ankle boots",
                            "birkens",
                            "slippers",
                            "tamancos",
                            "cardigans",
                            "suéteres",
                            "oxfords",
                            "casacos",
                            "meias",
                            "sneakers",
                            "sapatênis",
                            "jaquetas"
                        ]
                    }
                ]
            };
            var masculine = {
                name : 'MASCULINO',
                gender : 'm',
                infantile : false,
                submenus : [
                    {
                        name  : 'ACESSÓRIOS MASCULINOS',
                        category: 'acessorios',
                        items : [
                            "acessórios",
                            "relógios",
                            "cintos",
                            "bonés",
                            "carteiras",
                            "óculos",
                            "mochilas",
                            "gorros",
                            "bolsas",
                            "necessaires",
                            "chapéus",
                            "gravatas",
                            "cachecóis",
                            "lenços"
                        ]
                    },
                    {
                        name : 'ROUPAS MASCULINAS',
                        category: 'vestuario',
                        items : [
                            "camisetas",
                            "bermudas",
                            "camisas",
                            "blusas",
                            "polos",
                            "calças",
                            "jaquetas",
                            "suéteres",
                            "casacos",
                            "cuecas",
                            "sungas",
                            "meias",
                            "moletons",
                            "shorts",
                            "agasalhos",
                            "regatas",
                            "fitness",
                            "conjuntos",
                            "blazers",
                            "pijamas",
                            "coletes",
                            "short",
                            "tops",
                            "ternos",
                            "sapatos",
                            "macacões",
                            "cintos",
                            "saias",
                            "acessórios",
                            "skate",
                            "sneakers",
                            "relógios",
                            "vestidos",
                            "tunicas",
                            "mochilas",
                            "moda-praia",
                            "gorros",
                            "cosméticos",
                            "mocassins"
                        ]
                    },
                    {
                        name : 'CALÇADOS MASCULINOS',
                        category: 'calcados',
                        items : [
                            "calçados",
                            "tênis",
                            "sapatênis",
                            "botas",
                            "sapatos",
                            "chinelos",
                            "mocassins",
                            "sandálias",
                            "papetes",
                            "sapatilhas",
                            "cardigans",
                            "chuteiras",
                            "suéteres",
                            "skate",
                            "sneakers",
                            "camisetas",
                            "oxfords",
                            "slippers",
                            "jaquetas",
                            "scarpins",
                            "alpargatas"
                        ]
                    }
                ]
            };
            var infantile = {
                name : 'INFANTIL',
                gender : null,
                infantile : true,
                submenus : [
                    {
                        name  : 'ACESSÓRIOS INFANTIS',
                        category: 'acessorios',
                        items : [
                            "acessórios",
                            "mochilas",
                            "bolsas",
                            "cintos",
                            "bonés",
                            "óculos",
                            "gestante",
                            "meias",
                            "cachecóis",
                            "necessaires"
                        ]
                    },
                    {
                        name : 'ROUPAS INFANTIS',
                        category: 'vestuario',
                        items : [
                            "conjuntos",
                            "blusas",
                            "camisetas",
                            "calças",
                            "vestidos",
                            "regatas",
                            "casacos",
                            "bermudas",
                            "camisas",
                            "saias",
                            "pijamas",
                            "jaquetas",
                            "macacões",
                            "shorts",
                            "meias",
                            "bodies",
                            "cuecas",
                            "moletons",
                            "sungas",
                            "suéteres",
                            "polos",
                            "coletes",
                            "leggings",
                            "acessórios",
                            "bolsas",
                            "calcinhas",
                            "cintos",
                            "agasalhos",
                            "short",
                            "gorros",
                            "sobretudos",
                            "blazers",
                            "boleros",
                            "tops",
                            "camisolas"
                        ]
                    },
                    {
                        name : 'CALÇADOS INFANTIS',
                        category: 'calcados',
                        items : [
                            "calçados",
                            "tênis",
                            "sandálias",
                            "sapatilhas",
                            "botas",
                            "chinelos",
                            "sapatos",
                            "sapatênis",
                            "papetes",
                            "rasteiras",
                            "mocassins",
                            "casacos",
                            "cardigans",
                            "tamancos"
                        ]
                    }
                ]
            };
            var accessories = {
                name : 'ACESSÓRIOS',
                gender : null,
                infantile : false,
                submenus : [
                    {
                        name : 'ACESSÓRIOS',
                        category: 'acessorios',
                        items : [
                            "acessórios",
                            "bolsas",
                            "relógios",
                            "cintos",
                            "bijuterias",
                            "carteiras",
                            "óculos",
                            "mochilas",
                            "bonés",
                            "gorros",
                            "necessaires",
                            "lenços",
                            "chapéus",
                            "luvas",
                            "gravatas",
                            "cachecóis",
                            "gestante",
                            "fitness",
                            "meias"
                        ]
                    }
                ]
            };
            var menus = [
                feminine,
                masculine,
                infantile
                //accessories,
            ];
            return menus;
        }
    }

})();