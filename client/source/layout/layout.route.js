(function() {
    'use strict';

    angular.
        module('mooiweb.layout').
        config(layoutRouteConfig);

    layoutRouteConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', 'securityProvider'];
    function layoutRouteConfig($stateProvider, $urlRouterProvider, $locationProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider.state('root', {
            url      : '',
            abstract : true,
            views    : {
                'offCanvas' : {
                    templateUrl  : 'source/layout/off-canvas.html',
                    controller   : 'OffCanvas',
                    controllerAs : 'vm'
                },
                'header'    : {
                    templateUrl  : 'source/layout/header.html',
                    controller   : 'Header',
                    controllerAs : 'vm'
                },
                'footer'    : {
                    templateUrl  : 'source/layout/footer.html',
                    controller   : 'Footer',
                    controllerAs : 'vm'
                }
            },
            resolve : {
                user : security.checkLoggedIn
            }
        });

        $urlRouterProvider.when('', '/home');

        $urlRouterProvider.when('/', '/home');

        $urlRouterProvider.otherwise("/oops");

        $locationProvider.html5Mode(true).hashPrefix('!');

    }

})();
