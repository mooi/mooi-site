(function() {
    'use strict';
    angular.module('mooiweb.layout')
        .controller('Footer', Footer);

    Footer.$inject = ['$rootScope', 'db', 'ui', 'user'];
    function Footer($rootScope, db, ui, user) {
        var vm = this;
        vm.submit = submit;
        vm.sent = false;

        init();

        function init() {

        }

        function submit(subscribeForm) {
            if(!!subscribeForm.$valid && !vm.sent) {
                $rootScope.loading = true;
                db.subscribe(vm.email)
                    .then(function(success) {
                        if(!!success) {
                            vm.sent = true;
                        }
                    })['finally'](function() {
                    $rootScope.loading = false;
                });
            }
        }
    }

})();