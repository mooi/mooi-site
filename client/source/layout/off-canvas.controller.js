(function() {
    'use strict';
    angular.module('mooiweb.layout')
        .controller('OffCanvas', OffCanvas);

    OffCanvas.$inject = ['$scope', '$rootScope', '$state', 'helper', 'db', 'ui', 'usuario', 'user'];
    function OffCanvas($scope, $rootScope, $state, helper, db, ui, usuario, user) {
        var vm = this;
        vm.submitLogin = submitLogin;
        vm.logout = logout;
        vm.form = {};
        vm.user = user;
        vm.userGender = usuario.getUserGender(user);

        init();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function init() {
            helper.log('Off-Canvas loaded');
            $scope.$on('userChanged', function(event, user) {
                vm.user = user;
                vm.userGender = usuario.getUserGender(user);
            });
            //$scope.$on('HeaderLoaded', function(event, data) {
            //    ui.loadUI('Off-Canvas');
            //});
        }

        function submitLogin() {
            $rootScope.loading = true;
            db.login(vm.form)
                .then(function(user) {
                    $rootScope.user = user;
                    vm.form.password = undefined;
                    ui.closeLoginModal();
                    return $state.go('root.usuario', {}, {inherit: false});
                })['finally'](function() {
                $rootScope.loading = false;
            });
        }

        function logout() {
            $rootScope.loading = true;
            db.logout()
                .then(function(message) {
                    return $state.go('root.home', {}, {inherit: false});
                })['finally'](function() {
                $rootScope.loading = false;
            });
        }
    }

})();