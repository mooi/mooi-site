(function() {
    'use strict';

    angular.
        module('mooiweb.security')
        .provider('security', securityProvider);

    securityProvider.$inject = [];
    function securityProvider() {

        var user = null;
        this.$get = security;

        security.$inject = [];
        function security() {

            var service = {
                ensureLoggedIn    : ensureLoggedIn,
                ensureNotLoggedIn : ensureNotLoggedIn,
                checkLoggedIn     : checkLoggedIn,
                deauthorize       : deauthorize,
                updateUser        : updateUser
            };

            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            updateUser.$inject = [];
            function updateUser(newUser) {
                user = newUser;
                return user;
            }

            // Devolve 401 se não estiver logado. É interceptado e mandado pra home
            ensureLoggedIn.$inject = ['$q', '$http', '$state', 'helper'];
            function ensureLoggedIn($q, $http, $state, helper) {
                var deferred = $q.defer();
                $http.get('/auth/userProfile').then(function(response) {
                    var newUser = response.data.data;
                    if(newUser !== user) {
                        service.updateUser(newUser);
                        helper.broadcast("userChanged", newUser);
                    }
                    if(!!newUser) {
                        deferred.resolve(newUser);
                    }
                    else {
                        $state.go('root.home', {}, {inherit : false});
                        deferred.reject();
                    }
                }, function(reason) {
                    $state.go('root.home', {}, {inherit : false});
                    deferred.reject();
                });
                return deferred.promise;
            }

            // Devolve 401 caso o usuário já esteja logado para evitar conflitos de cadastro
            ensureNotLoggedIn.$inject = ['$q', '$http', '$state'];
            function ensureNotLoggedIn($q, $http, $state) {
                var deferred = $q.defer();
                if(user) {
                    $state.go('root.home', {}, {inherit : false});
                    deferred.reject();
                }
                else {
                    $http.get('/auth/isNotAuthorized').then(function(response) {
                        if(response.data.success === true) {
                            deferred.resolve(true);
                        }
                        else {
                            $state.go('root.home', {}, {inherit : false});
                            deferred.reject();
                        }
                    }, function(reason) {
                        $state.go('root.home', {}, {inherit : false});
                        deferred.reject();
                    });
                }
                return deferred.promise;
            }

            // Apenas pega o usuário, se estiver logado
            checkLoggedIn.$inject = ['$q', '$http', 'helper'];
            function checkLoggedIn($q, $http, helper) {
                var deferred = $q.defer();
                // usuário no cache
                if(user !== null && user !== undefined) {
                    deferred.resolve(user);
                }
                else {
                    $http.get('/auth/isLoggedIn').then(function(response) {
                        var newUser = response.data.data;
                        if(newUser !== user) {
                            service.updateUser(newUser);
                            helper.broadcast("userChanged", newUser);
                        }
                        deferred.resolve(newUser);
                    }, function(reason) {
                        deferred.reject();
                    });
                }
                return deferred.promise;
            }

            deauthorize.$inject = ['helper'];
            function deauthorize(helper) {
                service.updateUser(false);
                helper.broadcast("userChanged", false);
            }

            return service;
        }

    }
})();