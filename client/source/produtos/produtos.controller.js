(function() {
    'use strict';
    angular
        .module('mooiweb.produtos')
        .controller('Produtos', Produtos);

    Produtos.$inject = [
        '$scope', '$rootScope', '$state', '$filter', '$stateParams', 'ui', 'db', 'helper', 'config', 'storage', 'seo', 'bi', 'user'
    ];
    function Produtos($scope, $rootScope, $state, $filter, $stateParams, ui, db, helper, config, storage, seo, bi, user) {
        var vm = this;
        vm.resetAllFilters = resetAllFilters;
        vm.resetFilters = resetFilters;
        vm.setItemViewed = setItemViewed;
        vm.likeItem = likeItem;
        vm.dislikeItem = dislikeItem;
        vm.conversion = conversion;
        vm.interest = interest;
        vm.getImageURL = getImageURL;
        $rootScope.statusCode = 200;
        //vm.toggleGender
        //vm.toggleColor

        init();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Toda mudança é refletida na URL e a página é recalculada

        function init() {
            try {
                getParams();
                validateParams();
                pageSetup();
                getItems();
                ui.loadUI('Produtos');
                $scope.$on('ALWAYS', function(event) {
                    ui.loadUI('Produtos');
                });
                $scope.$on('productsRendered', function(event) {
                    ui.productsMouseOver();
                });
                helper.log('Produtos loaded - ' + user);
            }
            catch(e) {
                return $state.go('root.home');
            }
        }

        function getParams() {
            getURLParams();
            getControlParams();
            getDefaultParams();

            function getURLParams() {
                vm.query = {};
                vm.query.conditions = {}; // Filtros
                vm.query.options = {};    // Ordenação e quantidade
                // Filtros
                vm.query.conditions.category = $stateParams.categoria;             // Categoria (vestuário, acessorios,
                                                                                   // calcados)
                vm.query.conditions.gender = $stateParams.sexo;                    // Sexo ([f]eminino, [m]asculino)
                vm.query.conditions.infantile = JSON.parse($stateParams.infantil); // Infantil (true, false)
                vm.query.conditions.types = $stateParams.tipos;                    // Tipos ([vestidos, peep toes],
                                                                                   // etc...)
                vm.query.conditions.brands = $stateParams.marcas;                  // Marcas ([Abercrombie, Hering],
                                                                                   // etc...)
                vm.query.conditions.stores = $stateParams.lojas;                   // Lojas ([Marisa], [Dafiti], etc...)
                vm.query.conditions.colors = $stateParams.cores;                   // Cores ([verde, amarelo], etc...)
                vm.query.conditions.sizes = $stateParams.tamanhos;                 // Tamanhos ([P], [G, 60], etc...)
                for(var i = 0; i < vm.query.conditions.sizes.length; i += 1) {
                    vm.query.conditions.sizes[i] = vm.query.conditions.sizes[i].replace("%2F", "/");
                }

                vm.query.conditions.lowPrice = parseInt($stateParams.menorPreco);        // Menor Preço em centavos (0,
                                                                                         // etc...)
                vm.query.conditions.highPrice = parseInt($stateParams.maiorPreco);       // Maior Preço em centavos
                                                                                         // (100000, etc...)
                vm.query.conditions.lowDiscount = parseInt($stateParams.menorDesconto);  // Menor desconto em
                                                                                         // porcentagem (0, 30, etc...)
                vm.query.conditions.highDiscount = parseInt($stateParams.maiorDesconto); // Maior desconto em
                                                                                         // porcentagem (100, 90,
                                                                                         // etc...)
                vm.query.conditions.available = true;                          // Disponível (hardcoded por enquanto)
                // extrainfo
                vm.query.conditions.season = $stateParams.temporada;       // Temporada (Verão, Inverno 2015, etc...)
                vm.query.conditions.length = $stateParams.comprimento;     // Comprimento (5cm, 10cm, etc...)
                vm.query.conditions.collar = $stateParams.gola;            // Gola (em V, canoa, etc...)
                vm.query.conditions.sleeve = $stateParams.manga;           // Manga (curta, longa, etc...)
                vm.query.conditions.style = $stateParams.estilo;           // Estilo (Dramática, moderno, etc...)
                vm.query.conditions.pipe = $stateParams.cano;              // Cano (5cm, alto, etc...)
                vm.query.conditions.heelHeight = $stateParams.saltoAltura; // Altura do salto (5cm, 8cm, etc...)
                vm.query.conditions.heel = $stateParams.salto;             // Tipo do salto (Fino, etc...)
                vm.query.conditions.occasion = $stateParams.ocasiao;       // Ocasião (Festa, etc...)
                // Opções
                vm.query.options.orderBy = $stateParams.ordem;     // Ordenação (menorPreco, maiorDesconto, etc...)
                vm.query.options.perPage = parseInt($stateParams.porPagina); // Itens por página (40, 80, etc...)
                vm.query.options.page = parseInt($stateParams.pagina);       // Página (1, 10, etc...)
            }

            function getControlParams() {
                vm.control = {};
                vm.control.minPrice = 0;
                vm.control.maxPrice = 200000;
                vm.control.minDiscount = 0;
                vm.control.maxDiscount = 100;
                // UI
                vm.control.filters = $stateParams.filtros || false;
                $rootScope.loading = false;
                function toggleFilterPanel(value, $event) {
                    vm.control.filters = value;
                    console.log($stateParams);
                    $stateParams.filtros = value;
                    refresh($stateParams, {notify : false, reload : false, inherit : true})
                }

                vm.toggleFilterPanel = toggleFilterPanel;
            }

            function getDefaultParams() {
                vm.control.default = config.produtos.params;
            }
        }

        function validateParams() {
            var isParamsValid = helper.validateQuery(vm.query, vm.control);
            return isParamsValid;
        }

        function pageSetup() {
            // --- Filtros --- //

            // Ordenação
            setUpOrder();
            // Sliders
            setUpSliders();
            // Sexo
            setUpGenders();
            // Infantil
            setUpInfantile();
            // Cores
            setUpColors();
            // Paginação
            setUpPagination();

            // --- Informação --- //
            vm.items = [];
            //vm.results = 0;
            setUpPageTitle();

            function setUpOrder() {
                vm.control.possibleOrders = [
                    {name : 'Menor Preço', value : 'menorPreco'},
                    {name : 'Maior Preço', value : 'maiorPreco'},
                    {name : 'Menor Desconto', value : 'menorDesconto'},
                    {name : 'Maior Desconto', value : 'maiorDesconto'}
                ];
                var queryOrder = vm.query.options.orderBy;
                for(var i = 0; i < vm.control.possibleOrders.length; i += 1) {
                    if(vm.control.possibleOrders[i].value === queryOrder) {
                        vm.control.order = vm.control.possibleOrders[i];
                    }
                }
                $scope.$watch(function watchVariable() {
                    return (vm.control.order);
                }, function(current, original) {
                    if(current.value !== original.value) {
                        var currentOrderByValue = current.value;
                        return refresh({ordem : currentOrderByValue});
                    }
                });
            }

            function setUpSliders() {
                vm.control.sliders = {};
                setUpPriceRange();
                setUpDiscountRange();

                function setUpPriceRange() {
                    vm.control.sliders.price = {};
                    vm.control.sliders.price.values = [vm.query.conditions.lowPrice, vm.query.conditions.highPrice];
                    vm.control.sliders.price.previous = [].concat(vm.control.sliders.price.values);
                    vm.control.sliders.price.options = {
                        orientation : 'horizontal',
                        min         : vm.control.minPrice,
                        max         : vm.control.maxPrice,
                        step        : 100,
                        range       : true,
                        change      : function refreshPrice(event, data) {
                            // Bate com o valor anterior
                            if(vm.control.sliders.price.values[0] !== vm.control.sliders.price.previous[0]) {
                                var newlowPrice = vm.control.sliders.price.values[0];
                                return refresh({menorPreco : newlowPrice});
                            }
                            else if(vm.control.sliders.price.values[1] !== vm.control.sliders.price.previous[1]) {
                                var newHighPrice = vm.control.sliders.price.values[1];
                                return refresh({maiorPreco : newHighPrice});
                            }
                        }
                    };
                }

                function setUpDiscountRange() {
                    vm.control.sliders.discount = {};
                    vm.control.sliders.discount.values = [
                        vm.query.conditions.lowDiscount, vm.query.conditions.highDiscount
                    ];
                    vm.control.sliders.discount.previous = [].concat(vm.control.sliders.discount.values);
                    vm.control.sliders.discount.options = {
                        orientation : 'horizontal',
                        min         : vm.control.minDiscount,
                        max         : vm.control.maxDiscount,
                        step        : 1,
                        range       : true,
                        change      : function refreshDiscount(event, data) {
                            // Bate com o valor anterior
                            if(vm.control.sliders.discount.values[0] !== vm.control.sliders.discount.previous[0]) {
                                var newLowDiscount = vm.control.sliders.discount.values[0];
                                return refresh({menorDesconto : newLowDiscount});
                            }
                            else if(vm.control.sliders.discount.values[1] !== vm.control.sliders.discount.previous[1]) {
                                var newHighDiscount = vm.control.sliders.discount.values[1];
                                return refresh({maiorDesconto : newHighDiscount});
                            }
                        }
                    };
                }
            }

            function setUpGenders() {
                vm.control.possibleGenders = [
                    {
                        name     : 'Feminino',
                        value    : 'f',
                        selected : false
                    }, {
                        name     : 'Masculino',
                        value    : 'm',
                        selected : false
                    }
                ];
                var queryGender = vm.query.conditions.gender; // Array
                for(var i = 0; i < vm.control.possibleGenders.length; i += 1) {
                    if(queryGender.indexOf(vm.control.possibleGenders[i].value) !== -1) {
                        vm.control.possibleGenders[i].selected = true;
                    }
                }
                function toggleGender(gender, $event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    if($rootScope.loading === false) {
                        $rootScope.loading = true;
                        var newGender;
                        for(var i = 0; i < vm.control.possibleGenders.length; i += 1) {
                            var current = vm.control.possibleGenders[i];
                            if(current.value === gender) {
                                if(current.selected === true) {
                                    newGender = gender;
                                }
                                else {
                                    newGender = [];
                                }
                            }
                        }
                        return refresh({sexo : newGender, categoria : vm.query.conditions.category}, {inherit : false});
                    }
                }

                vm.toggleGender = toggleGender;
            }

            function setUpInfantile() {
                var queryInfantile = vm.query.conditions.infantile; // Boolean
                vm.control.infantile = {
                    name     : 'Infantil',
                    selected : queryInfantile
                };
                function toggleInfantile($event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    if($rootScope.loading === false) {
                        $rootScope.loading = true;
                        var infantile = JSON.stringify(vm.control.infantile.selected);
                        return refresh({infantil : infantile});
                    }
                }

                vm.toggleInfantile = toggleInfantile;
            }

            function setUpColors() {
                vm.control.possibleColors = [
                    {
                        name     : "Branco",
                        selected : false,
                        value    : "branco"
                    },
                    {
                        name     : "Amarelo",
                        selected : false,
                        value    : "amarelo"
                    },
                    {
                        name     : "Azul",
                        selected : false,
                        value    : "azul"
                    },
                    {
                        name     : "Cinza",
                        selected : false,
                        value    : "cinza"
                    },
                    {
                        name     : "Bege",
                        selected : false,
                        value    : "bege"
                    },
                    {
                        name     : "Laranja",
                        selected : false,
                        value    : "laranja"
                    },
                    {
                        name     : "Verde",
                        selected : false,
                        value    : "verde"
                    },
                    {
                        name     : "Marrom",
                        selected : false,
                        value    : "marrom"
                    },
                    {
                        name     : "Rosa",
                        selected : false,
                        value    : "rosa"
                    },
                    {
                        name     : "Vermelho",
                        selected : false,
                        value    : "vermelho"
                    },
                    {
                        name     : "Roxo",
                        selected : false,
                        value    : "roxo"
                    },
                    {
                        name     : "Preto",
                        selected : false,
                        value    : "preto"
                    }
                ];
                var queryColors = vm.query.conditions.colors; // Array
                for(var i = 0; i < vm.control.possibleColors.length; i += 1) {
                    if(queryColors.indexOf(vm.control.possibleColors[i].value) !== -1) {
                        vm.control.possibleColors[i].selected = true;
                    }
                }
                function toggleColor($event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    if($rootScope.loading === false) {
                        $rootScope.loading = true;
                        var colors = [];
                        for(var i = 0; i < vm.control.possibleColors.length; i += 1) {
                            var current = vm.control.possibleColors[i];
                            if(current.selected === true) {
                                colors.push(current.value);
                            }
                        }
                        return refresh({cores : colors});
                    }
                }

                vm.toggleColor = toggleColor;
            }

            function setUpPagination() {
                vm.changePage = function(newPage, $event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    return refresh({pagina : newPage});
                }
            }

            function setUpPageTitle() {
                var category = $filter('accentCorrection')(vm.query.conditions.category);
                category = $filter('capitalize')(category, false);
                // category = "Acessórios", "Calçados" ou "Vestuário"
                var plural = true;
                if(category === 'Vestuário') {
                    plural = false;
                }
                if(!!vm.query.conditions.infantile) {
                    if(plural) {
                        category += ' Infantis'
                    }
                    else {
                        category += ' Infantil'
                    }
                }
                if(vm.query.conditions.gender.indexOf('m') !== -1 && vm.query.conditions.gender.indexOf('f') === -1) {
                    if(plural) {
                        category += ' Masculinos'
                    }
                    else {
                        category += ' Masculino'
                    }
                }
                else if(vm.query.conditions.gender.indexOf('f') !== -1 && vm.query.conditions.gender.indexOf('m') === -1) {
                    if(plural) {
                        category += ' Femininos'
                    }
                    else {
                        category += ' Feminino'
                    }
                }
                vm.pageTitle = category;
            }
        }

        function pageDynamicSetup() {

            setUpTypes();
            setUpSizes();

            function setUpTypes() {
                if(vm.items.length !== 0) {
                    vm.items[0].distinct_types.sort();
                    vm.control.possibleTypes = [];
                    for(var j = 0; j < vm.items[0].distinct_types.length; j += 1) {
                        vm.control.possibleTypes.push({
                            value    : vm.items[0].distinct_types[j],
                            selected : false
                        });
                    }
                    var queryTypes = vm.query.conditions.types; // Array
                    for(var i = 0; i < vm.control.possibleTypes.length; i += 1) {
                        if(queryTypes.indexOf(vm.control.possibleTypes[i].value) !== -1) {
                            vm.control.possibleTypes[i].selected = true;
                        }
                    }
                }
                function toggleType($event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    if($rootScope.loading === false) {
                        $rootScope.loading = true;
                        var types = [];
                        for(var i = 0; i < vm.control.possibleTypes.length; i += 1) {
                            var current = vm.control.possibleTypes[i];
                            if(current.selected === true) {
                                types.push(current.value);
                            }
                        }
                        return refresh({tipos : types});
                    }
                }
                vm.toggleType = toggleType;
            }

            function setUpSizes() {
                if(vm.items.length !== 0) {
                    vm.items[0].distinct_sizes.sort();
                    vm.control.possibleSizes = [];
                    for(var j = 0; j < vm.items[0].distinct_sizes.length; j += 1) {
                        vm.control.possibleSizes.push({
                            value    : vm.items[0].distinct_sizes[j],
                            selected : false
                        });
                    }
                    var querySizes = vm.query.conditions.sizes; // Array
                    for(var i = 0; i < vm.control.possibleSizes.length; i += 1) {
                        if(querySizes.indexOf(vm.control.possibleSizes[i].value) !== -1) {
                            vm.control.possibleSizes[i].selected = true;
                        }
                    }
                }
                function toggleSize($event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    if($rootScope.loading === false) {
                        $rootScope.loading = true;
                        var sizes = [];
                        for(var i = 0; i < vm.control.possibleSizes.length; i += 1) {
                            var current = vm.control.possibleSizes[i];
                            if(current.selected === true) {
                                sizes.push(current.value);
                            }
                        }
                        return refresh({tamanhos : sizes});
                    }
                }

                vm.toggleSize = toggleSize;
            }

        }

        function sentimentSetup() {
            if(!!user && !!vm.items) {
                var likes = user.likes;
                var dislikes = user.dislikes;
                for(var i = 0; i < vm.items.length; i += 1) {
                    var id_externo = vm.items[i].id_externo;
                    var loja = vm.items[i].loja;
                    vm.items[i].liked = false;
                    vm.items[i].disliked = false;
                    for(var j = 0; j < likes.length; j += 1) {
                        if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                            vm.items[i].liked = true;
                        }
                    }
                    for(j = 0; j < dislikes.length; j += 1) {
                        if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                            vm.items[i].disliked = true;
                        }
                    }
                }
            }
        }

        function SEOSetup() {
            seo.setTitle('Mooi.me - ' + vm.pageTitle);
            var pageTitle = vm.pageTitle.toLowerCase();
            pageTitle = $filter('capitalize')(pageTitle, false);
            seo.setDescription(pageTitle + ' de várias lojas na Mooi.me. Compare preços e encontre os melhores produtos!');
            seo.setURL();
            var imageName = vm.pageTitle;
            imageName = imageName.replace('ó', 'o').replace('ç', 'c').replace('á', 'a');
            imageName = imageName.split(' ').join('');
            seo.setImage('http://' + config.mooiweb.host + '/img/' + imageName + '.jpg', 640, 425, true);
            seo.setPrice();
        }

        function getItems() {
            $rootScope.loading = true;
            db.getItems(vm.query)
                .then(function(response) { // Sucesso
                    vm.items = response;
                    sentimentSetup();
                    pageDynamicSetup();
                    SEOSetup();
                    if(response.length === 0) {
                        vm.results = 0;
                    }
                    else {
                        vm.results = vm.items[0].item_count;
                    }
                    return vm.items;
                }, function(reason) { // Falha
                    vm.error = reason.message;
                    return vm.error;
                })['finally'](function() {
                $rootScope.loading = false;
                window.prerenderReady = true;
            });
        }

        function resetAllFilters($event) {
            if($event) {
                $event.stopPropagation();
                $event.preventDefault();
            }
            var safeParameters = [
                'categoria',
                'sexo',
                'infantil',
                'ordem',
                'porPagina'
            ];
            var newParameters = {};
            for(var parameter in $stateParams) {
                if($stateParams.hasOwnProperty(parameter)) {
                    // Não está na lista de parâmetros não resetáveis
                    if(safeParameters.indexOf(parameter) === -1) {
                        newParameters[parameter] = vm.control.default[parameter].value;
                    }
                }
            }
            return refresh(newParameters);
        }

        function resetFilters(filters, $event) {
            if($event) {
                $event.stopPropagation();
                $event.preventDefault();
            }
            var newParameters = {};
            for(var i = 0; i < filters.length; i += 1) {
                newParameters[filters[i]] = vm.control.default[filters[i]].value;
            }
            return refresh(newParameters);
        }

        function refresh(params, options) {
            if(params.pagina === undefined) {
                params.pagina = 1;
            }
            options = options || {notify : true, reload : true};
            //return refresh({menorDesconto : 10});
            return $state.go('.', params, options);
        }

        function setItemViewed(item) {
            setRelated(item);
            return storage.setViewed(item);
        }

        function setRelated(item) {
            return storage.setRelated(item, vm.items);
        }

        function likeItem(item) {
            return sentiment(item, 'likeItem');
        }

        function dislikeItem(item) {
            return sentiment(item, 'dislikeItem');
        }

        function sentiment(item, sentimentFunction) {
            //$rootScope.loading = true;
            var id = {id_externo : item.id_externo, loja : item.loja};
            db[sentimentFunction](id)
                .then(function(newUser) { // Sucesso
                    user = newUser;
                    sentimentSetup();
                }, function(reason) { // Falha
                    console.log(reason);
                })['finally'](function() {
                //$rootScope.loading = false;
            });
        }

        function conversion(item) {
            var category = item.macro_categorias[0];
            var store = item.loja;
            bi.facebookPixel.conversion();
            bi.analytics.sendEvent(category, "Clique", store);
        }

        function interest(item) {
            var category = item.macro_categorias[0];
            var store = item.loja;
            bi.analytics.sendEvent(category, "Ver mais", store);
        }

        function getImageURL(item, index) {
            if(!item || isNaN(parseInt(index))) {
                return '';
            }
            if(item.images[index].indexOf('s3-sa-east-1.amazonaws.com') > -1) {
                return item.images[index];
            }
            else {
                var url = 'http://s3-sa-east-1.amazonaws.com/mooi-items/images/produtos/';
                url += item.loja;
                url += '/';
                url += item.images[index];
                return url;
            }
        }

    }
})();