(function() {
    'use strict';

    angular.
        module('mooiweb.produtos').
        config(produtosRouteConfig);

    produtosRouteConfig.$inject = ['$stateProvider', 'configProvider', 'securityProvider'];
    function produtosRouteConfig($stateProvider, configProvider, securityProvider) {
        var security = securityProvider.$get();
        var config = configProvider.$get();
        $stateProvider
            .state('root.produtos', {
                url    : '/produtos/:categoria?sexo&infantil&tipos&marcas&lojas&cores&tamanhos&menorDesconto' +
                '&maiorDesconto&menorPreco&maiorPreco&temporada&comprimento&gola&manga&estilo&cano&saltoAltura' +
                '&salto&ocasiao&ordem&porPagina&pagina',
                views  : {
                    'container@' : {
                        templateUrl  : 'source/produtos/produtos.html',
                        controller   : 'Produtos',
                        controllerAs : 'vm'
                    }
                },
                params : config.produtos.params,
                resolve : {
                    user : security.checkLoggedIn
                },
                data: {
                    title: 'Produtos'
                }
            });
    }
})();