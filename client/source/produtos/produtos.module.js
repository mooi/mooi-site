(function() {
    'use strict';
    angular
        .module('mooiweb.produtos', ['mooiweb.config', 'mooiweb.security']);
})();