(function() {
    'use strict';
    angular.module('mooiweb.home')
        .controller('Home', Home);

    Home.$inject = ['$rootScope', '$scope', 'seo', 'bi', 'config', 'helper', 'ui', 'usuario', 'user'];
    function Home($rootScope, $scope, seo, bi, config, helper, ui, usuario, user) {
        var vm = this;
        vm.accessories = "";
        vm.clothing = "";
        vm.shoes = "";
        vm.userName = usuario.getUserName(user);
        vm.userGender = usuario.getUserGender(user);
        vm.searchEnabled = config.mooiweb.searchEnabled;

        init();

        /////////////////////////

        function init() {
            getRandomPhotos();
            ui.loadUI("Home");
            seo.setTitle('Mooi.me');
            seo.setDescription('A sua vitrine da moda na internet');
            seo.setURL();
            seo.setImage('quadrado-medio.png', 252, 252, false);
            seo.setPrice();
            $scope.$on('userChanged', function(event, user) {
                vm.user = user;
                vm.userName = usuario.getUserName(user);
                vm.userGender = usuario.getUserGender(user);
            });
            $rootScope.statusCode = 200;
            window.prerenderReady = true;
        }

        function getRandomPhotos() {
            var accessories = [
                "acessorios1.jpg",
                "acessorios2.jpg",
                "acessorios3.jpg"
            ];
            vm.accessories = helper.getRandomFromArray(accessories);
            var clothing = [
                "roupas1.jpg",
                "roupas2.jpg",
                "roupas3.jpg"
            ];
            vm.clothing = helper.getRandomFromArray(clothing);
            var shoes = [
                "sapatos1.jpg",
                "sapatos2.jpg",
                "sapatos3.jpg"
            ];
            vm.shoes = helper.getRandomFromArray(shoes);
        }

    }

})();