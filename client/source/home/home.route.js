(function() {
    'use strict';

    angular.
        module('mooiweb.home').
        config(homeRouteConfig);

    homeRouteConfig.$inject = ['$stateProvider', 'securityProvider'];
    function homeRouteConfig($stateProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider
            .state('root.home', {
                url    : '/home?facebookLogin',
                views  : {
                    'container@' : {
                        templateUrl  : 'source/home/home.html',
                        controller   : 'Home',
                        controllerAs : 'vm'
                    }
                },
                params : {
                    facebookLogin : {
                        value  : null,
                        squash : true
                    }
                },
                resolve : {
                    user : security.checkLoggedIn
                },
                data: {
                    title: 'A sua vitrine da moda na internet'
                }
            });
    }

})();