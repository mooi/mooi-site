(function() {
    'use strict';

    describe("The Home controller", function() {

        var db;
        var config;
        var $httpBackend;
        var Home;
        var $scope;

        beforeEach(module('mooiweb'));

        beforeEach(inject(function(_db_, _config_, _$httpBackend_, $controller, $rootScope) {
            db = _db_;
            config = _config_;
            $httpBackend = _$httpBackend_;
            $scope = $rootScope.$new();
            Home = $controller('Home', {
                $scope : $scope
            });
        }));

        it("should load", function() {
            expect(Home).toBeDefined();
        });

        xit("should initialize", function(done) {
            var url = config.postgreSQLService.item.getMany;
            $httpBackend.whenGET(url + "?controller=Hom").respond(200, {success : true});

            expect(Home.init).toBeDefined();
            expect(db.getItems).toBeDefined();
            var promise = Home.init();
            $scope.$root.$digest();
            promise.then(function(response) {
                console.log(response);
                done.fail(response);
            }, function(reason) {
                console.log(reason);
                done.fail(reason);
            });
        }, 3000000);

    });

})();