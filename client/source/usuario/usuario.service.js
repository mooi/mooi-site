(function() {
    'use strict';

    angular
        .module('mooiweb.usuario')
        .factory('usuario', usuario);

    usuario.$inject = [];
    function usuario() {
        var service = {
            // Itens
            getUserName   : getUserName,
            getUserGender : getUserGender,
            getUserEmail  : getUserEmail,
        };

        return service;
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function getUserName(user) {
            var userName;
            if(!user) {
                userName = 'Usuário(a)'
            }
            else if(!!user.local) {
                userName = user.local.name;
            }
            else if(!!user.facebook) {
                userName = user.facebook.name;
            }
            else {
                userName = 'Usuário(a)'
            }
            return userName;
        }

        function getUserGender(user) {
            var userGender = {};
            if(!user) {
                userGender.char = null;
            }
            else if(!!user.local) {
                if(user.local.gender === 'Feminino') {
                    userGender.char = 'f';
                }
                else if(user.local.gender === 'Masculino') {
                    userGender.char = 'f';
                }
                else {
                    userGender.char = null;
                }
            }
            else if(!!user.facebook) {
                if(user.facebook.gender.toLowerCase() === 'female') {
                    userGender.char = 'f';
                }
                else if(user.facebook.gender.toLowerCase() === 'male') {
                    userGender.char = 'm';
                }
                else {
                    userGender.char = null;
                }
                // TODO: Verificar as zilhares de possibilidades do facebook
            }
            return userGender;
        }

        function getUserEmail(user) {
            if(!user) {
                return null;
            }
            if(!!user.local) {
                return user.local.email;
            }
            if(!!user.facebook) {
                return user.facebook.email;
            }
        }

    }
})();
