(function() {
    'use strict';

    angular
        .module('mooiweb.usuario')
        .config(usuarioRouteConfig);

    usuarioRouteConfig.$inject = ['$stateProvider', 'securityProvider'];
    function usuarioRouteConfig($stateProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider
            .state('root.usuario', {
                url     : '/usuario/:id',
                views   : {
                    'container@' : {
                        templateUrl  : 'source/usuario/usuario.html',
                        controller   : 'Usuario',
                        controllerAs : 'vm'
                    }
                },
                params  : {},
                resolve : {
                    user : security.ensureLoggedIn
                }
            });
    }

})();