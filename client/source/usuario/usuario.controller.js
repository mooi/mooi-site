(function() {
    'use strict';
    angular.module('mooiweb.usuario')
        .controller('Usuario', Usuario);

    Usuario.$inject = ['$rootScope', '$scope', 'ui', 'storage', 'db', 'usuario', 'user'];
    function Usuario($rootScope, $scope, ui, storage, db, usuario, user) {
        var vm = this;
        vm.user = user;
        vm.email = usuario.getUserEmail(user);
        vm.name = usuario.getUserName(user);
        vm.viewed = storage.getViewed();
        vm.liked = [];
        vm.suggested = [];
        vm.setItemViewed = setItemViewed;
        vm.likeItem = likeItem;
        vm.dislikeItem = dislikeItem;
        $rootScope.title = vm.name;
        $rootScope.statusCode = 200;

        init();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function init() {
            ui.loadUI('Usuario');
            getLikedItems();
            $scope.$on('ALWAYS', function(event) {
                ui.loadUI('Usuario');
            });
        }

        function getLikedItems() {
            var likedList = [];
            var liked = user.likes;
            for(var i = 0; i < liked.length; i += 1) {
                likedList.push({
                    id: liked[i].id_externo,
                    store: liked[i].loja
                });
            }
            if(likedList.length > 0) {
                db.getItemsById(likedList)
                    .then(function(response) { // Sucesso
                        vm.liked = response;
                        ui.loadUI('Usuario');
                        return vm.liked;
                    }, function(reason) { // Falha
                        //vm.error = reason.message;
                        //return vm.error;
                    })['finally'](function() {
                    sentimentSetup();
                    //$rootScope.loading = false;
                });
            }
            sentimentSetup();
        }

        function setItemViewed(item) {
            return storage.setViewed(item);
        }

        function likeItem(item) {
            return sentiment(item, 'likeItem');
        }

        function dislikeItem(item) {
            return sentiment(item, 'dislikeItem');
        }

        function sentiment(item, sentimentFunction) {
            var id = {id_externo : item.id_externo, loja : item.loja};
            db[sentimentFunction](id)
                .then(function(newUser) { // Sucesso
                    user = newUser;
                    sentimentSetup();
                }, function(reason) { // Falha
                    console.log(reason);
                })['finally'](function() {
                //$rootScope.loading = false;
            });
        }

        function sentimentSetup() {
            if(!!user) {
                var likes = user.likes;
                var dislikes = user.dislikes;
                var i, j;
                var id_externo;
                var loja;
                // TODO: otimizar isso
                if(!!vm.viewed) {
                    for(i = 0; i < vm.viewed.length; i += 1) {
                        id_externo = vm.viewed[i].id_externo;
                        loja = vm.viewed[i].loja;
                        vm.viewed[i].liked = false;
                        vm.viewed[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.viewed[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.viewed[i].disliked = true;
                            }
                        }
                    }
                }

                if(!!vm.liked) {
                    for(i = 0; i < vm.liked.length; i += 1) {
                        id_externo = vm.liked[i].id_externo;
                        loja = vm.liked[i].loja;
                        vm.liked[i].liked = false;
                        vm.liked[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.liked[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.liked[i].disliked = true;
                            }
                        }
                    }
                }

                if(!!vm.suggested) {
                    for(i = 0; i < vm.suggested.length; i += 1) {
                        id_externo = vm.suggested[i].id_externo;
                        loja = vm.suggested[i].loja;
                        vm.suggested[i].liked = false;
                        vm.suggested[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.suggested[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.suggested[i].disliked = true;
                            }
                        }
                    }
                }

            }
        }

    }

})();