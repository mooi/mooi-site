(function() {
    'use strict';

    angular.
        module('mooiweb.cadastro').
        config(cadastroRouteConfig);

    cadastroRouteConfig.$inject = ['$stateProvider', 'securityProvider'];
    function cadastroRouteConfig($stateProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider
            .state('root.cadastro', {
                url   : '/cadastro',
                views : {
                    'container@' : {
                        templateUrl  : 'source/cadastro/cadastro.html',
                        controller   : 'Cadastro',
                        controllerAs : 'vm'
                    }
                },
                resolve : {
                    notLogged : security.ensureNotLoggedIn
                },
                data: {
                    title: 'Cadastro'
                }
            });
    }

})();