(function() {
    'use strict';
    angular.module('mooiweb.cadastro')
        .controller('Cadastro', Cadastro);

    Cadastro.$inject = ['$rootScope', '$state', 'ui', 'db', 'helper', 'seo', 'notLogged'];
    function Cadastro($rootScope, $state, ui, db, helper, seo, notLogged) {
        var vm = this;
        vm.form = {};
        vm.form.gender = 'Feminino';
        vm.submit = submit;

        init();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function init() {
            ui.loadUI('Cadastro');
            seo.setTitle('Mooi.me - Cadastro');
            seo.setDescription('Cadastre-se na Mooi.me! Mantenha seus itens favoritos salvos e receba recomendações.');
            seo.setURL();
            seo.setImage('quadrado-medio.png', 252, 252, false);
            seo.setPrice();
            helper.log('Cadastro loaded - ' + notLogged);
            $rootScope.statusCode = 200;
            window.prerenderReady = true;
        }

        function submit(signupForm) {
            if(!!signupForm.$valid) {
                $rootScope.loading = true;
                db.signup(vm.form)
                    .then(function(user) {
                        if(!!user) {
                            return $state.go('root.home', {}, {inherit : false});
                        }
                    })['finally'](function() {
                    $rootScope.loading = false;
                });
            }
        }

    }

})();