(function() {
    'use strict';
    angular.
        module('mooiweb.seo').
        factory('seo', seo);

    seo.$inject = ['$rootScope', '$location'];

    function seo($rootScope, $location) {
        var service = {
            setPrice : setPrice,
            setTitle : setTitle,
            setDescription : setDescription,
            setAuthor : setAuthor,
            setURL : setURL,
            setImage : setImage
        };

        init();

        return service;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function init() {
            $rootScope.og = {};
            $rootScope.og.siteName = 'Mooi.me';
            $rootScope.schema = {};
            $rootScope.author = 'Mooi.me'
        }

        function setTitle(title) {
            //title = 'Mooi.me - ' + title;
            $rootScope.title = title;
            $rootScope.og.title = title;
            $rootScope.schema.name = title;
        }

        function setPrice(newPrice) {
            var price = parseInt(newPrice); // 1999
            if(isNaN(price)) {
                $rootScope.og.price = null;
            }
            else {
                var reais = Math.floor(price/100); // 19
                var centavos = price - (reais * 100); // 1999 - 1900 = 99
                $rootScope.og.price = reais + '.' + centavos; // 19.99
            }
        }

        function setDescription(description) {
            $rootScope.description = description;
            $rootScope.og.description = description;
            $rootScope.schema.description = description;
        }

        function setAuthor(author) {
            $rootScope.author = author;
        }

        function setURL(URL) {
            if(!!URL) {
                $rootScope.og.url = URL;
            }
            else {
                $rootScope.og.url = $location.absUrl();
            }
        }

        function setImage(image, width, height, external) {
            var imageURL;
            if(!!external) {
                imageURL = image;
            }
            else {
                imageURL = 'http://' + $location.host() + '/img/' + image; // http://www.mooi.me/img/imagem.png
            }
            $rootScope.og.image = imageURL;
            $rootScope.schema.image = imageURL;
            $rootScope.og.width = width;
            $rootScope.og.height = height;
        }

    }
})();