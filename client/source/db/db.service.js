(function() {
    'use strict';

    angular
        .module('mooiweb.db')
        .factory('db', db);

    db.$inject = ['$http', '$state', '$filter', 'helper', 'config', 'security', 'ui'];
    function db($http, $state, $filter, helper, config, security, ui) {
        var service = {
            // Itens
            getItems     : getItems,
            getItemsById : getItemsById,
            getTestOK    : getTestOK,
            getTestError : getTestError,
            // Usuário
            signup       : signup,
            login        : login,
            isLoggedIn   : isLoggedIn,
            logout       : logout,
            likeItem     : likeItem,
            dislikeItem  : dislikeItem,
            // Email
            subscribe    : subscribe
        };

        return service;
        /////////////////////

        // Itens

        function getItems(originalQuery) {
            if(originalQuery === undefined) {
                throw new Error('db.service.js:getItems - No query passed');
            }
            var newQuery = JSON.parse(JSON.stringify(originalQuery));
            newQuery.conditions.category = $filter('accentCorrection')(newQuery.conditions.category);
            var queryParameters = {
                method  : 'GET',
                url     : config.postgreSQLService.item.getMany,
                params  : newQuery,
                headers : {'Authorization' : 'Token token=xxxxYYYYZzzz'},
                timeout : 10000
            };
            var query = $http(queryParameters);
            // TODO: Cache
            return query.then(function(response) {
                // {config: ..., data: data, headers: ..., status: 200, statusText: "OK"}//
                return response.data.data;
            }, function(reason) {
                // TODO: Enviar o erro para um servidor de análise
                var errorMessage;
                switch(reason.status) {
                    case -1 :
                        errorMessage = "Problema de conexão. Tente novamente mais tarde.";
                        helper.warning(errorMessage);
                        return [];
                        break;
                    case 400 :
                        errorMessage = reason.data.error || reason.data;
                        helper.warning(errorMessage);
                        break;
                    default :
                        errorMessage = reason.data.error || reason.data;
                        if(config.mooiweb.environment === 'development') {
                            helper.warning(errorMessage);
                        }
                        break;
                }
                throw new Error(errorMessage);
            });
        }

        function getItemsById(list) {
            if(list === undefined) {
                throw new Error('db.service.js:getItemsById - No conditions passed');
            }
            var queryParameters = {
                method  : 'GET',
                url     : config.postgreSQLService.item.getManyById,
                params  : {data : JSON.stringify(list)},
                headers : {'Authorization' : 'Token token=xxxxYYYYZzzz'},
                timeout : 100000
            };
            var query = $http(queryParameters);
            // TODO: Cache
            return query.then(function(response) {
                // {config: ..., data: data, headers: ..., status: 200, statusText: "OK"}//
                var item = response.data.data[0];
                if(item.macro_categorias[0] === 'vestuário') {
                    item.macro_categorias[0] = 'vestuario';
                }
                else if(item.macro_categorias[0] === 'calçados') {
                    item.macro_categorias[0] = 'calcados';
                }
                else if(item.macro_categorias[0] === 'acessórios') {
                    item.macro_categorias[0] = 'acessorios';
                }
                return item;
            }, function(reason) {
                // TODO: Enviar o erro para um servidor de análise
                var errorMessage;
                switch(reason.status) {
                    case -1 :
                        errorMessage = "Problema de conexão. Tente novamente mais tarde.";
                        helper.warning(errorMessage);
                        return [];
                        break;
                    case 400 :
                        errorMessage = reason.data.error || reason.data;
                        helper.warning(errorMessage);
                        break;
                    default :
                        errorMessage = reason.data.error || reason.data;
                        if(config.mooiweb.environment === 'development') {
                            helper.warning(errorMessage);
                        }
                        break;
                }
                throw new Error(errorMessage);
            });
        }

        function getTestOK() {
            var queryParameters = {
                method  : 'GET',
                url     : config.postgreSQLService.test.ok,
                timeout : 3000
            };
            var query = $http(queryParameters);

            return query.then(function(response) {
                return response.data;
            }, function(reason) {
                return reason.data;
            });
        }

        function getTestError() {
            var queryParameters = {
                method  : 'GET',
                url     : config.postgreSQLService.test.error,
                timeout : 3000
            };
            var query = $http(queryParameters);

            return query.then(function(response) {
                return response.data;
            }, function(reason) {
                return reason.data;
            });
        }

        function getLikedItems(user) {

        }

        // Usuário

        function getUserOK(response) {
            // {config: ..., data: data, headers: ..., status: 200, statusText: "OK"}//
            if(!!response.data.success) {
                var newUser = response.data.data;
                return updateUser(newUser);
            }
            else {
                var message;
                if(response.status == 200) {
                    message = response.data.data;
                    helper.warning(message);
                    return updateUser(false);
                }
                else {
                    // Erro que não deve aparecer para o usuário
                    message = response.data.data || response.data.error || response.data.message;
                    helper.error(message);
                    return updateUser(false);
                }
            }
        }

        function getUserError(reason) {
            var message;
            if(reason.status == -1) {
                message = "Problema de conexão. Tente novamente mais tarde.";
                helper.warning(message);
                return updateUser(false);
            }
            else {
                // Erro que não deve aparecer para o usuário
                message = response.data.data || response.data.error || response.data.message;
                helper.error(message);
                return updateUser(false);
            }
        }

        function signup(user) {
            if(user === undefined) {
                throw new Error('db.service.js:signup - No user passed');
            }
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.user.signup,
                data    : user,
                headers : {'Authorization' : 'Token token=xxxxYYYYZzzz'},
                timeout : 100000
            };
            var query = $http(queryParameters);
            return query.then(getUserOK, getUserError);
        }

        function login(user) {
            if(user === undefined) {
                throw new Error('db.service.js:login - No user passed');
            }
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.user.login,
                data    : user,
                headers : {'Authorization' : 'Token token=xxxxYYYYZzzz'},
                timeout : 100000
            };
            var query = $http(queryParameters);
            return query.then(getUserOK, getTestError);
        }

        function isLoggedIn() {
            var queryParameters = {
                method  : 'GET',
                url     : config.mongodb.user.isLoggedIn,
                timeout : 100000
            };
            var query = $http(queryParameters);

            return query.then(getUserOK, getTestError);
        }

        function logout() {
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.user.logout,
                timeout : 100000
            };
            var query = $http(queryParameters);
            return query.then(function() {
                $state.go('root.home');
                helper.info('Volte sempre :)');
                return updateUser(false);
            }, function(reason) {
                var message;
                if(reason.status == -1) {
                    message = "Problema de conexão. Tente novamente mais tarde.";
                    helper.warning(message);
                    $state.go('root.home');
                    return updateUser(false);
                }
                else {
                    // Erro que não deve aparecer para o usuário
                    message = reason.data.data || reason.data.error || reason.data.message;
                    helper.error(message);
                    $state.go('root.home');
                    return updateUser(false);
                }
            });
        }

        function itemSentiment(queryParameters) {
            var query = $http(queryParameters);
            return query.then(function(response) {
                var success = response.data.success;
                var user = response.data.data;
                if(success === true) {
                    return updateUser(user);
                }
                else {
                    var message;
                    if(response.status == 200) {
                        // Não está logado
                        ui.openLoginModal();
                    }
                    else {
                        // Erro que não deve aparecer para o usuário
                        message = response.data.data || response.data.error || response.data.message;
                        helper.error(message);
                        helper.warning('Houve algum erro. Tente novamente mais tarde.');
                    }
                }
            }, function(reason) {
                var message;
                if(reason.status == -1) {
                    message = "Problema de conexão. Tente novamente mais tarde.";
                    helper.warning(message);
                    return updateUser(false);
                }
                else {
                    // Erro que não deve aparecer para o usuário
                    message = response.data.data || response.data.error || response.data.message;
                    helper.error(message);
                    return updateUser(false);
                }
            });
        }

        function likeItem(item) {
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.user.likeItem,
                data    : item,
                timeout : 100000
            };
            return itemSentiment(queryParameters);
        }

        function dislikeItem(item) {
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.user.dislikeItem,
                data    : item,
                timeout : 100000
            };
            return itemSentiment(queryParameters);
        }

        function updateUser(user) {
            security.updateUser(user);
            helper.broadcast("userChanged", user);
            return user;
        }

        // Email

        function subscribe(email) {
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.subscription.subscribe,
                data    : {email : email},
                timeout : 100000
            };
            var query = $http(queryParameters);
            return query.then(function(response) {
                if(!response.data.success) {
                    helper.warning(response.data.data);
                }
                else {
                    helper.info('Inscrição realizada com sucesso!');
                }
                return response.data.success;
            }, function(reason) {
                var message;
                if(reason.status == -1) {
                    message = "Problema de conexão. Tente novamente mais tarde.";
                    helper.warning(message);
                }
                else {
                    // Erro que não deve aparecer para o usuário
                    message = response.data.data || response.data.error || response.data.message;
                    helper.error(message);
                }
                return false;
            });
        }
    }

})();
