(function() {
    'use strict';

    describe("The db module", function() {

        var db;
        var config;
        var $httpBackend;

        beforeEach(module('mooiweb'));

        beforeEach(inject(function(_db_, _config_, _$httpBackend_) {
            db = _db_;
            config = _config_;
            $httpBackend = _$httpBackend_;
        }));

        it("should load", function() {
            expect(db).toBeDefined();
            expect($httpBackend).toBeDefined();
        });

        describe("when sending item requests", function() {

            var url;

            beforeEach(function() {
                url = config.postgreSQLService.item.getMany;
                $httpBackend.whenGET(url).respond(200, {success : true});
            });

            it("should load /getItems", function() {
                expect(db.getItems).toBeDefined();
            });

            it("should send a request for items with conditions", function() {
                try {
                    db.getItems();
                    $httpBackend.flush();
                    fail("Should have thrown an error");
                }
                catch(error) {
                    expect(error).toMatch(/No conditions passed/);
                }
            });

            it("should throw an error when sending requests for items with no conditions", function(done) {
                $httpBackend.expectGET(url);
                db.getItems({}).then(function(response) {
                    expect(response.success).toBe(true);
                    done();
                });
                $httpBackend.flush();
            }, 3000);

        });

        afterEach(function() {
            //$httpBackend.verifyNoOutstandingExpectation() dá um erro que acredito ser um bug do próprio angular
            // Utilizar done() para controlar as expectations
            //$httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

    });

})();