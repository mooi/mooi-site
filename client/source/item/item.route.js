(function() {
    'use strict';

    angular.
        module('mooiweb.item').
        config(itemRouteConfig);

    itemRouteConfig.$inject = ['$stateProvider', 'securityProvider'];
    function itemRouteConfig($stateProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider
            .state('root.item', {
                url    : '/item/:loja/:id',
                views  : {
                    'container@' : {
                        templateUrl  : 'source/item/item.html',
                        controller   : 'Item',
                        controllerAs : 'vm'
                    }
                },
                params : {},
                resolve : {
                    user : security.checkLoggedIn
                }
            });
    }
})();