(function() {
    'use strict';
    angular
        .module('mooiweb.item')
        .controller('Item', Item);

    Item.$inject = ['$window', '$scope', '$rootScope', '$stateParams', '$filter', 'ui', 'db', 'storage', 'seo', 'bi', 'user'];
    function Item($window,  $scope, $rootScope, $stateParams, $filter, ui, db, storage, seo, bi, user) {
        var vm = this;
        vm.photoIndex = 0; // Qual foto da lista de fotos aparece
        vm.changeImage = changeImage;
        vm.setItemViewed = setItemViewed;
        vm.query = [{id: $stateParams.id, store: $stateParams.loja}];
        vm.viewed = storage.getViewed();
        vm.related = [];
        vm.suggested = [];
        vm.likeItem = likeItem;
        vm.dislikeItem = dislikeItem;
        vm.url = $window.location.origin + $window.location.pathname;
        vm.conversion = conversion;
        vm.interest = interest;
        vm.getImageURL = getImageURL;
        $rootScope.statusCode = 200;

        init();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////


        function init() {
            ui.loadUI('Item');
            getItem();
            $scope.$on('ALWAYS', function(event) {
                ui.loadUI('Item');
            });
        }

        function getItem() {
            $rootScope.loading = true;
            db.getItemsById(vm.query)
                .then(function(item) { // Sucesso
                    seo.setTitle('Mooi.me - ' + item.nome);
                    seo.setDescription($filter('lineBreakCorrection')(item.descricao, true));
                    seo.setURL();
                    seo.setImage(item.images[0], 252, 252, true);
                    seo.setPrice(item.preco_vista);
                    vm.item = item;
                    vm.related = storage.getRelated(vm.item);
                    ui.singleProductUI(vm.item.loja);
                    sentimentSetup();
                    return vm.item;
                }, function(reason) { // Falha
                    vm.error = reason.message;
                    return vm.error;
                })['finally'](function() {
                $rootScope.loading = false;
                window.prerenderReady = true;
            });
        }

        function changeImage(index) {
            vm.photoIndex = index;
        }

        function setItemViewed(item) {
            return storage.setViewed(item);
        }

        function likeItem(item) {
            return sentiment(item, 'likeItem');
        }

        function dislikeItem(item) {
            return sentiment(item, 'dislikeItem');
        }

        function sentiment(item, sentimentFunction) {
            var id = {id_externo : item.id_externo, loja : item.loja};
            db[sentimentFunction](id)
                .then(function(newUser) { // Sucesso
                    user = newUser;
                    sentimentSetup();
                }, function(reason) { // Falha
                    console.log(reason);
                })['finally'](function() {
                //$rootScope.loading = false;
            });
        }

        function sentimentSetup() {
            if(!!user) {
                var likes = user.likes;
                var dislikes = user.dislikes;
                var i, j;
                var id_externo;
                var loja;
                // TODO: otimizar isso
                for(i = 0; i < likes.length; i += 1) {
                    if(vm.item.id_externo === likes[i].id_externo && vm.item.loja === likes[i].loja) {
                        vm.item.liked = true;
                    }
                }
                for(i = 0; i < dislikes.length; i += 1) {
                    if(vm.item.id_externo === dislikes[i].id_externo && vm.item.loja === dislikes[i].loja) {
                        vm.item.disliked = true;
                    }
                }
                if(!!vm.viewed) {
                    for(i = 0; i < vm.viewed.length; i += 1) {
                        id_externo = vm.viewed[i].id_externo;
                        loja = vm.viewed[i].loja;
                        vm.viewed[i].liked = false;
                        vm.viewed[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.viewed[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.viewed[i].disliked = true;
                            }
                        }
                    }
                }

                if(!!vm.related) {
                    for(i = 0; i < vm.related.length; i += 1) {
                        id_externo = vm.related[i].id_externo;
                        loja = vm.related[i].loja;
                        vm.related[i].liked = false;
                        vm.related[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.related[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.related[i].disliked = true;
                            }
                        }
                    }
                }

                if(!!vm.suggested) {
                    for(i = 0; i < vm.suggested.length; i += 1) {
                        id_externo = vm.suggested[i].id_externo;
                        loja = vm.suggested[i].loja;
                        vm.suggested[i].liked = false;
                        vm.suggested[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.suggested[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.suggested[i].disliked = true;
                            }
                        }
                    }
                }

            }
        }

        function conversion(item) {
            var category = item.macro_categorias[0];
            var store = item.loja;
            bi.facebookPixel.conversion();
            bi.analytics.sendEvent(category, "Clique", store);
        }

        function interest(item) {
            var category = item.macro_categorias[0];
            var store = item.loja;
            bi.analytics.sendEvent(category, "Ver mais", store);
        }

        function getImageURL(item, index) {
            if(!item || isNaN(parseInt(index))) {
                return '';
            }
            if(item.images[index].indexOf('s3-sa-east-1.amazonaws.com') > -1) {
                return item.images[index];
            }
            else {
                var url = 'http://s3-sa-east-1.amazonaws.com/mooi-items/images/produtos/';
                url += item.loja;
                url += '/';
                url += item.images[index];
                return url;
            }
        }

    }
})();