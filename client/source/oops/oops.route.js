(function() {
    'use strict';

    angular.
        module('mooiweb.oops').
        config(oopsRouteConfig);

    oopsRouteConfig.$inject = ['$stateProvider', 'securityProvider'];
    function oopsRouteConfig($stateProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider
            // Para que a página ignore os arquivos header e footer, mostrando apenas a página oops, usar
            // 'oops' ao invés de 'root.oops'
            .state('root.oops', {
                url   : '/oops',
                views : {
                    'container@' : {
                        templateUrl  : 'source/oops/oops.html',
                        controller   : 'Oops',
                        controllerAs : 'vm'
                    }
                },
                resolve : {
                    user : security.checkLoggedIn
                },
                data: {
                    title: 'Página não encontrada!'
                }
            });
    }

})();