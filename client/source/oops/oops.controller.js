(function() {
    'use strict';
    angular.module('mooiweb.oops')
        .controller('Oops', Oops);

    Oops.$inject = ['$rootScope', 'seo', 'ui', 'user'];
    function Oops($rootScope, seo, ui, user) {
        var vm = this;
        seo.setTitle('Mooi.me - Página não encontrada');
        seo.setDescription('Página não encontrada!');
        seo.setURL();
        seo.setImage('quadrado-medio.png', 252, 252, false);
        seo.setPrice();
        init();

        function init() {
            $rootScope.statusCode = 404;
            ui.loadUI('Oops');
            window.prerenderReady = true;
        }
    }

})();