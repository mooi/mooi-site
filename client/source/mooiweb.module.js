(function() {
    'use strict';
    angular
        .module('mooiweb', [
            'mooiweb.core',       // Módulos do Angular e 3rd party
            'mooiweb.layout',     // Index, header e footer
            'mooiweb.home',       // Home
            'mooiweb.oops',       // 404
            'mooiweb.produtos',   // Página de produtos
            'mooiweb.usuario',    // Página de usuário
            'mooiweb.item',       // Página individual de produto
            'mooiweb.cadastro',   // Página de cadastro
            'mooiweb.db',         // Serviço de interface com o PostgresqlService
            'mooiweb.config',     // Configurações
            'mooiweb.storage',    // LocalStorage, para guardar informação no browser
            'mooiweb.bi',         // Analytics et. al
            'mooiweb.helper',     // Biblioteca de funções genéricas
            'mooiweb.ui',         // Parte gráfica do site
            'mooiweb.filters',    // Filtros
            'mooiweb.directives', // Diretivas
            'mooiweb.security',   // Interceptações de autenticação
            'mooiweb.seo'         // Serviço de variáveis de SEO
        ]);
})();