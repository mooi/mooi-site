(function() {
    'use strict';

    angular
        .module('mooiweb.helper')
        .factory('helper', helper);

    helper.$inject = ['config', 'toastr', '$rootScope'];
    function helper(config, toastr, $rootScope) {
        var service = {
            log                : log,
            info               : info,
            warning            : warning,
            error              : error,
            getRandomFromArray : getRandomFromArray,
            validateQuery      : validateQuery,
            broadcast          : broadcast
        };

        return service;
        /////////////////////

        function log(message) {
            if(config.mooiweb.environment === 'development') {
                console.log(message);
            }
        }

        function info(message) {
            toastr.info(JSON.parse(JSON.stringify(message)));
        }

        function warning(message) {
            toastr.warning(JSON.parse(JSON.stringify(message)));
        }

        function error(message) {
            if(config.mooiweb.environment === 'development') {
                toastr.error(JSON.parse(JSON.stringify(message)));
            }
        }

        function getRandomFromArray(array) {
            var length = array.length;
            var random = Math.floor(Math.random() * length);
            return array[random];
        }

        function validateQuery(query, control) {
            var isCategoryValid = validateCategory(query.conditions.category);
            var isGendersValid = validateGenders(query.conditions.gender);
            var isInfantileValid = validateInfantile(query.conditions.infantile);
            var isPriceRangeValid = validateRange(query.conditions.lowPrice, query.conditions.highPrice, control.minPrice, control.maxPrice);
            var isDiscountRangeValid = validateRange(query.conditions.lowDiscount, query.conditions.highDiscount, control.minDiscount, control.maxDiscount);
            var isOrderByvalid = validateOrderBy(query.options.orderBy);
            var isPageNumberValid = validatePageNumber(query.options.page);
            var isPerPageValid = validatePerPage(query.options.perPage);
            var validations = [
                isCategoryValid,
                isGendersValid,
                isInfantileValid,
                isPriceRangeValid,
                isDiscountRangeValid,
                isOrderByvalid,
                isPageNumberValid,
                isPerPageValid
            ];
            return validations.every(function(element, index, array) {
                return element;
            });

            // Filtros

            function validateCategory(category) {
                var result = ['vestuario', 'acessorios', 'calcados'].indexOf(category) !== -1;
                if(!result) {
                    throw new Error('validateCategory: ' + category);
                }
                return true;
            }

            function validateGenders(genders) {
                return genders.every(function isValidGender(gender, index, array) {
                    var result = ['f', 'm'].indexOf(gender) !== -1;
                    if(!result) {
                        throw new Error('validateGenders: ' + gender);
                    }
                    return true;
                });
            }

            function validateInfantile(infantile) {
                var result = [true, false].indexOf(infantile) !== -1;
                if(!result) {
                    throw new Error('validateInfantile: ' + infantile);
                }
                return true;
            }

            function validateRange(low, high, lowerBound, upperBound) {
                var isLowValid = validateNumber(low, lowerBound, upperBound);
                var isHighValid = validateNumber(high, lowerBound, upperBound);
                var isLowLowerOrEqualToHigh = low <= high;
                var result = isLowValid && isHighValid && isLowLowerOrEqualToHigh;
                if(!result) {
                    throw new Error('validateRange: low=' + low + ', high=' + high + ', lowerBound=' + lowerBound
                        + ', upperBound=' + upperBound);
                }
                return true;
            }

            function validateNumber(number, lowerBound, upperBound) {
                upperBound = upperBound || Number.POSITIVE_INFINITY;
                lowerBound = lowerBound || Number.NEGATIVE_INFINITY;
                var parsedNumber = parseInt(number, 10);
                var result = !isNaN(parsedNumber) && parsedNumber <= upperBound && parsedNumber >= lowerBound;
                return result;
            }

            // Opções
            function validateOrderBy(orderBy) {
                var result = ['menorPreco', 'maiorPreco', 'menorDesconto', 'maiorDesconto'].indexOf(orderBy) !== -1;
                return result;
            }

            function validatePageNumber(page) {
                var parsedPage = parseInt(page, 10);
                var result = !isNaN(parsedPage) && parsedPage < 10000 && parsedPage > 0;
                if(!result) {
                    throw new Error('validatePageNumber: ' + page);
                }
                return true;
            }

            function validatePerPage(perPage) {
                var result = [40, 80, 120, 160].indexOf(perPage) !== -1;
                if(!result) {
                    throw new Error('validatePerPage: ' + perPage);
                }
                return true;
            }
        }

        function broadcast(name, content) {
            $rootScope.$broadcast(name, content);
        }

    }

})();
