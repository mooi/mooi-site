(function() {
    'use strict';

    angular
        .module('mooiweb.ui')
        .factory('ui', ui);

    ui.$inject = ['$location', '$anchorScroll', '$timeout', '$rootScope', 'helper'];
    function ui($location, $anchorScroll, $timeout, $rootScope, helper) {
        var service = {
            headerUI          : headerUI,
            offCanvasUI       : offCanvasUI,
            productsUI        : productsUI,
            productsMouseOver : productsMouseOver,
            singleProductUI   : singleProductUI,
            loadUI            : loadUI,
            openLoginModal    : openLoginModal,
            closeLoginModal   : closeLoginModal
        };

        var loaded = {
            header : false,
            modal  : false,
            pushy  : false
        };

        var state;

        return service;
        /////////////////////

        function headerUI() {
            if(!loaded.header) {
                jQuery(document).ready(function($) {
                    var headerNav = $('.navigation .main-nav .main-nav-item');

                    if($('.main-header .btn-input-search').length === 0 || headerNav.length === 0) {
                        return false;
                    }

                    // Abrir o input de busca no header: adicionar a classe "is-active"
                    $(document).on('click', '.main-header .btn-input-search', function(e) {
                        e.preventDefault();
                        $('.input-search-label').addClass('is-active');
                    });

                    // Expande o submenu no hover do botão
                    $(document).on('mouseover', '.main-header .btn-my-account', function() {
                        // $('.hold-my-account-submenu').slideDown({duration: 200, queue: false});
                        $('.hold-my-account-submenu').addClass('is-active');
                    });

                    // Colapsa o submenu no hover do botão
                    $(document).on('mouseleave', '.main-header .btn-my-account', function() {
                        // $('.hold-my-account-submenu').slideUp({duration: 200, queue: false});
                        $('.hold-my-account-submenu').removeClass('is-active');

                    });

                    // Fecha o input de busca no header
                    $('.btn-close-input-search').click(function() {
                        $('.input-search-label').removeClass('is-active');
                    });

                    // NAV: abre o submenu: Adicionar classe "submenu-is-active"
                    // Primeiro dá slideUP em todos os menus
                    $('.main-nav-submenu').slideUp({duration: 0, queue: false});
                    headerNav.hover(function() {
                        $('.navigation').toggleClass('submenu-is-active');
                        $(this).find('.main-nav-submenu').slideDown({duration: 200, queue: false});
                    }, function() {
                        $(this).find('.main-nav-submenu').slideUp({duration: 200, queue: false});
                        $('.navigation').delay(500).toggleClass('submenu-is-active');
                    });

                    helper.log('HeaderUI loaded');
                    loaded.header = true;
                    //helper.broadcast('HeaderLoaded');
                    offCanvasUI();
                    return true;
                });
            }
        }

        function offCanvasUI() {
            if(loaded.header) {
                jQuery(document).ready(function($) {
                    if(!loaded.modal) {
                        loaded.modal = modal($);
                        if(loaded.modal === false) { // retry
                            $timeout(function() {
                                loaded.modal = modal($);
                                if(loaded.modal === true) {
                                    helper.log('modal loaded');
                                }
                            }, 6000);
                        }
                        else {
                            helper.log('modal loaded');
                        }
                    }
                    if(!loaded.pushy) {
                        loaded.pushy = pushy($);
                        if(loaded.pushy === false) { // retry
                            $timeout(function() {
                                loaded.pushy = pushy($);
                                if(loaded.pushy === true) {
                                    helper.log('pushy loaded');
                                }
                            }, 6000);
                        }
                        else {
                            helper.log('pushy loaded');
                        }
                    }
                });
            }
            else {
                $timeout(function() {
                    offCanvasUI();
                }, 6000);
            }
        }

        function productsUI() {
            jQuery(document).ready(function($) {
                var products = $('.product-thumb');
                var maxHeight = Number.NEGATIVE_INFINITY;
                products.each(function(i) {
                    var image = products.eq(i).find('.main-product-image');
                    var height = image[0].offsetHeight;
                    if(height > maxHeight) {
                        maxHeight = height;
                    }
                });
                products.each(function(i) {
                    products.eq(i).css('height', maxHeight + 'px');
                });
                //if(products.length > 0) {
                //    console.log(products.width());
                //}
                //x.height('400px');
                // BREADCRUMBS: calcula o tamanho da div de acordo com a quantidade de li
                //$('.breadcrumbs').width(( $('.breadcrumbs li').width() + 30 ) * ( $('.breadcrumbs li').length ));

                //// PRODUCT COLOR
                //var productColor = $('.product-color .list li').data('color');
                //$('.product-color .list li i').css('color', productColor);

                // PRODUCT HOVER: Mostra as informações de cor de tamanho quando passa o mouse em cima do thumb do
                // produto. Adicionar classe "is-hovering"
                products.off('mouseenter mouseleave').hover(function() {
                    $(this).addClass('is-hovering');
                }, function() {
                    $(this).removeClass('is-hovering');
                });
                products.off('touchstart touchend');
                products.on('touchstart', function() {
                    products.each(function(i) {
                        var product = products.eq(i);
                        product.removeClass('is-hovering');
                    });
                    $(this).addClass('is-hovering');

                });


                //// Esconde os filtros ao clicar no botão OFF
                //// Adicionar class "filtre-hidden"
                //$('.btn-filtreOff').off('click').click(function() {
                //    $('#products').addClass('filtre-hidden');
                //});
                //
                //// Mostra os filtros ao clicar no botão ON
                //// Remover classe "filtre-hidden"
                //$('.btn-filtreOn').off('click').click(function() {
                //    $('#products').removeClass('filtre-hidden');
                //});

                // Mostra os filtros escondidos ao clicar no botão (Mobile)
                // Adicionar classe "sidebar-opened"
                $('.btn-expand-filtre').off('click').click(function() {
                    $('.sidebar').toggleClass('sidebar-opened');
                });

                helper.log('ProductsUI loaded');
            });
        }

        function loadUI(page) {
            if(!page) {
                throw new Error('Especifique o controlador chamando loadUI');
            }
            if(page === 'Header') {
                headerUI();
                //offCanvasUI();
            }
            //if(page === 'Off-Canvas') {
            //    offCanvasUI();
            //}
            if(page === 'Produtos' || page === 'Item' || page === 'Usuario') {
                productsUI();
            }
        }

        function pushy($) {
            jQuery(document).ready(function($) {
                var pushy = $('.pushy'), //menu css class
                    body = $('body'),
                    container = $('#container'), //container css class
                    push = $('.push'), //css class to add pushy capability
                    siteOverlay = $('.site-overlay'), //site overlay
                    pushyClass = "pushy-left pushy-open", //menu position & menu open class
                    pushyActiveClass = "pushy-active", //css class to toggle site overlay
                    containerClass = "container-push", //container open class
                    pushClass = "push-push", //css class to add pushy capability
                    menuBtn = $('.nav-pull, .pushy a'), //css classes to toggle the menu
                    menuSpeed = 200, //jQuery fallback menu speed
                    menuWidth = pushy.width() + "px", //jQuery fallback menu width
                    btnClosePushy = $('.btn-close-nav');
                if(pushy.length === 0 || menuBtn.length === 0 || btnClosePushy.length === 0) {
                    return false;
                }

                function togglePushy() {
                    body.toggleClass(pushyActiveClass); //toggle site overlay
                    pushy.toggleClass(pushyClass);
                    container.toggleClass(containerClass);
                    push.toggleClass(pushClass); //css class to add pushy capability
                }

                function openPushyFallback() {
                    body.addClass(pushyActiveClass);
                    pushy.animate({left : "0px"}, menuSpeed);
                    container.animate({left : menuWidth}, menuSpeed);
                    push.animate({left : menuWidth}, menuSpeed); //css class to add pushy capability
                }

                function closePushyFallback() {
                    body.removeClass(pushyActiveClass);
                    pushy.animate({left : "-" + menuWidth}, menuSpeed);
                    container.animate({left : "0px"}, menuSpeed);
                    push.animate({left : "0px"}, menuSpeed); //css class to add pushy capability
                }

                function fallback() {
                    if(state) {
                        openPushyFallback();
                        state = false;
                    }
                    else {
                        closePushyFallback();
                        state = true;
                    }
                }

                if(Modernizr.csstransforms3d) {
                    //toggle menu
                    menuBtn.off('click', togglePushy).on('click', togglePushy);
                    //close menu when clicking site overlay
                    siteOverlay.off('click', togglePushy).on('click', togglePushy);

                    btnClosePushy.off('click', togglePushy).on('click', togglePushy);
                }
                else {
                    //jQuery fallback
                    pushy.css({left : "-" + menuWidth}); //hide menu by default
                    container.css({"overflow-x" : "hidden"}); //fixes IE scrollbar issue

                    //keep track of menu state (open/close)
                    state = true;

                    //toggle menu
                    menuBtn.off('click', fallback).on('click', fallback);

                    //close menu when clicking site overlay
                    siteOverlay.off('click', fallback).on('click', fallback);

                    btnClosePushy.off('click', fallback).on('click', fallback);
                }
                return true;
            });
        }

        function modal($) {

            if($('.btn-login').length === 0) {
                return false;
            }
            // MODAL LOGIN: Abre o modal ao clicar em login
            $(document).on('click', '.btn-login', openLoginModal);

            // MODAL LOGIN: Fecha o modal ao clicar no X
            $(document).on('click', '.btn-close-modal-login', closeLoginModal);

            // MODAL LOGIN: Fecha o modal ao clicar em cadastro
            $(document).on('click', '.close-modal-login-link', closeLoginModal);

            return true;
        }

        function openLoginModal() {
            $('.hold-modal-login').fadeIn();
            $location.hash('login');
            $anchorScroll();
            $location.hash('');
        }

        function closeLoginModal() {
            $('.hold-modal-login').fadeOut();
        }

        function productsMouseOver() {
            jQuery(document).ready(function($) {
                var products = $('.product-thumb');
                products.off('mouseenter mouseleave').hover(function() {
                    $(this).addClass('is-hovering');
                }, function() {
                    $(this).removeClass('is-hovering');
                });
            });
        }

        function singleProductUI(store) {
            jQuery(document).ready(function($) {
                // Troca o logo da loja no botão: Comprar na loja (configurar de acordo com a necessidade de vocês)
                $('.btn-comprar-loja').off('mouseenter mouseleave').hover(function() {
                    $(this).find('.logo-loja').html('<img src="/img/logo-' + store + '-white.png" alt="Logo ' + store + '">');
                }, function() {
                    $(this).find('.logo-loja').html('<img src="/img/logo-' + store + '.png" alt="Logo ' + store + '">');
                });
                helper.log("SingleProductUI loaded");
            });
        }
    }

})();
