// Remove the ugly Facebook appended hash
// <https://github.com/jaredhanson/passport-facebook/issues/12>
if (window.location.hash && window.location.hash === "#_=_") {
    // If you are not using Modernizr, then the alternative is:
    //   `if (window.history && history.replaceState) {`
    if (Modernizr.history) {
        window.history.replaceState("", document.title, window.location.pathname + window.location.search);
    } else {
        window.location = window.location.href.split('#')[0];
    }
}