(function() {
    'use strict';
    angular
        .module('mooiweb', [
            'mooiweb.core',       // Módulos do Angular e 3rd party
            'mooiweb.layout',     // Index, header e footer
            'mooiweb.home',       // Home
            'mooiweb.oops',       // 404
            'mooiweb.produtos',   // Página de produtos
            'mooiweb.usuario',    // Página de usuário
            'mooiweb.item',       // Página individual de produto
            'mooiweb.cadastro',   // Página de cadastro
            'mooiweb.db',         // Serviço de interface com o PostgresqlService
            'mooiweb.config',     // Configurações
            'mooiweb.storage',    // LocalStorage, para guardar informação no browser
            'mooiweb.bi',         // Analytics et. al
            'mooiweb.helper',     // Biblioteca de funções genéricas
            'mooiweb.ui',         // Parte gráfica do site
            'mooiweb.filters',    // Filtros
            'mooiweb.directives', // Diretivas
            'mooiweb.security',   // Interceptações de autenticação
            'mooiweb.seo'         // Serviço de variáveis de SEO
        ]);
})();
(function() {
    'use strict';
    angular.
        module('mooiweb.core', [
            //do angular
            'ngAnimate',

            //módulos cross-app reutilizaveis

            // 3rd party
            'toastr',
            'ui.router',
            'ui.slider',
            'imagesLoaded',
            'angularUtils.directives.dirPagination',
            'djds4rce.angular-socialshare'
        ])
})();
(function() {
    'use strict';
    angular.
        module('mooiweb.bi', []);
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.cadastro', []);
})();
(function() {
    'use strict';
    angular.
        module('mooiweb.config', [])
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.db', []);
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.directives', []);
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.filters', []);
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.helper', []);
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.home', ['mooiweb.security']);
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.item', []);
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.layout', ['mooiweb.security']);
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.oops', []);
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.produtos', ['mooiweb.config', 'mooiweb.security']);
})();
(function() {
    'use strict';
    angular.
        module('mooiweb.security', [])
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.storage', []);
})();
(function() {
    'use strict';
    angular.
        module('mooiweb.seo', []);
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.ui', []);
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.usuario', ['mooiweb.security']);
})();
(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .provider('config', configProvider);

    configProvider.$inject = [];
    function configProvider() {

        this.$get = config;

        config.$inject = [];
        function config() {
            var service = {
                mooiweb           : mooiweb(),
                postgreSQLService : postgreSQLService(),
                mongodb           : mongodb(),
                facebook          : facebook(),
                analytics         : analytics(),
                produtos          : produtos(),
                dirPagination     : dirPagination()
            };

            return service;

            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            function mooiweb() {
                var host;
                if('development' == 'production') {
                    host = 'www.mooi.me';
                }
                else {
                    host = 'localhost';
                }
                return {
                    port                  : 80,
                    environment           : 'development',
                    localStorageAvailable : Modernizr.localstorage,
                    searchEnabled         : false,
                    host                  : host
                }
            }

            function postgreSQLService() {
                var address = mooiweb().host;//'localhost'; // 'mooi.me'
                var port = 80;
                var url = 'http://' + address + ':' + port + '/'; // 'http://localhost:12354/'

                var item = {
                    getMany     : url + 'item/getMany', // 'http://localhost:12354/item/getMany'
                    getManyById : url + 'item/getManyById'
                };

                var test = {
                    ok       : url + 'test/ok', // 'http://localhost:12354/test/ok',
                    error    : url + 'test/error',
                    notFound : url + 'test/notfound'
                };

                return {
                    address : address,
                    port    : port,
                    url     : url,
                    item    : item,
                    //test    : test
                }
            }

            function mongodb() {
                var address = mooiweb().host;//'localhost';
                var port = 80;
                var url = 'http://' + address + ':' + port + '/'; // 'http://localhost:80/'

                var user = {
                    signup      : url + 'auth/signup', // 'http://localhost:80/auth/signup'
                    login       : url + 'auth/localLogin',
                    isLoggedIn  : url + 'auth/isLoggedIn',
                    logout      : url + 'auth/logout',
                    likeItem    : url + 'user/likeItem',
                    dislikeItem : url + 'user/dislikeItem'
                };

                var subscription = {
                    subscribe : url + 'subscription/subscribe'
                };

                return {
                    address      : address,
                    port         : port,
                    url          : url,
                    user         : user,
                    subscription : subscription
                }
            }

            function facebook() {
                var appId;
                if(mooiweb().environment === 'production') {
                    appId = '1470679093192561'; // Mooi!
                }
                else {
                    appId = '1529509513976185'; // Mooi! Test 1
                }
                return {
                    appId        : appId,
                    version      : 'v2.2',
                    permissions  : 'email',
                    pixelId      : '6021327954984',
                    loginEnabled : true,
                    pixelEnabled : true
                }
            }

            function analytics() {
                return {
                    id      : 'UA-67282577-1',
                    enabled : true
                }
            }

            function produtos() {
                return {
                    params : {
                        // Filtros
                        sexo          : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        infantil      : {
                            value  : 'false',
                            squash : true
                        },
                        tipos         : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        marcas        : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        lojas         : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        cores         : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        tamanhos      : {
                            value  : [],
                            array  : true,
                            squash : true
                        },
                        menorDesconto : {
                            value  : '0',
                            squash : true
                        },
                        maiorDesconto : {
                            value  : '100',
                            squash : true
                        },
                        menorPreco    : {
                            value  : '0',
                            squash : true
                        },
                        maiorPreco    : {
                            value  : '200000',
                            squash : true
                        },
                        // extrainfo
                        temporada     : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        comprimento   : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        gola          : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        manga         : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        estilo        : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        cano          : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        saltoAltura   : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        salto         : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        ocasiao       : {
                            value  : [],
                            squash : true,
                            array  : true
                        },
                        // Opções
                        ordem         : {
                            value  : 'maiorDesconto',
                            squash : true
                        },
                        porPagina     : {
                            value  : '80',
                            squash : true
                        },
                        pagina        : {
                            value  : '1',
                            squash : true
                        },
                        // UI
                        filtros       : {
                            value : true
                        }
                    }
                }
            }

            function dirPagination() {
                return {
                    path : '/components/angular-utils-pagination/dirPagination.tpl.html'
                }
            }
        }

    }

})();

(function() {
    'use strict';

    angular.
        module('mooiweb.security')
        .provider('security', securityProvider);

    securityProvider.$inject = [];
    function securityProvider() {

        var user = null;
        this.$get = security;

        security.$inject = [];
        function security() {

            var service = {
                ensureLoggedIn    : ensureLoggedIn,
                ensureNotLoggedIn : ensureNotLoggedIn,
                checkLoggedIn     : checkLoggedIn,
                deauthorize       : deauthorize,
                updateUser        : updateUser
            };

            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            updateUser.$inject = [];
            function updateUser(newUser) {
                user = newUser;
                return user;
            }

            // Devolve 401 se não estiver logado. É interceptado e mandado pra home
            ensureLoggedIn.$inject = ['$q', '$http', '$state', 'helper'];
            function ensureLoggedIn($q, $http, $state, helper) {
                var deferred = $q.defer();
                $http.get('/auth/userProfile').then(function(response) {
                    var newUser = response.data.data;
                    if(newUser !== user) {
                        service.updateUser(newUser);
                        helper.broadcast("userChanged", newUser);
                    }
                    if(!!newUser) {
                        deferred.resolve(newUser);
                    }
                    else {
                        $state.go('root.home', {}, {inherit : false});
                        deferred.reject();
                    }
                }, function(reason) {
                    $state.go('root.home', {}, {inherit : false});
                    deferred.reject();
                });
                return deferred.promise;
            }

            // Devolve 401 caso o usuário já esteja logado para evitar conflitos de cadastro
            ensureNotLoggedIn.$inject = ['$q', '$http', '$state'];
            function ensureNotLoggedIn($q, $http, $state) {
                var deferred = $q.defer();
                if(user) {
                    $state.go('root.home', {}, {inherit : false});
                    deferred.reject();
                }
                else {
                    $http.get('/auth/isNotAuthorized').then(function(response) {
                        if(response.data.success === true) {
                            deferred.resolve(true);
                        }
                        else {
                            $state.go('root.home', {}, {inherit : false});
                            deferred.reject();
                        }
                    }, function(reason) {
                        $state.go('root.home', {}, {inherit : false});
                        deferred.reject();
                    });
                }
                return deferred.promise;
            }

            // Apenas pega o usuário, se estiver logado
            checkLoggedIn.$inject = ['$q', '$http', 'helper'];
            function checkLoggedIn($q, $http, helper) {
                var deferred = $q.defer();
                // usuário no cache
                if(user !== null && user !== undefined) {
                    deferred.resolve(user);
                }
                else {
                    $http.get('/auth/isLoggedIn').then(function(response) {
                        var newUser = response.data.data;
                        if(newUser !== user) {
                            service.updateUser(newUser);
                            helper.broadcast("userChanged", newUser);
                        }
                        deferred.resolve(newUser);
                    }, function(reason) {
                        deferred.reject();
                    });
                }
                return deferred.promise;
            }

            deauthorize.$inject = ['helper'];
            function deauthorize(helper) {
                service.updateUser(false);
                helper.broadcast("userChanged", false);
            }

            return service;
        }

    }
})();
(function() {
    'use strict';
    angular.
        module('mooiweb.bi').
        factory('bi', bi);

    bi.$inject = ['$window', '$location', 'config'];

    function bi($window, $location, config) {
        var service = {
            analytics     : analytics(),
            facebookPixel : facebookPixel()
        };

        return service;
        /////////////////////

        function analytics() {
            var methods = {
                sendEvent : function sendEvent(category, action, label) {
                    if(config.analytics.enabled) {
                        $window.ga('send', 'event', category, action, label, {
                            page      : $location.url(),
                            useBeacon : true
                        });
                    }
                },
                pageView  : function pageView() {
                    if(config.analytics.enabled) {
                        $window.ga('send', 'pageview', {
                            page      : $location.url(),
                            useBeacon : true
                        });
                    }
                }
            };
            return methods;
        }

        function facebookPixel() {
            var methods = {
                conversion: function conversion() {
                    if(config.facebook.pixelEnabled) {
                        $window._fbq.push(['track', config.facebook.pixelId, {
                            'value'    : '1.00',
                            'currency' : 'BRL'
                        }]);
                    }
                }
            };
            return methods;
        }
    }
})();
(function() {
    'use strict';

    angular
        .module('mooiweb.db')
        .factory('db', db);

    db.$inject = ['$http', '$state', '$filter', 'helper', 'config', 'security', 'ui'];
    function db($http, $state, $filter, helper, config, security, ui) {
        var service = {
            // Itens
            getItems     : getItems,
            getItemsById : getItemsById,
            getTestOK    : getTestOK,
            getTestError : getTestError,
            // Usuário
            signup       : signup,
            login        : login,
            isLoggedIn   : isLoggedIn,
            logout       : logout,
            likeItem     : likeItem,
            dislikeItem  : dislikeItem,
            // Email
            subscribe    : subscribe
        };

        return service;
        /////////////////////

        // Itens

        function getItems(originalQuery) {
            if(originalQuery === undefined) {
                throw new Error('db.service.js:getItems - No query passed');
            }
            var newQuery = JSON.parse(JSON.stringify(originalQuery));
            newQuery.conditions.category = $filter('accentCorrection')(newQuery.conditions.category);
            var queryParameters = {
                method  : 'GET',
                url     : config.postgreSQLService.item.getMany,
                params  : newQuery,
                headers : {'Authorization' : 'Token token=xxxxYYYYZzzz'},
                timeout : 10000
            };
            var query = $http(queryParameters);
            // TODO: Cache
            return query.then(function(response) {
                // {config: ..., data: data, headers: ..., status: 200, statusText: "OK"}//
                return response.data.data;
            }, function(reason) {
                // TODO: Enviar o erro para um servidor de análise
                var errorMessage;
                switch(reason.status) {
                    case -1 :
                        errorMessage = "Problema de conexão. Tente novamente mais tarde.";
                        helper.warning(errorMessage);
                        return [];
                        break;
                    case 400 :
                        errorMessage = reason.data.error || reason.data;
                        helper.warning(errorMessage);
                        break;
                    default :
                        errorMessage = reason.data.error || reason.data;
                        if(config.mooiweb.environment === 'development') {
                            helper.warning(errorMessage);
                        }
                        break;
                }
                throw new Error(errorMessage);
            });
        }

        function getItemsById(list) {
            if(list === undefined) {
                throw new Error('db.service.js:getItemsById - No conditions passed');
            }
            var queryParameters = {
                method  : 'GET',
                url     : config.postgreSQLService.item.getManyById,
                params  : {data : JSON.stringify(list)},
                headers : {'Authorization' : 'Token token=xxxxYYYYZzzz'},
                timeout : 100000
            };
            var query = $http(queryParameters);
            // TODO: Cache
            return query.then(function(response) {
                // {config: ..., data: data, headers: ..., status: 200, statusText: "OK"}//
                var item = response.data.data[0];
                if(item.macro_categorias[0] === 'vestuário') {
                    item.macro_categorias[0] = 'vestuario';
                }
                else if(item.macro_categorias[0] === 'calçados') {
                    item.macro_categorias[0] = 'calcados';
                }
                else if(item.macro_categorias[0] === 'acessórios') {
                    item.macro_categorias[0] = 'acessorios';
                }
                return item;
            }, function(reason) {
                // TODO: Enviar o erro para um servidor de análise
                var errorMessage;
                switch(reason.status) {
                    case -1 :
                        errorMessage = "Problema de conexão. Tente novamente mais tarde.";
                        helper.warning(errorMessage);
                        return [];
                        break;
                    case 400 :
                        errorMessage = reason.data.error || reason.data;
                        helper.warning(errorMessage);
                        break;
                    default :
                        errorMessage = reason.data.error || reason.data;
                        if(config.mooiweb.environment === 'development') {
                            helper.warning(errorMessage);
                        }
                        break;
                }
                throw new Error(errorMessage);
            });
        }

        function getTestOK() {
            var queryParameters = {
                method  : 'GET',
                url     : config.postgreSQLService.test.ok,
                timeout : 3000
            };
            var query = $http(queryParameters);

            return query.then(function(response) {
                return response.data;
            }, function(reason) {
                return reason.data;
            });
        }

        function getTestError() {
            var queryParameters = {
                method  : 'GET',
                url     : config.postgreSQLService.test.error,
                timeout : 3000
            };
            var query = $http(queryParameters);

            return query.then(function(response) {
                return response.data;
            }, function(reason) {
                return reason.data;
            });
        }

        function getLikedItems(user) {

        }

        // Usuário

        function getUserOK(response) {
            // {config: ..., data: data, headers: ..., status: 200, statusText: "OK"}//
            if(!!response.data.success) {
                var newUser = response.data.data;
                return updateUser(newUser);
            }
            else {
                var message;
                if(response.status == 200) {
                    message = response.data.data;
                    helper.warning(message);
                    return updateUser(false);
                }
                else {
                    // Erro que não deve aparecer para o usuário
                    message = response.data.data || response.data.error || response.data.message;
                    helper.error(message);
                    return updateUser(false);
                }
            }
        }

        function getUserError(reason) {
            var message;
            if(reason.status == -1) {
                message = "Problema de conexão. Tente novamente mais tarde.";
                helper.warning(message);
                return updateUser(false);
            }
            else {
                // Erro que não deve aparecer para o usuário
                message = response.data.data || response.data.error || response.data.message;
                helper.error(message);
                return updateUser(false);
            }
        }

        function signup(user) {
            if(user === undefined) {
                throw new Error('db.service.js:signup - No user passed');
            }
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.user.signup,
                data    : user,
                headers : {'Authorization' : 'Token token=xxxxYYYYZzzz'},
                timeout : 100000
            };
            var query = $http(queryParameters);
            return query.then(getUserOK, getUserError);
        }

        function login(user) {
            if(user === undefined) {
                throw new Error('db.service.js:login - No user passed');
            }
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.user.login,
                data    : user,
                headers : {'Authorization' : 'Token token=xxxxYYYYZzzz'},
                timeout : 100000
            };
            var query = $http(queryParameters);
            return query.then(getUserOK, getTestError);
        }

        function isLoggedIn() {
            var queryParameters = {
                method  : 'GET',
                url     : config.mongodb.user.isLoggedIn,
                timeout : 100000
            };
            var query = $http(queryParameters);

            return query.then(getUserOK, getTestError);
        }

        function logout() {
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.user.logout,
                timeout : 100000
            };
            var query = $http(queryParameters);
            return query.then(function() {
                $state.go('root.home');
                helper.info('Volte sempre :)');
                return updateUser(false);
            }, function(reason) {
                var message;
                if(reason.status == -1) {
                    message = "Problema de conexão. Tente novamente mais tarde.";
                    helper.warning(message);
                    $state.go('root.home');
                    return updateUser(false);
                }
                else {
                    // Erro que não deve aparecer para o usuário
                    message = reason.data.data || reason.data.error || reason.data.message;
                    helper.error(message);
                    $state.go('root.home');
                    return updateUser(false);
                }
            });
        }

        function itemSentiment(queryParameters) {
            var query = $http(queryParameters);
            return query.then(function(response) {
                var success = response.data.success;
                var user = response.data.data;
                if(success === true) {
                    return updateUser(user);
                }
                else {
                    var message;
                    if(response.status == 200) {
                        // Não está logado
                        ui.openLoginModal();
                    }
                    else {
                        // Erro que não deve aparecer para o usuário
                        message = response.data.data || response.data.error || response.data.message;
                        helper.error(message);
                        helper.warning('Houve algum erro. Tente novamente mais tarde.');
                    }
                }
            }, function(reason) {
                var message;
                if(reason.status == -1) {
                    message = "Problema de conexão. Tente novamente mais tarde.";
                    helper.warning(message);
                    return updateUser(false);
                }
                else {
                    // Erro que não deve aparecer para o usuário
                    message = response.data.data || response.data.error || response.data.message;
                    helper.error(message);
                    return updateUser(false);
                }
            });
        }

        function likeItem(item) {
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.user.likeItem,
                data    : item,
                timeout : 100000
            };
            return itemSentiment(queryParameters);
        }

        function dislikeItem(item) {
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.user.dislikeItem,
                data    : item,
                timeout : 100000
            };
            return itemSentiment(queryParameters);
        }

        function updateUser(user) {
            security.updateUser(user);
            helper.broadcast("userChanged", user);
            return user;
        }

        // Email

        function subscribe(email) {
            var queryParameters = {
                method  : 'POST',
                url     : config.mongodb.subscription.subscribe,
                data    : {email : email},
                timeout : 100000
            };
            var query = $http(queryParameters);
            return query.then(function(response) {
                if(!response.data.success) {
                    helper.warning(response.data.data);
                }
                else {
                    helper.info('Inscrição realizada com sucesso!');
                }
                return response.data.success;
            }, function(reason) {
                var message;
                if(reason.status == -1) {
                    message = "Problema de conexão. Tente novamente mais tarde.";
                    helper.warning(message);
                }
                else {
                    // Erro que não deve aparecer para o usuário
                    message = response.data.data || response.data.error || response.data.message;
                    helper.error(message);
                }
                return false;
            });
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('mooiweb.helper')
        .factory('helper', helper);

    helper.$inject = ['config', 'toastr', '$rootScope'];
    function helper(config, toastr, $rootScope) {
        var service = {
            log                : log,
            info               : info,
            warning            : warning,
            error              : error,
            getRandomFromArray : getRandomFromArray,
            validateQuery      : validateQuery,
            broadcast          : broadcast
        };

        return service;
        /////////////////////

        function log(message) {
            if(config.mooiweb.environment === 'development') {
                console.log(message);
            }
        }

        function info(message) {
            toastr.info(JSON.parse(JSON.stringify(message)));
        }

        function warning(message) {
            toastr.warning(JSON.parse(JSON.stringify(message)));
        }

        function error(message) {
            if(config.mooiweb.environment === 'development') {
                toastr.error(JSON.parse(JSON.stringify(message)));
            }
        }

        function getRandomFromArray(array) {
            var length = array.length;
            var random = Math.floor(Math.random() * length);
            return array[random];
        }

        function validateQuery(query, control) {
            var isCategoryValid = validateCategory(query.conditions.category);
            var isGendersValid = validateGenders(query.conditions.gender);
            var isInfantileValid = validateInfantile(query.conditions.infantile);
            var isPriceRangeValid = validateRange(query.conditions.lowPrice, query.conditions.highPrice, control.minPrice, control.maxPrice);
            var isDiscountRangeValid = validateRange(query.conditions.lowDiscount, query.conditions.highDiscount, control.minDiscount, control.maxDiscount);
            var isOrderByvalid = validateOrderBy(query.options.orderBy);
            var isPageNumberValid = validatePageNumber(query.options.page);
            var isPerPageValid = validatePerPage(query.options.perPage);
            var validations = [
                isCategoryValid,
                isGendersValid,
                isInfantileValid,
                isPriceRangeValid,
                isDiscountRangeValid,
                isOrderByvalid,
                isPageNumberValid,
                isPerPageValid
            ];
            return validations.every(function(element, index, array) {
                return element;
            });

            // Filtros

            function validateCategory(category) {
                var result = ['vestuario', 'acessorios', 'calcados'].indexOf(category) !== -1;
                if(!result) {
                    throw new Error('validateCategory: ' + category);
                }
                return true;
            }

            function validateGenders(genders) {
                return genders.every(function isValidGender(gender, index, array) {
                    var result = ['f', 'm'].indexOf(gender) !== -1;
                    if(!result) {
                        throw new Error('validateGenders: ' + gender);
                    }
                    return true;
                });
            }

            function validateInfantile(infantile) {
                var result = [true, false].indexOf(infantile) !== -1;
                if(!result) {
                    throw new Error('validateInfantile: ' + infantile);
                }
                return true;
            }

            function validateRange(low, high, lowerBound, upperBound) {
                var isLowValid = validateNumber(low, lowerBound, upperBound);
                var isHighValid = validateNumber(high, lowerBound, upperBound);
                var isLowLowerOrEqualToHigh = low <= high;
                var result = isLowValid && isHighValid && isLowLowerOrEqualToHigh;
                if(!result) {
                    throw new Error('validateRange: low=' + low + ', high=' + high + ', lowerBound=' + lowerBound
                        + ', upperBound=' + upperBound);
                }
                return true;
            }

            function validateNumber(number, lowerBound, upperBound) {
                upperBound = upperBound || Number.POSITIVE_INFINITY;
                lowerBound = lowerBound || Number.NEGATIVE_INFINITY;
                var parsedNumber = parseInt(number, 10);
                var result = !isNaN(parsedNumber) && parsedNumber <= upperBound && parsedNumber >= lowerBound;
                return result;
            }

            // Opções
            function validateOrderBy(orderBy) {
                var result = ['menorPreco', 'maiorPreco', 'menorDesconto', 'maiorDesconto'].indexOf(orderBy) !== -1;
                return result;
            }

            function validatePageNumber(page) {
                var parsedPage = parseInt(page, 10);
                var result = !isNaN(parsedPage) && parsedPage < 10000 && parsedPage > 0;
                if(!result) {
                    throw new Error('validatePageNumber: ' + page);
                }
                return true;
            }

            function validatePerPage(perPage) {
                var result = [40, 80, 120, 160].indexOf(perPage) !== -1;
                if(!result) {
                    throw new Error('validatePerPage: ' + perPage);
                }
                return true;
            }
        }

        function broadcast(name, content) {
            $rootScope.$broadcast(name, content);
        }

    }

})();

(function() {
    'use strict';

    angular
        .module('mooiweb.storage')
        .factory('storage', storage);

    storage.$inject = ['config'];
    function storage(config) {
        var service = {
            // Itens
            set        : set,
            setViewed  : setViewed,
            setRelated : setRelated,
            get        : get,
            getViewed  : getViewed,
            getRelated : getRelated
        };

        return service;
        /////////////////////

        // Itens

        function get(key) {
            if(config.mooiweb.localStorageAvailable) {
                if(!!key) {
                    var value = localStorage.getItem(key);
                    try {
                        var parsedValue = JSON.parse(value);
                        return parsedValue;
                    }
                    catch(e) {
                        return value;
                    }
                }
                else {
                    throw new Error('storage.service.js:get - No item passed');
                }
            }
            else {
                return null;
            }
        }

        function getViewed() {
            if(config.mooiweb.localStorageAvailable) {
                var viewedList = get('viewedList') || [];
                return viewedList;
            }
            else {
                return null;
            }
        }

        function set(key, value) {
            if(config.mooiweb.localStorageAvailable) {
                if(!!key && !!value) {
                    var valueString = JSON.stringify(value);
                    localStorage.setItem(key, valueString);
                    var valueSaved = localStorage.getItem(key);
                    try {
                        var parsedValue = JSON.parse(valueSaved);
                        return parsedValue;
                    }
                    catch(e) {
                        return valueSaved;
                    }
                }
                else {
                    throw new Error('storage.service.js:get - No key or value passed');
                }
            }
            else {
                return null;
            }
        }

        function setViewed(item) {
            if(config.mooiweb.localStorageAvailable) {
                if(!!item) {
                    var viewedList = get('viewedList') || [];
                    for(var i = 0; i < viewedList.length; i += 1) {
                        if(viewedList[i].id_externo === item.id_externo) {
                            viewedList.splice(i, 1);
                        }
                    }
                    viewedList.unshift(item);
                    if(viewedList.length > 8) {
                        viewedList.pop();
                    }
                    return set('viewedList', viewedList) === viewedList;
                }
                else {
                    return new Error('storage.service.js:setViewed - No item passed');
                }
            }
            else {
                return null;
            }
        }

        function setRelated(item, related) {
            if(config.mooiweb.localStorageAvailable) {
                if(!!item && !!related) {
                    var relatedList = related.slice(0, 8); // 8 primeiras posições
                    var id = item.loja + item.id_externo;
                    return set('related_' + id, relatedList) === relatedList;
                }
                else {
                    throw new Error('storage.service.js:setRelated - No item or related list passed');
                }
            }
            else {
                return null;
            }
        }

        function getRelated(item) {
            if(config.mooiweb.localStorageAvailable) {
                var id = item.loja + item.id_externo;
                var relatedList = get('related_' + id) || [];
                return relatedList;
            }
            else {
                return null;
            }
        }
    }
})();

(function() {
    'use strict';
    angular.
        module('mooiweb.seo').
        factory('seo', seo);

    seo.$inject = ['$rootScope', '$location'];

    function seo($rootScope, $location) {
        var service = {
            setPrice : setPrice,
            setTitle : setTitle,
            setDescription : setDescription,
            setAuthor : setAuthor,
            setURL : setURL,
            setImage : setImage
        };

        init();

        return service;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function init() {
            $rootScope.og = {};
            $rootScope.og.siteName = 'Mooi.me';
            $rootScope.schema = {};
            $rootScope.author = 'Mooi.me'
        }

        function setTitle(title) {
            //title = 'Mooi.me - ' + title;
            $rootScope.title = title;
            $rootScope.og.title = title;
            $rootScope.schema.name = title;
        }

        function setPrice(newPrice) {
            var price = parseInt(newPrice); // 1999
            if(isNaN(price)) {
                $rootScope.og.price = null;
            }
            else {
                var reais = Math.floor(price/100); // 19
                var centavos = price - (reais * 100); // 1999 - 1900 = 99
                $rootScope.og.price = reais + '.' + centavos; // 19.99
            }
        }

        function setDescription(description) {
            $rootScope.description = description;
            $rootScope.og.description = description;
            $rootScope.schema.description = description;
        }

        function setAuthor(author) {
            $rootScope.author = author;
        }

        function setURL(URL) {
            if(!!URL) {
                $rootScope.og.url = URL;
            }
            else {
                $rootScope.og.url = $location.absUrl();
            }
        }

        function setImage(image, width, height, external) {
            var imageURL;
            if(!!external) {
                imageURL = image;
            }
            else {
                imageURL = 'http://' + $location.host() + '/img/' + image; // http://www.mooi.me/img/imagem.png
            }
            $rootScope.og.image = imageURL;
            $rootScope.schema.image = imageURL;
            $rootScope.og.width = width;
            $rootScope.og.height = height;
        }

    }
})();
(function() {
    'use strict';

    angular
        .module('mooiweb.ui')
        .factory('ui', ui);

    ui.$inject = ['$location', '$anchorScroll', '$timeout', '$rootScope', 'helper'];
    function ui($location, $anchorScroll, $timeout, $rootScope, helper) {
        var service = {
            headerUI          : headerUI,
            offCanvasUI       : offCanvasUI,
            productsUI        : productsUI,
            productsMouseOver : productsMouseOver,
            singleProductUI   : singleProductUI,
            loadUI            : loadUI,
            openLoginModal    : openLoginModal,
            closeLoginModal   : closeLoginModal
        };

        var loaded = {
            header : false,
            modal  : false,
            pushy  : false
        };

        var state;

        return service;
        /////////////////////

        function headerUI() {
            if(!loaded.header) {
                jQuery(document).ready(function($) {
                    var headerNav = $('.navigation .main-nav .main-nav-item');

                    if($('.main-header .btn-input-search').length === 0 || headerNav.length === 0) {
                        return false;
                    }

                    // Abrir o input de busca no header: adicionar a classe "is-active"
                    $(document).on('click', '.main-header .btn-input-search', function(e) {
                        e.preventDefault();
                        $('.input-search-label').addClass('is-active');
                    });

                    // Expande o submenu no hover do botão
                    $(document).on('mouseover', '.main-header .btn-my-account', function() {
                        // $('.hold-my-account-submenu').slideDown({duration: 200, queue: false});
                        $('.hold-my-account-submenu').addClass('is-active');
                    });

                    // Colapsa o submenu no hover do botão
                    $(document).on('mouseleave', '.main-header .btn-my-account', function() {
                        // $('.hold-my-account-submenu').slideUp({duration: 200, queue: false});
                        $('.hold-my-account-submenu').removeClass('is-active');

                    });

                    // Fecha o input de busca no header
                    $('.btn-close-input-search').click(function() {
                        $('.input-search-label').removeClass('is-active');
                    });

                    // NAV: abre o submenu: Adicionar classe "submenu-is-active"
                    // Primeiro dá slideUP em todos os menus
                    $('.main-nav-submenu').slideUp({duration: 0, queue: false});
                    headerNav.hover(function() {
                        $('.navigation').toggleClass('submenu-is-active');
                        $(this).find('.main-nav-submenu').slideDown({duration: 200, queue: false});
                    }, function() {
                        $(this).find('.main-nav-submenu').slideUp({duration: 200, queue: false});
                        $('.navigation').delay(500).toggleClass('submenu-is-active');
                    });

                    helper.log('HeaderUI loaded');
                    loaded.header = true;
                    //helper.broadcast('HeaderLoaded');
                    offCanvasUI();
                    return true;
                });
            }
        }

        function offCanvasUI() {
            if(loaded.header) {
                jQuery(document).ready(function($) {
                    if(!loaded.modal) {
                        loaded.modal = modal($);
                        if(loaded.modal === false) { // retry
                            $timeout(function() {
                                loaded.modal = modal($);
                                if(loaded.modal === true) {
                                    helper.log('modal loaded');
                                }
                            }, 6000);
                        }
                        else {
                            helper.log('modal loaded');
                        }
                    }
                    if(!loaded.pushy) {
                        loaded.pushy = pushy($);
                        if(loaded.pushy === false) { // retry
                            $timeout(function() {
                                loaded.pushy = pushy($);
                                if(loaded.pushy === true) {
                                    helper.log('pushy loaded');
                                }
                            }, 6000);
                        }
                        else {
                            helper.log('pushy loaded');
                        }
                    }
                });
            }
            else {
                $timeout(function() {
                    offCanvasUI();
                }, 6000);
            }
        }

        function productsUI() {
            jQuery(document).ready(function($) {
                var products = $('.product-thumb');
                var maxHeight = Number.NEGATIVE_INFINITY;
                products.each(function(i) {
                    var image = products.eq(i).find('.main-product-image');
                    var height = image[0].offsetHeight;
                    if(height > maxHeight) {
                        maxHeight = height;
                    }
                });
                products.each(function(i) {
                    products.eq(i).css('height', maxHeight + 'px');
                });
                //if(products.length > 0) {
                //    console.log(products.width());
                //}
                //x.height('400px');
                // BREADCRUMBS: calcula o tamanho da div de acordo com a quantidade de li
                //$('.breadcrumbs').width(( $('.breadcrumbs li').width() + 30 ) * ( $('.breadcrumbs li').length ));

                //// PRODUCT COLOR
                //var productColor = $('.product-color .list li').data('color');
                //$('.product-color .list li i').css('color', productColor);

                // PRODUCT HOVER: Mostra as informações de cor de tamanho quando passa o mouse em cima do thumb do
                // produto. Adicionar classe "is-hovering"
                products.off('mouseenter mouseleave').hover(function() {
                    $(this).addClass('is-hovering');
                }, function() {
                    $(this).removeClass('is-hovering');
                });
                products.off('touchstart touchend');
                products.on('touchstart', function() {
                    products.each(function(i) {
                        var product = products.eq(i);
                        product.removeClass('is-hovering');
                    });
                    $(this).addClass('is-hovering');

                });


                //// Esconde os filtros ao clicar no botão OFF
                //// Adicionar class "filtre-hidden"
                //$('.btn-filtreOff').off('click').click(function() {
                //    $('#products').addClass('filtre-hidden');
                //});
                //
                //// Mostra os filtros ao clicar no botão ON
                //// Remover classe "filtre-hidden"
                //$('.btn-filtreOn').off('click').click(function() {
                //    $('#products').removeClass('filtre-hidden');
                //});

                // Mostra os filtros escondidos ao clicar no botão (Mobile)
                // Adicionar classe "sidebar-opened"
                $('.btn-expand-filtre').off('click').click(function() {
                    $('.sidebar').toggleClass('sidebar-opened');
                });

                helper.log('ProductsUI loaded');
            });
        }

        function loadUI(page) {
            if(!page) {
                throw new Error('Especifique o controlador chamando loadUI');
            }
            if(page === 'Header') {
                headerUI();
                //offCanvasUI();
            }
            //if(page === 'Off-Canvas') {
            //    offCanvasUI();
            //}
            if(page === 'Produtos' || page === 'Item' || page === 'Usuario') {
                productsUI();
            }
        }

        function pushy($) {
            jQuery(document).ready(function($) {
                var pushy = $('.pushy'), //menu css class
                    body = $('body'),
                    container = $('#container'), //container css class
                    push = $('.push'), //css class to add pushy capability
                    siteOverlay = $('.site-overlay'), //site overlay
                    pushyClass = "pushy-left pushy-open", //menu position & menu open class
                    pushyActiveClass = "pushy-active", //css class to toggle site overlay
                    containerClass = "container-push", //container open class
                    pushClass = "push-push", //css class to add pushy capability
                    menuBtn = $('.nav-pull, .pushy a'), //css classes to toggle the menu
                    menuSpeed = 200, //jQuery fallback menu speed
                    menuWidth = pushy.width() + "px", //jQuery fallback menu width
                    btnClosePushy = $('.btn-close-nav');
                if(pushy.length === 0 || menuBtn.length === 0 || btnClosePushy.length === 0) {
                    return false;
                }

                function togglePushy() {
                    body.toggleClass(pushyActiveClass); //toggle site overlay
                    pushy.toggleClass(pushyClass);
                    container.toggleClass(containerClass);
                    push.toggleClass(pushClass); //css class to add pushy capability
                }

                function openPushyFallback() {
                    body.addClass(pushyActiveClass);
                    pushy.animate({left : "0px"}, menuSpeed);
                    container.animate({left : menuWidth}, menuSpeed);
                    push.animate({left : menuWidth}, menuSpeed); //css class to add pushy capability
                }

                function closePushyFallback() {
                    body.removeClass(pushyActiveClass);
                    pushy.animate({left : "-" + menuWidth}, menuSpeed);
                    container.animate({left : "0px"}, menuSpeed);
                    push.animate({left : "0px"}, menuSpeed); //css class to add pushy capability
                }

                function fallback() {
                    if(state) {
                        openPushyFallback();
                        state = false;
                    }
                    else {
                        closePushyFallback();
                        state = true;
                    }
                }

                if(Modernizr.csstransforms3d) {
                    //toggle menu
                    menuBtn.off('click', togglePushy).on('click', togglePushy);
                    //close menu when clicking site overlay
                    siteOverlay.off('click', togglePushy).on('click', togglePushy);

                    btnClosePushy.off('click', togglePushy).on('click', togglePushy);
                }
                else {
                    //jQuery fallback
                    pushy.css({left : "-" + menuWidth}); //hide menu by default
                    container.css({"overflow-x" : "hidden"}); //fixes IE scrollbar issue

                    //keep track of menu state (open/close)
                    state = true;

                    //toggle menu
                    menuBtn.off('click', fallback).on('click', fallback);

                    //close menu when clicking site overlay
                    siteOverlay.off('click', fallback).on('click', fallback);

                    btnClosePushy.off('click', fallback).on('click', fallback);
                }
                return true;
            });
        }

        function modal($) {

            if($('.btn-login').length === 0) {
                return false;
            }
            // MODAL LOGIN: Abre o modal ao clicar em login
            $(document).on('click', '.btn-login', openLoginModal);

            // MODAL LOGIN: Fecha o modal ao clicar no X
            $(document).on('click', '.btn-close-modal-login', closeLoginModal);

            // MODAL LOGIN: Fecha o modal ao clicar em cadastro
            $(document).on('click', '.close-modal-login-link', closeLoginModal);

            return true;
        }

        function openLoginModal() {
            $('.hold-modal-login').fadeIn();
            $location.hash('login');
            $anchorScroll();
            $location.hash('');
        }

        function closeLoginModal() {
            $('.hold-modal-login').fadeOut();
        }

        function productsMouseOver() {
            jQuery(document).ready(function($) {
                var products = $('.product-thumb');
                products.off('mouseenter mouseleave').hover(function() {
                    $(this).addClass('is-hovering');
                }, function() {
                    $(this).removeClass('is-hovering');
                });
            });
        }

        function singleProductUI(store) {
            jQuery(document).ready(function($) {
                // Troca o logo da loja no botão: Comprar na loja (configurar de acordo com a necessidade de vocês)
                $('.btn-comprar-loja').off('mouseenter mouseleave').hover(function() {
                    $(this).find('.logo-loja').html('<img src="/img/logo-' + store + '-white.png" alt="Logo ' + store + '">');
                }, function() {
                    $(this).find('.logo-loja').html('<img src="/img/logo-' + store + '.png" alt="Logo ' + store + '">');
                });
                helper.log("SingleProductUI loaded");
            });
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('mooiweb.usuario')
        .factory('usuario', usuario);

    usuario.$inject = [];
    function usuario() {
        var service = {
            // Itens
            getUserName   : getUserName,
            getUserGender : getUserGender,
            getUserEmail  : getUserEmail,
        };

        return service;
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function getUserName(user) {
            var userName;
            if(!user) {
                userName = 'Usuário(a)'
            }
            else if(!!user.local) {
                userName = user.local.name;
            }
            else if(!!user.facebook) {
                userName = user.facebook.name;
            }
            else {
                userName = 'Usuário(a)'
            }
            return userName;
        }

        function getUserGender(user) {
            var userGender = {};
            if(!user) {
                userGender.char = null;
            }
            else if(!!user.local) {
                if(user.local.gender === 'Feminino') {
                    userGender.char = 'f';
                }
                else if(user.local.gender === 'Masculino') {
                    userGender.char = 'f';
                }
                else {
                    userGender.char = null;
                }
            }
            else if(!!user.facebook) {
                if(user.facebook.gender.toLowerCase() === 'female') {
                    userGender.char = 'f';
                }
                else if(user.facebook.gender.toLowerCase() === 'male') {
                    userGender.char = 'm';
                }
                else {
                    userGender.char = null;
                }
                // TODO: Verificar as zilhares de possibilidades do facebook
            }
            return userGender;
        }

        function getUserEmail(user) {
            if(!user) {
                return null;
            }
            if(!!user.local) {
                return user.local.email;
            }
            if(!!user.facebook) {
                return user.facebook.email;
            }
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .config(configure);

    configure.$inject = ['$windowProvider', 'configProvider'];
    function configure($windowProvider, configProvider){
        var $window = $windowProvider.$get();
        var config = configProvider.$get();
        var id = config.analytics.id;
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        $window.ga('create', id, 'auto');
    }

})();
(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .config(configure);

    configure.$inject = ['paginationTemplateProvider', 'configProvider'];
    function configure(paginationTemplateProvider, configProvider) {
        var config = configProvider.$get();
        var templatePath = config.dirPagination.path;
        paginationTemplateProvider.setPath(templatePath);
    }

})();
(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .config(configure);

    configure.$inject = ['$windowProvider'];
    function configure($windowProvider) {
        var $window = $windowProvider.$get();
        (function() {
            var _fbq = $window._fbq || ($window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
        })();
        $window._fbq = window._fbq || [];
    }

})();
(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .config(configure);

    configure.$inject = ['$httpProvider'];
    function configure($httpProvider) {
        $httpProvider.interceptors.push(httpUnauthorizedInterceptor);

        httpUnauthorizedInterceptor.$inject = ['$q', '$location'];
        function httpUnauthorizedInterceptor($q, $location) {
            return {
                response      : function(response) {
                    return response;
                },
                responseError : function(response) {
                    if(response.status === 401) {
                        $location.url('/home');
                    }
                    return $q.reject(response);
                }
            };
        }
    }
})();
(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .config(configure);

    configure.$inject = ['toastrConfig'];
    function configure(toastrConfig) {
        angular.extend(toastrConfig, {
            positionClass         : 'toast-bottom-right'
        });
    }

})();
(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .run(run);

    run.$inject = ['$rootScope', '$state', 'bi'];
    function run($rootScope, $state, bi) {
        changeTitle($rootScope, $state);
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            //changeTitle($rootScope, $state);
            bi.analytics.pageView();
        })
    }

    function changeTitle($rootScope, $state) {
        var defaultTitle = 'A sua vitrine da moda na internet';
        if(!!$state.current.data) {
            $rootScope.title = $state.current.data.title || defaultTitle;
        }
        else {
            $rootScope.title = defaultTitle;
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('mooiweb.config')
        .run(run);

    run.$inject = ['$FB', 'config'];
    function run($FB, config) {
        var appId = config.facebook.appId;
        $FB.init(appId);
    }

})();
(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('accentCorrection', accentCorrection);

    accentCorrection.$inject = [];
    function accentCorrection() {
        var dictionary = [
            {from: 'acessorios', to: 'acessórios'},
            {from: 'calcados', to: 'calçados'},
            {from: 'vestuario', to: 'vestuário'}
        ];

        return function(input) {
            var correctEntry = dictionary.filter(function(entry) {
                return entry.from === input;
            })[0];
            if(correctEntry !== undefined) {
                return correctEntry.to;
            }
            return input;
        }
    }

})();
(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('capitalize', capitalize);

    capitalize.$inject = [];
    function capitalize() {
        var reg;
        return function(input, all) {
            if(all) {
                reg = /([^\W_]+[^\s-]*) */g;
            }
            else {
                reg = /([^\W_]+[^\s-]*)/;
            }
            if(!!input) {
                var capitalized = input.replace(reg, function(txt) {
                    var capitalizedWord = txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    return capitalizedWord;
                });
                return capitalized;
            }
            return input;
        }
    }
})();

(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('currency', currency);

    currency.$inject = [];
    function currency() {
        return function(input) {
            var response = 'R$';
            var parsedInput = parseInt(input, 10); // 6999
            if(isNaN(parsedInput)) {
                return response + parsedInput;
            }
            else {
                var real = Math.floor(parsedInput/100); // 69
                var cents = parsedInput - real * 100; // 6999 - 6900 = 99
                if(cents < 10) { // 0
                    cents = '0' + cents; // 00 -> 10,00
                }
                return response + real + ',' + cents;
            }
        }
    }

})();
(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('genderCorrection', genderCorrection);

    genderCorrection.$inject = [];
    function genderCorrection() {
        var dictionary = [
            {from: 'f', to: 'Feminino'},
            {from: 'm', to: 'Masculino'},
            {from: 'u', to: 'Unissex'},
            {from: 'x', to: '-'}
        ];

        return function(input) {
            if(!!input) {
                var correctEntry = dictionary.filter(function(entry) {
                    return entry.from === input.toLowerCase();
                })[0];
                if(correctEntry !== undefined) {
                    return correctEntry.to;
                }
            }
            return input;
        }
    }
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('lineBreakCorrection', lineBreakCorrection);

    lineBreakCorrection.$inject = [];
    function lineBreakCorrection() {
        var regex = /(<br[\s]*[/]*>)/gi;

        return function(input, nonewline) {
            nonewline = nonewline || false;
            if(input !== undefined) {
                input = input.toString();
                if(!nonewline) {
                    input = input.replace(regex, "\n");
                }
                else {
                    input = input.replace(regex, " ");
                }
            }
            return input;
        }
    }

})();
(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('listToString', listToString);

    listToString.$inject = ['$filter'];
    function listToString($filter) {
        return function(input) {
            var result;
            if(!!input) {
                if(input.constructor !== Array) {
                    input = [input];
                }
                result = input.join(', ');
                result = $filter('capitalize')(result, true);
            }
            return result;
        }
    }
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('material', material);

    material.$inject = ['$filter'];
    function material($filter) {
        return function(input) {
            if(!!input && input.constructor === Array && input.length > 0) {
                var entriesArray = [];
                for(var i = 0; i < input.length; i += 1) {
                    var entry = '';
                    if(!!input[i].valor) {
                        if(!!input[i].porcentagem) {
                            var percentage = input[i].porcentagem + '% ';
                            entry += percentage;
                        }
                        var value = $filter('capitalize')(input[i].valor, true);
                        entry += value;
                        entriesArray.push(entry);
                    }
                }
                var response = entriesArray.join(', ');
                return response;
            }
            return '';
        }
    }
})();
(function() {
    'use strict';
    angular
        .module('mooiweb.filters')
        .filter('percentage', percentage);

    percentage.$inject = [];
    function percentage() {
        return function(input) {
            var value = Math.floor(parseFloat(input));
            if(isNaN(value)) {
                value = input;
            }
            return value + '%';
        }
    }

})();
(function() {
    'use strict';
    angular
        .module('mooiweb.directives')
        .directive('mooiOnFinishRender', mooiOnFinishRender);

    mooiOnFinishRender.$inject = ['$timeout'];
    function mooiOnFinishRender($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit(attr.mooiOnFinishRender);
                    });
                }
            }
        }
    }

})();
(function() {
    'use strict';
    angular
        .module('mooiweb.directives')
        .directive('mooiValidEmail', mooiValidEmail);

    mooiValidEmail.$inject = [];
    function mooiValidEmail() {
        var emailRegex = /^[a-z0-9._%+-]+@(?:[a-z0-9-]+\.)+[a-z]{2,63}$/;
        return {
            require: 'ngModel',
            restrict: '',
            link: function(scope, elm, attrs, ctrl) {
                // only apply the validator if ngModel is present and Angular has added the email validator
                if (ctrl && ctrl.$validators.email) {

                    // this will overwrite the default Angular email validator
                    ctrl.$validators.email = function(modelValue) {
                        var isEmpty = ctrl.$isEmpty(modelValue);
                        var isValid = false;
                        if(!isEmpty) {
                            isValid = emailRegex.test(modelValue.toLowerCase());
                        }
                        return isEmpty || isValid;
                    };
                }
            }
        };
    }

})();
(function() {
    'use strict';
    angular
        .module('mooiweb.directives')
        .directive('mooiValidName', mooiValidName);

    mooiValidName.$inject = [];
    function mooiValidName() {
        var nameRegex = /[a-zA-Z \u00C0-\u024f]+$/;
        return {
            require: 'ngModel',
            restrict: '',
            link: function(scope, elm, attrs, ctrl) {
                if (ctrl) {
                    ctrl.$validators.name = function(modelValue) {
                        var isEmpty = ctrl.$isEmpty(modelValue);
                        var isValid = false;
                        if(!isEmpty) {
                            isValid = nameRegex.test(modelValue) && modelValue.length >= 3;
                        }
                        return isEmpty || isValid;
                    };
                }
            }
        };
    }

})();
(function() {
    'use strict';
    angular
        .module('mooiweb.directives')
        .directive('mooiValidPassword', mooiValidPassword);

    mooiValidPassword.$inject = [];
    function mooiValidPassword() {
        var passwordRegex = /^[a-zA-Z0-9,!_.-@#%*]+$/;
        return {
            require: 'ngModel',
            restrict: '',
            link: function(scope, elm, attrs, ctrl) {
                // only apply the validator if ngModel is present
                if (ctrl) {
                    ctrl.$validators.password = function(modelValue) {
                        var isEmpty = ctrl.$isEmpty(modelValue);
                        var isValid = false;
                        if(!isEmpty) {
                            isValid = passwordRegex.test(modelValue.toLowerCase()) && modelValue.length >= 5;
                        }
                        return isEmpty || isValid;
                    };
                }
            }
        };
    }

})();
(function() {
    'use strict';

    angular.
        module('mooiweb.layout').
        config(layoutRouteConfig);

    layoutRouteConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', 'securityProvider'];
    function layoutRouteConfig($stateProvider, $urlRouterProvider, $locationProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider.state('root', {
            url      : '',
            abstract : true,
            views    : {
                'offCanvas' : {
                    templateUrl  : 'source/layout/off-canvas.html',
                    controller   : 'OffCanvas',
                    controllerAs : 'vm'
                },
                'header'    : {
                    templateUrl  : 'source/layout/header.html',
                    controller   : 'Header',
                    controllerAs : 'vm'
                },
                'footer'    : {
                    templateUrl  : 'source/layout/footer.html',
                    controller   : 'Footer',
                    controllerAs : 'vm'
                }
            },
            resolve : {
                user : security.checkLoggedIn
            }
        });

        $urlRouterProvider.when('', '/home');

        $urlRouterProvider.when('/', '/home');

        $urlRouterProvider.otherwise("/oops");

        $locationProvider.html5Mode(true).hashPrefix('!');

    }

})();

(function() {
    'use strict';

    angular.
        module('mooiweb.cadastro').
        config(cadastroRouteConfig);

    cadastroRouteConfig.$inject = ['$stateProvider', 'securityProvider'];
    function cadastroRouteConfig($stateProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider
            .state('root.cadastro', {
                url   : '/cadastro',
                views : {
                    'container@' : {
                        templateUrl  : 'source/cadastro/cadastro.html',
                        controller   : 'Cadastro',
                        controllerAs : 'vm'
                    }
                },
                resolve : {
                    notLogged : security.ensureNotLoggedIn
                },
                data: {
                    title: 'Cadastro'
                }
            });
    }

})();
(function() {
    'use strict';

    angular.
        module('mooiweb.home').
        config(homeRouteConfig);

    homeRouteConfig.$inject = ['$stateProvider', 'securityProvider'];
    function homeRouteConfig($stateProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider
            .state('root.home', {
                url    : '/home?facebookLogin',
                views  : {
                    'container@' : {
                        templateUrl  : 'source/home/home.html',
                        controller   : 'Home',
                        controllerAs : 'vm'
                    }
                },
                params : {
                    facebookLogin : {
                        value  : null,
                        squash : true
                    }
                },
                resolve : {
                    user : security.checkLoggedIn
                },
                data: {
                    title: 'A sua vitrine da moda na internet'
                }
            });
    }

})();
(function() {
    'use strict';

    angular.
        module('mooiweb.item').
        config(itemRouteConfig);

    itemRouteConfig.$inject = ['$stateProvider', 'securityProvider'];
    function itemRouteConfig($stateProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider
            .state('root.item', {
                url    : '/item/:loja/:id',
                views  : {
                    'container@' : {
                        templateUrl  : 'source/item/item.html',
                        controller   : 'Item',
                        controllerAs : 'vm'
                    }
                },
                params : {},
                resolve : {
                    user : security.checkLoggedIn
                }
            });
    }
})();
(function() {
    'use strict';

    angular.
        module('mooiweb.oops').
        config(oopsRouteConfig);

    oopsRouteConfig.$inject = ['$stateProvider', 'securityProvider'];
    function oopsRouteConfig($stateProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider
            // Para que a página ignore os arquivos header e footer, mostrando apenas a página oops, usar
            // 'oops' ao invés de 'root.oops'
            .state('root.oops', {
                url   : '/oops',
                views : {
                    'container@' : {
                        templateUrl  : 'source/oops/oops.html',
                        controller   : 'Oops',
                        controllerAs : 'vm'
                    }
                },
                resolve : {
                    user : security.checkLoggedIn
                },
                data: {
                    title: 'Página não encontrada!'
                }
            });
    }

})();
(function() {
    'use strict';

    angular.
        module('mooiweb.produtos').
        config(produtosRouteConfig);

    produtosRouteConfig.$inject = ['$stateProvider', 'configProvider', 'securityProvider'];
    function produtosRouteConfig($stateProvider, configProvider, securityProvider) {
        var security = securityProvider.$get();
        var config = configProvider.$get();
        $stateProvider
            .state('root.produtos', {
                url    : '/produtos/:categoria?sexo&infantil&tipos&marcas&lojas&cores&tamanhos&menorDesconto' +
                '&maiorDesconto&menorPreco&maiorPreco&temporada&comprimento&gola&manga&estilo&cano&saltoAltura' +
                '&salto&ocasiao&ordem&porPagina&pagina',
                views  : {
                    'container@' : {
                        templateUrl  : 'source/produtos/produtos.html',
                        controller   : 'Produtos',
                        controllerAs : 'vm'
                    }
                },
                params : config.produtos.params,
                resolve : {
                    user : security.checkLoggedIn
                },
                data: {
                    title: 'Produtos'
                }
            });
    }
})();
(function() {
    'use strict';

    angular
        .module('mooiweb.usuario')
        .config(usuarioRouteConfig);

    usuarioRouteConfig.$inject = ['$stateProvider', 'securityProvider'];
    function usuarioRouteConfig($stateProvider, securityProvider) {
        var security = securityProvider.$get();
        $stateProvider
            .state('root.usuario', {
                url     : '/usuario/:id',
                views   : {
                    'container@' : {
                        templateUrl  : 'source/usuario/usuario.html',
                        controller   : 'Usuario',
                        controllerAs : 'vm'
                    }
                },
                params  : {},
                resolve : {
                    user : security.ensureLoggedIn
                }
            });
    }

})();
(function() {
    'use strict';
    angular.module('mooiweb.cadastro')
        .controller('Cadastro', Cadastro);

    Cadastro.$inject = ['$rootScope', '$state', 'ui', 'db', 'helper', 'seo', 'notLogged'];
    function Cadastro($rootScope, $state, ui, db, helper, seo, notLogged) {
        var vm = this;
        vm.form = {};
        vm.form.gender = 'Feminino';
        vm.submit = submit;

        init();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function init() {
            ui.loadUI('Cadastro');
            seo.setTitle('Mooi.me - Cadastro');
            seo.setDescription('Cadastre-se na Mooi.me! Mantenha seus itens favoritos salvos e receba recomendações.');
            seo.setURL();
            seo.setImage('quadrado-medio.png', 252, 252, false);
            seo.setPrice();
            helper.log('Cadastro loaded - ' + notLogged);
            $rootScope.statusCode = 200;
            window.prerenderReady = true;
        }

        function submit(signupForm) {
            if(!!signupForm.$valid) {
                $rootScope.loading = true;
                db.signup(vm.form)
                    .then(function(user) {
                        if(!!user) {
                            return $state.go('root.home', {}, {inherit : false});
                        }
                    })['finally'](function() {
                    $rootScope.loading = false;
                });
            }
        }

    }

})();
(function() {
    'use strict';
    angular.module('mooiweb.home')
        .controller('Home', Home);

    Home.$inject = ['$rootScope', '$scope', 'seo', 'bi', 'config', 'helper', 'ui', 'usuario', 'user'];
    function Home($rootScope, $scope, seo, bi, config, helper, ui, usuario, user) {
        var vm = this;
        vm.accessories = "";
        vm.clothing = "";
        vm.shoes = "";
        vm.userName = usuario.getUserName(user);
        vm.userGender = usuario.getUserGender(user);
        vm.searchEnabled = config.mooiweb.searchEnabled;

        init();

        /////////////////////////

        function init() {
            getRandomPhotos();
            ui.loadUI("Home");
            seo.setTitle('Mooi.me');
            seo.setDescription('A sua vitrine da moda na internet');
            seo.setURL();
            seo.setImage('quadrado-medio.png', 252, 252, false);
            seo.setPrice();
            $scope.$on('userChanged', function(event, user) {
                vm.user = user;
                vm.userName = usuario.getUserName(user);
                vm.userGender = usuario.getUserGender(user);
            });
            $rootScope.statusCode = 200;
            window.prerenderReady = true;
        }

        function getRandomPhotos() {
            var accessories = [
                "acessorios1.jpg",
                "acessorios2.jpg",
                "acessorios3.jpg"
            ];
            vm.accessories = helper.getRandomFromArray(accessories);
            var clothing = [
                "roupas1.jpg",
                "roupas2.jpg",
                "roupas3.jpg"
            ];
            vm.clothing = helper.getRandomFromArray(clothing);
            var shoes = [
                "sapatos1.jpg",
                "sapatos2.jpg",
                "sapatos3.jpg"
            ];
            vm.shoes = helper.getRandomFromArray(shoes);
        }

    }

})();
(function() {
    'use strict';
    angular
        .module('mooiweb.item')
        .controller('Item', Item);

    Item.$inject = ['$window', '$scope', '$rootScope', '$stateParams', '$filter', 'ui', 'db', 'storage', 'seo', 'bi', 'user'];
    function Item($window,  $scope, $rootScope, $stateParams, $filter, ui, db, storage, seo, bi, user) {
        var vm = this;
        vm.photoIndex = 0; // Qual foto da lista de fotos aparece
        vm.changeImage = changeImage;
        vm.setItemViewed = setItemViewed;
        vm.query = [{id: $stateParams.id, store: $stateParams.loja}];
        vm.viewed = storage.getViewed();
        vm.related = [];
        vm.suggested = [];
        vm.likeItem = likeItem;
        vm.dislikeItem = dislikeItem;
        vm.url = $window.location.origin + $window.location.pathname;
        vm.conversion = conversion;
        vm.interest = interest;
        vm.getImageURL = getImageURL;
        $rootScope.statusCode = 200;

        init();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////


        function init() {
            ui.loadUI('Item');
            getItem();
            $scope.$on('ALWAYS', function(event) {
                ui.loadUI('Item');
            });
        }

        function getItem() {
            $rootScope.loading = true;
            db.getItemsById(vm.query)
                .then(function(item) { // Sucesso
                    seo.setTitle('Mooi.me - ' + item.nome);
                    seo.setDescription($filter('lineBreakCorrection')(item.descricao, true));
                    seo.setURL();
                    seo.setImage(item.images[0], 252, 252, true);
                    seo.setPrice(item.preco_vista);
                    vm.item = item;
                    vm.related = storage.getRelated(vm.item);
                    ui.singleProductUI(vm.item.loja);
                    sentimentSetup();
                    return vm.item;
                }, function(reason) { // Falha
                    vm.error = reason.message;
                    return vm.error;
                })['finally'](function() {
                $rootScope.loading = false;
                window.prerenderReady = true;
            });
        }

        function changeImage(index) {
            vm.photoIndex = index;
        }

        function setItemViewed(item) {
            return storage.setViewed(item);
        }

        function likeItem(item) {
            return sentiment(item, 'likeItem');
        }

        function dislikeItem(item) {
            return sentiment(item, 'dislikeItem');
        }

        function sentiment(item, sentimentFunction) {
            var id = {id_externo : item.id_externo, loja : item.loja};
            db[sentimentFunction](id)
                .then(function(newUser) { // Sucesso
                    user = newUser;
                    sentimentSetup();
                }, function(reason) { // Falha
                    console.log(reason);
                })['finally'](function() {
                //$rootScope.loading = false;
            });
        }

        function sentimentSetup() {
            if(!!user) {
                var likes = user.likes;
                var dislikes = user.dislikes;
                var i, j;
                var id_externo;
                var loja;
                // TODO: otimizar isso
                for(i = 0; i < likes.length; i += 1) {
                    if(vm.item.id_externo === likes[i].id_externo && vm.item.loja === likes[i].loja) {
                        vm.item.liked = true;
                    }
                }
                for(i = 0; i < dislikes.length; i += 1) {
                    if(vm.item.id_externo === dislikes[i].id_externo && vm.item.loja === dislikes[i].loja) {
                        vm.item.disliked = true;
                    }
                }
                if(!!vm.viewed) {
                    for(i = 0; i < vm.viewed.length; i += 1) {
                        id_externo = vm.viewed[i].id_externo;
                        loja = vm.viewed[i].loja;
                        vm.viewed[i].liked = false;
                        vm.viewed[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.viewed[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.viewed[i].disliked = true;
                            }
                        }
                    }
                }

                if(!!vm.related) {
                    for(i = 0; i < vm.related.length; i += 1) {
                        id_externo = vm.related[i].id_externo;
                        loja = vm.related[i].loja;
                        vm.related[i].liked = false;
                        vm.related[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.related[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.related[i].disliked = true;
                            }
                        }
                    }
                }

                if(!!vm.suggested) {
                    for(i = 0; i < vm.suggested.length; i += 1) {
                        id_externo = vm.suggested[i].id_externo;
                        loja = vm.suggested[i].loja;
                        vm.suggested[i].liked = false;
                        vm.suggested[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.suggested[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.suggested[i].disliked = true;
                            }
                        }
                    }
                }

            }
        }

        function conversion(item) {
            var category = item.macro_categorias[0];
            var store = item.loja;
            bi.facebookPixel.conversion();
            bi.analytics.sendEvent(category, "Clique", store);
        }

        function interest(item) {
            var category = item.macro_categorias[0];
            var store = item.loja;
            bi.analytics.sendEvent(category, "Ver mais", store);
        }

        function getImageURL(item, index) {
            if(!item || isNaN(parseInt(index))) {
                return '';
            }
            if(item.images[index].indexOf('s3-sa-east-1.amazonaws.com') > -1) {
                return item.images[index];
            }
            else {
                var url = 'http://s3-sa-east-1.amazonaws.com/mooi-items/images/produtos/';
                url += item.loja;
                url += '/';
                url += item.images[index];
                return url;
            }
        }

    }
})();
(function() {
    'use strict';
    angular.module('mooiweb.layout')
        .controller('Footer', Footer);

    Footer.$inject = ['$rootScope', 'db', 'ui', 'user'];
    function Footer($rootScope, db, ui, user) {
        var vm = this;
        vm.submit = submit;
        vm.sent = false;

        init();

        function init() {

        }

        function submit(subscribeForm) {
            if(!!subscribeForm.$valid && !vm.sent) {
                $rootScope.loading = true;
                db.subscribe(vm.email)
                    .then(function(success) {
                        if(!!success) {
                            vm.sent = true;
                        }
                    })['finally'](function() {
                    $rootScope.loading = false;
                });
            }
        }
    }

})();
(function() {
    'use strict';
    angular.module('mooiweb.layout')
        .controller('Header', Header);

    Header.$inject = ['$filter', '$scope', '$state', 'helper', 'config', 'db', 'usuario', 'ui', 'user'];
    function Header($filter, $scope, $state, helper, config, db, usuario, ui, user) {
        var vm = this;
        vm.user = user;
        vm.userName = usuario.getUserName(user);
        vm.logout = logout;
        vm.cadastro = cadastro;
        vm.searchEnabled = config.mooiweb.searchEnabled;
        vm.menus = menus();

        init();

        ///////////////////////////////

        function init() {
            helper.log('Header Loaded');
            $scope.$on('userChanged', function(event, user) {
                vm.user = user;
                vm.userName = usuario.getUserName(user);
            });

            $scope.$on('headerMenusRendered', function(event) {
                ui.loadUI('Header');
            });
        }

        function logout() {
            return db.logout();
        }

        function cadastro() {
            return $state.go('root.cadastro', {}, {inherit : false});
        }

        function menus() {
            var feminine = {
                name : 'FEMININO',
                gender : 'f',
                infantile : false,
                submenus : [
                    {
                        name  : 'ACESSÓRIOS FEMININOS',
                        category : 'acessorios',
                        items : [
                            "acessórios",
                            "bolsas",
                            "bijuterias",
                            "relógios",
                            "cintos",
                            "carteiras",
                            "óculos",
                            "mochilas",
                            "necessaires",
                            "lenços",
                            "gorros",
                            "chapéus",
                            "bonés",
                            "fitness",
                            "luvas",
                            "cachecóis",
                            "meias"
                        ]
                    },
                    {
                        name : 'ROUPAS FEMININAS',
                        category : 'vestuario',
                        items : [
                            "blusas",
                            "vestidos",
                            "camisetas",
                            "calças",
                            "saias",
                            "shorts",
                            "camisas",
                            "moda-praia",
                            "casacos",
                            "bermudas",
                            "calcinhas",
                            "tops",
                            "macacões",
                            "biquinis",
                            "regatas",
                            "jaquetas",
                            "fitness",
                            "suéteres",
                            "pijamas",
                            "leggings",
                            "camisolas",
                            "blazers",
                            "conjuntos",
                            "meias",
                            "coletes",
                            "bodies",
                            "short",
                            "moletons",
                            "lingeries",
                            "modeladores e cintas",
                            "maiôs",
                            "moda praia",
                            "agasalhos",
                            "sobretudos",
                            "polos",
                            "tunicas",
                            "gestante",
                            "boleros",
                            "bolsas",
                            "sapatilhas",
                            "cintos",
                            "sutiãs",
                            "carteiras"
                        ]
                    },
                    {
                        name : 'CALÇADOS FEMININOS',
                        category : 'calcados',
                        items : [
                            "calçados",
                            "sapatilhas",
                            "rasteiras",
                            "tênis",
                            "scarpins",
                            "botas",
                            "sapatos",
                            "sandálias",
                            "mocassins",
                            "chinelos",
                            "anabelas",
                            "alpargatas",
                            "ankle boots",
                            "birkens",
                            "slippers",
                            "tamancos",
                            "cardigans",
                            "suéteres",
                            "oxfords",
                            "casacos",
                            "meias",
                            "sneakers",
                            "sapatênis",
                            "jaquetas"
                        ]
                    }
                ]
            };
            var masculine = {
                name : 'MASCULINO',
                gender : 'm',
                infantile : false,
                submenus : [
                    {
                        name  : 'ACESSÓRIOS MASCULINOS',
                        category: 'acessorios',
                        items : [
                            "acessórios",
                            "relógios",
                            "cintos",
                            "bonés",
                            "carteiras",
                            "óculos",
                            "mochilas",
                            "gorros",
                            "bolsas",
                            "necessaires",
                            "chapéus",
                            "gravatas",
                            "cachecóis",
                            "lenços"
                        ]
                    },
                    {
                        name : 'ROUPAS MASCULINAS',
                        category: 'vestuario',
                        items : [
                            "camisetas",
                            "bermudas",
                            "camisas",
                            "blusas",
                            "polos",
                            "calças",
                            "jaquetas",
                            "suéteres",
                            "casacos",
                            "cuecas",
                            "sungas",
                            "meias",
                            "moletons",
                            "shorts",
                            "agasalhos",
                            "regatas",
                            "fitness",
                            "conjuntos",
                            "blazers",
                            "pijamas",
                            "coletes",
                            "short",
                            "tops",
                            "ternos",
                            "sapatos",
                            "macacões",
                            "cintos",
                            "saias",
                            "acessórios",
                            "skate",
                            "sneakers",
                            "relógios",
                            "vestidos",
                            "tunicas",
                            "mochilas",
                            "moda-praia",
                            "gorros",
                            "cosméticos",
                            "mocassins"
                        ]
                    },
                    {
                        name : 'CALÇADOS MASCULINOS',
                        category: 'calcados',
                        items : [
                            "calçados",
                            "tênis",
                            "sapatênis",
                            "botas",
                            "sapatos",
                            "chinelos",
                            "mocassins",
                            "sandálias",
                            "papetes",
                            "sapatilhas",
                            "cardigans",
                            "chuteiras",
                            "suéteres",
                            "skate",
                            "sneakers",
                            "camisetas",
                            "oxfords",
                            "slippers",
                            "jaquetas",
                            "scarpins",
                            "alpargatas"
                        ]
                    }
                ]
            };
            var infantile = {
                name : 'INFANTIL',
                gender : null,
                infantile : true,
                submenus : [
                    {
                        name  : 'ACESSÓRIOS INFANTIS',
                        category: 'acessorios',
                        items : [
                            "acessórios",
                            "mochilas",
                            "bolsas",
                            "cintos",
                            "bonés",
                            "óculos",
                            "gestante",
                            "meias",
                            "cachecóis",
                            "necessaires"
                        ]
                    },
                    {
                        name : 'ROUPAS INFANTIS',
                        category: 'vestuario',
                        items : [
                            "conjuntos",
                            "blusas",
                            "camisetas",
                            "calças",
                            "vestidos",
                            "regatas",
                            "casacos",
                            "bermudas",
                            "camisas",
                            "saias",
                            "pijamas",
                            "jaquetas",
                            "macacões",
                            "shorts",
                            "meias",
                            "bodies",
                            "cuecas",
                            "moletons",
                            "sungas",
                            "suéteres",
                            "polos",
                            "coletes",
                            "leggings",
                            "acessórios",
                            "bolsas",
                            "calcinhas",
                            "cintos",
                            "agasalhos",
                            "short",
                            "gorros",
                            "sobretudos",
                            "blazers",
                            "boleros",
                            "tops",
                            "camisolas"
                        ]
                    },
                    {
                        name : 'CALÇADOS INFANTIS',
                        category: 'calcados',
                        items : [
                            "calçados",
                            "tênis",
                            "sandálias",
                            "sapatilhas",
                            "botas",
                            "chinelos",
                            "sapatos",
                            "sapatênis",
                            "papetes",
                            "rasteiras",
                            "mocassins",
                            "casacos",
                            "cardigans",
                            "tamancos"
                        ]
                    }
                ]
            };
            var accessories = {
                name : 'ACESSÓRIOS',
                gender : null,
                infantile : false,
                submenus : [
                    {
                        name : 'ACESSÓRIOS',
                        category: 'acessorios',
                        items : [
                            "acessórios",
                            "bolsas",
                            "relógios",
                            "cintos",
                            "bijuterias",
                            "carteiras",
                            "óculos",
                            "mochilas",
                            "bonés",
                            "gorros",
                            "necessaires",
                            "lenços",
                            "chapéus",
                            "luvas",
                            "gravatas",
                            "cachecóis",
                            "gestante",
                            "fitness",
                            "meias"
                        ]
                    }
                ]
            };
            var menus = [
                feminine,
                masculine,
                infantile
                //accessories,
            ];
            return menus;
        }
    }

})();
(function() {
    'use strict';
    angular.module('mooiweb.layout')
        .controller('OffCanvas', OffCanvas);

    OffCanvas.$inject = ['$scope', '$rootScope', '$state', 'helper', 'db', 'ui', 'usuario', 'user'];
    function OffCanvas($scope, $rootScope, $state, helper, db, ui, usuario, user) {
        var vm = this;
        vm.submitLogin = submitLogin;
        vm.logout = logout;
        vm.form = {};
        vm.user = user;
        vm.userGender = usuario.getUserGender(user);

        init();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function init() {
            helper.log('Off-Canvas loaded');
            $scope.$on('userChanged', function(event, user) {
                vm.user = user;
                vm.userGender = usuario.getUserGender(user);
            });
            //$scope.$on('HeaderLoaded', function(event, data) {
            //    ui.loadUI('Off-Canvas');
            //});
        }

        function submitLogin() {
            $rootScope.loading = true;
            db.login(vm.form)
                .then(function(user) {
                    $rootScope.user = user;
                    vm.form.password = undefined;
                    ui.closeLoginModal();
                    return $state.go('root.usuario', {}, {inherit: false});
                })['finally'](function() {
                $rootScope.loading = false;
            });
        }

        function logout() {
            $rootScope.loading = true;
            db.logout()
                .then(function(message) {
                    return $state.go('root.home', {}, {inherit: false});
                })['finally'](function() {
                $rootScope.loading = false;
            });
        }
    }

})();
(function() {
    'use strict';
    angular.module('mooiweb.oops')
        .controller('Oops', Oops);

    Oops.$inject = ['$rootScope', 'seo', 'ui', 'user'];
    function Oops($rootScope, seo, ui, user) {
        var vm = this;
        seo.setTitle('Mooi.me - Página não encontrada');
        seo.setDescription('Página não encontrada!');
        seo.setURL();
        seo.setImage('quadrado-medio.png', 252, 252, false);
        seo.setPrice();
        init();

        function init() {
            $rootScope.statusCode = 404;
            ui.loadUI('Oops');
            window.prerenderReady = true;
        }
    }

})();
(function() {
    'use strict';
    angular
        .module('mooiweb.produtos')
        .controller('Produtos', Produtos);

    Produtos.$inject = [
        '$scope', '$rootScope', '$state', '$filter', '$stateParams', 'ui', 'db', 'helper', 'config', 'storage', 'seo', 'bi', 'user'
    ];
    function Produtos($scope, $rootScope, $state, $filter, $stateParams, ui, db, helper, config, storage, seo, bi, user) {
        var vm = this;
        vm.resetAllFilters = resetAllFilters;
        vm.resetFilters = resetFilters;
        vm.setItemViewed = setItemViewed;
        vm.likeItem = likeItem;
        vm.dislikeItem = dislikeItem;
        vm.conversion = conversion;
        vm.interest = interest;
        vm.getImageURL = getImageURL;
        $rootScope.statusCode = 200;
        //vm.toggleGender
        //vm.toggleColor

        init();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Toda mudança é refletida na URL e a página é recalculada

        function init() {
            try {
                getParams();
                validateParams();
                pageSetup();
                getItems();
                ui.loadUI('Produtos');
                $scope.$on('ALWAYS', function(event) {
                    ui.loadUI('Produtos');
                });
                $scope.$on('productsRendered', function(event) {
                    ui.productsMouseOver();
                });
                helper.log('Produtos loaded - ' + user);
            }
            catch(e) {
                return $state.go('root.home');
            }
        }

        function getParams() {
            getURLParams();
            getControlParams();
            getDefaultParams();

            function getURLParams() {
                vm.query = {};
                vm.query.conditions = {}; // Filtros
                vm.query.options = {};    // Ordenação e quantidade
                // Filtros
                vm.query.conditions.category = $stateParams.categoria;             // Categoria (vestuário, acessorios,
                                                                                   // calcados)
                vm.query.conditions.gender = $stateParams.sexo;                    // Sexo ([f]eminino, [m]asculino)
                vm.query.conditions.infantile = JSON.parse($stateParams.infantil); // Infantil (true, false)
                vm.query.conditions.types = $stateParams.tipos;                    // Tipos ([vestidos, peep toes],
                                                                                   // etc...)
                vm.query.conditions.brands = $stateParams.marcas;                  // Marcas ([Abercrombie, Hering],
                                                                                   // etc...)
                vm.query.conditions.stores = $stateParams.lojas;                   // Lojas ([Marisa], [Dafiti], etc...)
                vm.query.conditions.colors = $stateParams.cores;                   // Cores ([verde, amarelo], etc...)
                vm.query.conditions.sizes = $stateParams.tamanhos;                 // Tamanhos ([P], [G, 60], etc...)
                for(var i = 0; i < vm.query.conditions.sizes.length; i += 1) {
                    vm.query.conditions.sizes[i] = vm.query.conditions.sizes[i].replace("%2F", "/");
                }

                vm.query.conditions.lowPrice = parseInt($stateParams.menorPreco);        // Menor Preço em centavos (0,
                                                                                         // etc...)
                vm.query.conditions.highPrice = parseInt($stateParams.maiorPreco);       // Maior Preço em centavos
                                                                                         // (100000, etc...)
                vm.query.conditions.lowDiscount = parseInt($stateParams.menorDesconto);  // Menor desconto em
                                                                                         // porcentagem (0, 30, etc...)
                vm.query.conditions.highDiscount = parseInt($stateParams.maiorDesconto); // Maior desconto em
                                                                                         // porcentagem (100, 90,
                                                                                         // etc...)
                vm.query.conditions.available = true;                          // Disponível (hardcoded por enquanto)
                // extrainfo
                vm.query.conditions.season = $stateParams.temporada;       // Temporada (Verão, Inverno 2015, etc...)
                vm.query.conditions.length = $stateParams.comprimento;     // Comprimento (5cm, 10cm, etc...)
                vm.query.conditions.collar = $stateParams.gola;            // Gola (em V, canoa, etc...)
                vm.query.conditions.sleeve = $stateParams.manga;           // Manga (curta, longa, etc...)
                vm.query.conditions.style = $stateParams.estilo;           // Estilo (Dramática, moderno, etc...)
                vm.query.conditions.pipe = $stateParams.cano;              // Cano (5cm, alto, etc...)
                vm.query.conditions.heelHeight = $stateParams.saltoAltura; // Altura do salto (5cm, 8cm, etc...)
                vm.query.conditions.heel = $stateParams.salto;             // Tipo do salto (Fino, etc...)
                vm.query.conditions.occasion = $stateParams.ocasiao;       // Ocasião (Festa, etc...)
                // Opções
                vm.query.options.orderBy = $stateParams.ordem;     // Ordenação (menorPreco, maiorDesconto, etc...)
                vm.query.options.perPage = parseInt($stateParams.porPagina); // Itens por página (40, 80, etc...)
                vm.query.options.page = parseInt($stateParams.pagina);       // Página (1, 10, etc...)
            }

            function getControlParams() {
                vm.control = {};
                vm.control.minPrice = 0;
                vm.control.maxPrice = 200000;
                vm.control.minDiscount = 0;
                vm.control.maxDiscount = 100;
                // UI
                vm.control.filters = $stateParams.filtros || false;
                $rootScope.loading = false;
                function toggleFilterPanel(value, $event) {
                    vm.control.filters = value;
                    console.log($stateParams);
                    $stateParams.filtros = value;
                    refresh($stateParams, {notify : false, reload : false, inherit : true})
                }

                vm.toggleFilterPanel = toggleFilterPanel;
            }

            function getDefaultParams() {
                vm.control.default = config.produtos.params;
            }
        }

        function validateParams() {
            var isParamsValid = helper.validateQuery(vm.query, vm.control);
            return isParamsValid;
        }

        function pageSetup() {
            // --- Filtros --- //

            // Ordenação
            setUpOrder();
            // Sliders
            setUpSliders();
            // Sexo
            setUpGenders();
            // Infantil
            setUpInfantile();
            // Cores
            setUpColors();
            // Paginação
            setUpPagination();

            // --- Informação --- //
            vm.items = [];
            //vm.results = 0;
            setUpPageTitle();

            function setUpOrder() {
                vm.control.possibleOrders = [
                    {name : 'Menor Preço', value : 'menorPreco'},
                    {name : 'Maior Preço', value : 'maiorPreco'},
                    {name : 'Menor Desconto', value : 'menorDesconto'},
                    {name : 'Maior Desconto', value : 'maiorDesconto'}
                ];
                var queryOrder = vm.query.options.orderBy;
                for(var i = 0; i < vm.control.possibleOrders.length; i += 1) {
                    if(vm.control.possibleOrders[i].value === queryOrder) {
                        vm.control.order = vm.control.possibleOrders[i];
                    }
                }
                $scope.$watch(function watchVariable() {
                    return (vm.control.order);
                }, function(current, original) {
                    if(current.value !== original.value) {
                        var currentOrderByValue = current.value;
                        return refresh({ordem : currentOrderByValue});
                    }
                });
            }

            function setUpSliders() {
                vm.control.sliders = {};
                setUpPriceRange();
                setUpDiscountRange();

                function setUpPriceRange() {
                    vm.control.sliders.price = {};
                    vm.control.sliders.price.values = [vm.query.conditions.lowPrice, vm.query.conditions.highPrice];
                    vm.control.sliders.price.previous = [].concat(vm.control.sliders.price.values);
                    vm.control.sliders.price.options = {
                        orientation : 'horizontal',
                        min         : vm.control.minPrice,
                        max         : vm.control.maxPrice,
                        step        : 100,
                        range       : true,
                        change      : function refreshPrice(event, data) {
                            // Bate com o valor anterior
                            if(vm.control.sliders.price.values[0] !== vm.control.sliders.price.previous[0]) {
                                var newlowPrice = vm.control.sliders.price.values[0];
                                return refresh({menorPreco : newlowPrice});
                            }
                            else if(vm.control.sliders.price.values[1] !== vm.control.sliders.price.previous[1]) {
                                var newHighPrice = vm.control.sliders.price.values[1];
                                return refresh({maiorPreco : newHighPrice});
                            }
                        }
                    };
                }

                function setUpDiscountRange() {
                    vm.control.sliders.discount = {};
                    vm.control.sliders.discount.values = [
                        vm.query.conditions.lowDiscount, vm.query.conditions.highDiscount
                    ];
                    vm.control.sliders.discount.previous = [].concat(vm.control.sliders.discount.values);
                    vm.control.sliders.discount.options = {
                        orientation : 'horizontal',
                        min         : vm.control.minDiscount,
                        max         : vm.control.maxDiscount,
                        step        : 1,
                        range       : true,
                        change      : function refreshDiscount(event, data) {
                            // Bate com o valor anterior
                            if(vm.control.sliders.discount.values[0] !== vm.control.sliders.discount.previous[0]) {
                                var newLowDiscount = vm.control.sliders.discount.values[0];
                                return refresh({menorDesconto : newLowDiscount});
                            }
                            else if(vm.control.sliders.discount.values[1] !== vm.control.sliders.discount.previous[1]) {
                                var newHighDiscount = vm.control.sliders.discount.values[1];
                                return refresh({maiorDesconto : newHighDiscount});
                            }
                        }
                    };
                }
            }

            function setUpGenders() {
                vm.control.possibleGenders = [
                    {
                        name     : 'Feminino',
                        value    : 'f',
                        selected : false
                    }, {
                        name     : 'Masculino',
                        value    : 'm',
                        selected : false
                    }
                ];
                var queryGender = vm.query.conditions.gender; // Array
                for(var i = 0; i < vm.control.possibleGenders.length; i += 1) {
                    if(queryGender.indexOf(vm.control.possibleGenders[i].value) !== -1) {
                        vm.control.possibleGenders[i].selected = true;
                    }
                }
                function toggleGender(gender, $event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    if($rootScope.loading === false) {
                        $rootScope.loading = true;
                        var newGender;
                        for(var i = 0; i < vm.control.possibleGenders.length; i += 1) {
                            var current = vm.control.possibleGenders[i];
                            if(current.value === gender) {
                                if(current.selected === true) {
                                    newGender = gender;
                                }
                                else {
                                    newGender = [];
                                }
                            }
                        }
                        return refresh({sexo : newGender, categoria : vm.query.conditions.category}, {inherit : false});
                    }
                }

                vm.toggleGender = toggleGender;
            }

            function setUpInfantile() {
                var queryInfantile = vm.query.conditions.infantile; // Boolean
                vm.control.infantile = {
                    name     : 'Infantil',
                    selected : queryInfantile
                };
                function toggleInfantile($event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    if($rootScope.loading === false) {
                        $rootScope.loading = true;
                        var infantile = JSON.stringify(vm.control.infantile.selected);
                        return refresh({infantil : infantile});
                    }
                }

                vm.toggleInfantile = toggleInfantile;
            }

            function setUpColors() {
                vm.control.possibleColors = [
                    {
                        name     : "Branco",
                        selected : false,
                        value    : "branco"
                    },
                    {
                        name     : "Amarelo",
                        selected : false,
                        value    : "amarelo"
                    },
                    {
                        name     : "Azul",
                        selected : false,
                        value    : "azul"
                    },
                    {
                        name     : "Cinza",
                        selected : false,
                        value    : "cinza"
                    },
                    {
                        name     : "Bege",
                        selected : false,
                        value    : "bege"
                    },
                    {
                        name     : "Laranja",
                        selected : false,
                        value    : "laranja"
                    },
                    {
                        name     : "Verde",
                        selected : false,
                        value    : "verde"
                    },
                    {
                        name     : "Marrom",
                        selected : false,
                        value    : "marrom"
                    },
                    {
                        name     : "Rosa",
                        selected : false,
                        value    : "rosa"
                    },
                    {
                        name     : "Vermelho",
                        selected : false,
                        value    : "vermelho"
                    },
                    {
                        name     : "Roxo",
                        selected : false,
                        value    : "roxo"
                    },
                    {
                        name     : "Preto",
                        selected : false,
                        value    : "preto"
                    }
                ];
                var queryColors = vm.query.conditions.colors; // Array
                for(var i = 0; i < vm.control.possibleColors.length; i += 1) {
                    if(queryColors.indexOf(vm.control.possibleColors[i].value) !== -1) {
                        vm.control.possibleColors[i].selected = true;
                    }
                }
                function toggleColor($event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    if($rootScope.loading === false) {
                        $rootScope.loading = true;
                        var colors = [];
                        for(var i = 0; i < vm.control.possibleColors.length; i += 1) {
                            var current = vm.control.possibleColors[i];
                            if(current.selected === true) {
                                colors.push(current.value);
                            }
                        }
                        return refresh({cores : colors});
                    }
                }

                vm.toggleColor = toggleColor;
            }

            function setUpPagination() {
                vm.changePage = function(newPage, $event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    return refresh({pagina : newPage});
                }
            }

            function setUpPageTitle() {
                var category = $filter('accentCorrection')(vm.query.conditions.category);
                category = $filter('capitalize')(category, false);
                // category = "Acessórios", "Calçados" ou "Vestuário"
                var plural = true;
                if(category === 'Vestuário') {
                    plural = false;
                }
                if(!!vm.query.conditions.infantile) {
                    if(plural) {
                        category += ' Infantis'
                    }
                    else {
                        category += ' Infantil'
                    }
                }
                if(vm.query.conditions.gender.indexOf('m') !== -1 && vm.query.conditions.gender.indexOf('f') === -1) {
                    if(plural) {
                        category += ' Masculinos'
                    }
                    else {
                        category += ' Masculino'
                    }
                }
                else if(vm.query.conditions.gender.indexOf('f') !== -1 && vm.query.conditions.gender.indexOf('m') === -1) {
                    if(plural) {
                        category += ' Femininos'
                    }
                    else {
                        category += ' Feminino'
                    }
                }
                vm.pageTitle = category;
            }
        }

        function pageDynamicSetup() {

            setUpTypes();
            setUpSizes();

            function setUpTypes() {
                if(vm.items.length !== 0) {
                    vm.items[0].distinct_types.sort();
                    vm.control.possibleTypes = [];
                    for(var j = 0; j < vm.items[0].distinct_types.length; j += 1) {
                        vm.control.possibleTypes.push({
                            value    : vm.items[0].distinct_types[j],
                            selected : false
                        });
                    }
                    var queryTypes = vm.query.conditions.types; // Array
                    for(var i = 0; i < vm.control.possibleTypes.length; i += 1) {
                        if(queryTypes.indexOf(vm.control.possibleTypes[i].value) !== -1) {
                            vm.control.possibleTypes[i].selected = true;
                        }
                    }
                }
                function toggleType($event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    if($rootScope.loading === false) {
                        $rootScope.loading = true;
                        var types = [];
                        for(var i = 0; i < vm.control.possibleTypes.length; i += 1) {
                            var current = vm.control.possibleTypes[i];
                            if(current.selected === true) {
                                types.push(current.value);
                            }
                        }
                        return refresh({tipos : types});
                    }
                }
                vm.toggleType = toggleType;
            }

            function setUpSizes() {
                if(vm.items.length !== 0) {
                    vm.items[0].distinct_sizes.sort();
                    vm.control.possibleSizes = [];
                    for(var j = 0; j < vm.items[0].distinct_sizes.length; j += 1) {
                        vm.control.possibleSizes.push({
                            value    : vm.items[0].distinct_sizes[j],
                            selected : false
                        });
                    }
                    var querySizes = vm.query.conditions.sizes; // Array
                    for(var i = 0; i < vm.control.possibleSizes.length; i += 1) {
                        if(querySizes.indexOf(vm.control.possibleSizes[i].value) !== -1) {
                            vm.control.possibleSizes[i].selected = true;
                        }
                    }
                }
                function toggleSize($event) {
                    if($event) {
                        $event.stopPropagation();
                        $event.preventDefault();
                    }
                    if($rootScope.loading === false) {
                        $rootScope.loading = true;
                        var sizes = [];
                        for(var i = 0; i < vm.control.possibleSizes.length; i += 1) {
                            var current = vm.control.possibleSizes[i];
                            if(current.selected === true) {
                                sizes.push(current.value);
                            }
                        }
                        return refresh({tamanhos : sizes});
                    }
                }

                vm.toggleSize = toggleSize;
            }

        }

        function sentimentSetup() {
            if(!!user && !!vm.items) {
                var likes = user.likes;
                var dislikes = user.dislikes;
                for(var i = 0; i < vm.items.length; i += 1) {
                    var id_externo = vm.items[i].id_externo;
                    var loja = vm.items[i].loja;
                    vm.items[i].liked = false;
                    vm.items[i].disliked = false;
                    for(var j = 0; j < likes.length; j += 1) {
                        if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                            vm.items[i].liked = true;
                        }
                    }
                    for(j = 0; j < dislikes.length; j += 1) {
                        if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                            vm.items[i].disliked = true;
                        }
                    }
                }
            }
        }

        function SEOSetup() {
            seo.setTitle('Mooi.me - ' + vm.pageTitle);
            var pageTitle = vm.pageTitle.toLowerCase();
            pageTitle = $filter('capitalize')(pageTitle, false);
            seo.setDescription(pageTitle + ' de várias lojas na Mooi.me. Compare preços e encontre os melhores produtos!');
            seo.setURL();
            var imageName = vm.pageTitle;
            imageName = imageName.replace('ó', 'o').replace('ç', 'c').replace('á', 'a');
            imageName = imageName.split(' ').join('');
            seo.setImage('http://' + config.mooiweb.host + '/img/' + imageName + '.jpg', 640, 425, true);
            seo.setPrice();
        }

        function getItems() {
            $rootScope.loading = true;
            db.getItems(vm.query)
                .then(function(response) { // Sucesso
                    vm.items = response;
                    sentimentSetup();
                    pageDynamicSetup();
                    SEOSetup();
                    if(response.length === 0) {
                        vm.results = 0;
                    }
                    else {
                        vm.results = vm.items[0].item_count;
                    }
                    return vm.items;
                }, function(reason) { // Falha
                    vm.error = reason.message;
                    return vm.error;
                })['finally'](function() {
                $rootScope.loading = false;
                window.prerenderReady = true;
            });
        }

        function resetAllFilters($event) {
            if($event) {
                $event.stopPropagation();
                $event.preventDefault();
            }
            var safeParameters = [
                'categoria',
                'sexo',
                'infantil',
                'ordem',
                'porPagina'
            ];
            var newParameters = {};
            for(var parameter in $stateParams) {
                if($stateParams.hasOwnProperty(parameter)) {
                    // Não está na lista de parâmetros não resetáveis
                    if(safeParameters.indexOf(parameter) === -1) {
                        newParameters[parameter] = vm.control.default[parameter].value;
                    }
                }
            }
            return refresh(newParameters);
        }

        function resetFilters(filters, $event) {
            if($event) {
                $event.stopPropagation();
                $event.preventDefault();
            }
            var newParameters = {};
            for(var i = 0; i < filters.length; i += 1) {
                newParameters[filters[i]] = vm.control.default[filters[i]].value;
            }
            return refresh(newParameters);
        }

        function refresh(params, options) {
            if(params.pagina === undefined) {
                params.pagina = 1;
            }
            options = options || {notify : true, reload : true};
            //return refresh({menorDesconto : 10});
            return $state.go('.', params, options);
        }

        function setItemViewed(item) {
            setRelated(item);
            return storage.setViewed(item);
        }

        function setRelated(item) {
            return storage.setRelated(item, vm.items);
        }

        function likeItem(item) {
            return sentiment(item, 'likeItem');
        }

        function dislikeItem(item) {
            return sentiment(item, 'dislikeItem');
        }

        function sentiment(item, sentimentFunction) {
            //$rootScope.loading = true;
            var id = {id_externo : item.id_externo, loja : item.loja};
            db[sentimentFunction](id)
                .then(function(newUser) { // Sucesso
                    user = newUser;
                    sentimentSetup();
                }, function(reason) { // Falha
                    console.log(reason);
                })['finally'](function() {
                //$rootScope.loading = false;
            });
        }

        function conversion(item) {
            var category = item.macro_categorias[0];
            var store = item.loja;
            bi.facebookPixel.conversion();
            bi.analytics.sendEvent(category, "Clique", store);
        }

        function interest(item) {
            var category = item.macro_categorias[0];
            var store = item.loja;
            bi.analytics.sendEvent(category, "Ver mais", store);
        }

        function getImageURL(item, index) {
            if(!item || isNaN(parseInt(index))) {
                return '';
            }
            if(item.images[index].indexOf('s3-sa-east-1.amazonaws.com') > -1) {
                return item.images[index];
            }
            else {
                var url = 'http://s3-sa-east-1.amazonaws.com/mooi-items/images/produtos/';
                url += item.loja;
                url += '/';
                url += item.images[index];
                return url;
            }
        }

    }
})();
(function() {
    'use strict';
    angular.module('mooiweb.usuario')
        .controller('Usuario', Usuario);

    Usuario.$inject = ['$rootScope', '$scope', 'ui', 'storage', 'db', 'usuario', 'user'];
    function Usuario($rootScope, $scope, ui, storage, db, usuario, user) {
        var vm = this;
        vm.user = user;
        vm.email = usuario.getUserEmail(user);
        vm.name = usuario.getUserName(user);
        vm.viewed = storage.getViewed();
        vm.liked = [];
        vm.suggested = [];
        vm.setItemViewed = setItemViewed;
        vm.likeItem = likeItem;
        vm.dislikeItem = dislikeItem;
        $rootScope.title = vm.name;
        $rootScope.statusCode = 200;

        init();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function init() {
            ui.loadUI('Usuario');
            getLikedItems();
            $scope.$on('ALWAYS', function(event) {
                ui.loadUI('Usuario');
            });
        }

        function getLikedItems() {
            var likedList = [];
            var liked = user.likes;
            for(var i = 0; i < liked.length; i += 1) {
                likedList.push({
                    id: liked[i].id_externo,
                    store: liked[i].loja
                });
            }
            if(likedList.length > 0) {
                db.getItemsById(likedList)
                    .then(function(response) { // Sucesso
                        vm.liked = response;
                        ui.loadUI('Usuario');
                        return vm.liked;
                    }, function(reason) { // Falha
                        //vm.error = reason.message;
                        //return vm.error;
                    })['finally'](function() {
                    sentimentSetup();
                    //$rootScope.loading = false;
                });
            }
            sentimentSetup();
        }

        function setItemViewed(item) {
            return storage.setViewed(item);
        }

        function likeItem(item) {
            return sentiment(item, 'likeItem');
        }

        function dislikeItem(item) {
            return sentiment(item, 'dislikeItem');
        }

        function sentiment(item, sentimentFunction) {
            var id = {id_externo : item.id_externo, loja : item.loja};
            db[sentimentFunction](id)
                .then(function(newUser) { // Sucesso
                    user = newUser;
                    sentimentSetup();
                }, function(reason) { // Falha
                    console.log(reason);
                })['finally'](function() {
                //$rootScope.loading = false;
            });
        }

        function sentimentSetup() {
            if(!!user) {
                var likes = user.likes;
                var dislikes = user.dislikes;
                var i, j;
                var id_externo;
                var loja;
                // TODO: otimizar isso
                if(!!vm.viewed) {
                    for(i = 0; i < vm.viewed.length; i += 1) {
                        id_externo = vm.viewed[i].id_externo;
                        loja = vm.viewed[i].loja;
                        vm.viewed[i].liked = false;
                        vm.viewed[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.viewed[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.viewed[i].disliked = true;
                            }
                        }
                    }
                }

                if(!!vm.liked) {
                    for(i = 0; i < vm.liked.length; i += 1) {
                        id_externo = vm.liked[i].id_externo;
                        loja = vm.liked[i].loja;
                        vm.liked[i].liked = false;
                        vm.liked[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.liked[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.liked[i].disliked = true;
                            }
                        }
                    }
                }

                if(!!vm.suggested) {
                    for(i = 0; i < vm.suggested.length; i += 1) {
                        id_externo = vm.suggested[i].id_externo;
                        loja = vm.suggested[i].loja;
                        vm.suggested[i].liked = false;
                        vm.suggested[i].disliked = false;
                        for(j = 0; j < likes.length; j += 1) {
                            if(id_externo === likes[j].id_externo && loja === likes[j].loja) {
                                vm.suggested[i].liked = true;
                            }
                        }
                        for(j = 0; j < dislikes.length; j += 1) {
                            if(id_externo === dislikes[j].id_externo && loja === dislikes[j].loja) {
                                vm.suggested[i].disliked = true;
                            }
                        }
                    }
                }

            }
        }

    }

})();