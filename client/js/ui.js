jQuery(document).ready(function($) {
    // Abrir o input de busca no header: adicionar a classe "is-active"
    $(document).on('click', '.main-header .btn-input-search', function(e) {
        e.preventDefault();
        $('.input-search-label').addClass('is-active');
    }); // OK

    // Expande o submenu no hover do botão
    $(document).on('mouseover', '.main-header .btn-my-account', function() {
        // $('.hold-my-account-submenu').slideDown({duration: 200, queue: false});
        $('.hold-my-account-submenu').addClass('is-active');
    }); // OK

    // Colapsa o submenu no hover do botão
    $(document).on('mouseleave', '.main-header .btn-my-account', function() {
        // $('.hold-my-account-submenu').slideUp({duration: 200, queue: false});
        $('.hold-my-account-submenu').removeClass('is-active');
    }); // OK

    $(document).on('click', '.btn-close-input-search', function() {
        console.log('click');
        $('.input-search-label').removeClass('is-active');
    }); // OK

    $(document).on('hover', '.navigation .main-nav .main-nav-item', function() {
        "use strict";
        $('.navigation').toggleClass('submenu-is-active');
    });

    // NAV: abre o submenu: Adicionar classe "submenu-is-active"
    $('.navigation .main-nav .main-nav-item').hover(function() {
        $('.navigation').toggleClass('submenu-is-active');
        // $(this).find('.main-nav-submenu').slideDown({duration: 200, queue: false});
    }, function() {
        // $(this).find('.main-nav-submenu').slideUp({duration: 200, queue: false});
        $('.navigation').delay(500).toggleClass('submenu-is-active');
    });

    // MODAL LOGIN: Abre o modal ao clicar em login
    $(document).on('click', '.btn-login', function() {
        $('.hold-modal-login').fadeIn();
    });

    // MODAL LOGIN: Fecha o modal ao clicar em login
    $(document).on('click', '.btn-close-modal-login', function() {
        $('.hold-modal-login').fadeOut();
    });

    // BREADCRUMBS: calcula o tamanho da div de acordo com a quantidade de li
    $('.breadcrumbs').width(( $('.breadcrumbs li').width() + 30 ) * ( $('.breadcrumbs li').length ));

    // PRODUCT COLOR
    var productColor = $('.product-color .list li').data('color');
    $('.product-color .list li i').css('color', productColor);

    // PRODUCT HOVER: Mostra as informações de cor de tamanho quando passa o mouse em cima do thumb do produto.
    // Adicionar classe "is-hovering"
    $('.product-thumb').hover(function() {
        $(this).addClass('is-hovering');
    }, function() {
        $(this).removeClass('is-hovering');
    });


    // FILTROS
    // Seleciona a opção no filtro da sidebar, muda o bg e adiciona um botão de excluir seleção
    // Adicionar classe "checked"
    $('.sidebar-content .input-hidden input').click(function() {
        if($(this).is(':checked')) {
            $(this).parent().addClass('checked');
            $(this).parent().find('label').append('<div class="icon"><i class="fa fa-times"></i></div>');
        }
        else {
            $(this).parent().removeClass('checked');
            $(this).parent().find('label').find('.icon').remove();
        }
    });

    // Esconde os filtros ao clicar no botão OFF
    // Adicionar class "filtre-hidden"
    $('.btn-filtreOff').click(function() {
        $('#products').addClass('filtre-hidden');
    });

    // Mostra os filtros ao clicar no botão ON
    // Remover classe "filtre-hidden"
    $('.btn-filtreOn').click(function() {
        $('#products').removeClass('filtre-hidden');
    });

    // Mostra os filtros escondidos ao clicar no botão (Mobile)
    // Adicionar classe "sidebar-opened"
    $('.btn-expand-filtre').click(function() {
        console.log('click');
        $('.sidebar').toggleClass('sidebar-opened');
    });

    // RANGE de preço e desconto nos filtros (configurar de acordo com a necessidade de vocês)
    $("#range-preco").slider({
        range : "min",
        value : 37,
        min   : 1,
        max   : 700,
        slide : function(event, ui) {
            $("#amount-preco").val("R$" + ui.value);
        }
    });
    $("#amount-preco").val("R$" + $("#range-preco").slider("value"));

    $("#range-desconto").slider({
        range : "min",
        value : 37,
        min   : 1,
        max   : 700,
        slide : function(event, ui) {
            $("#amount-desconto").val(ui.value + "%");
        }
    });

    $("#amount-desconto").val($("#range-desconto").slider("value") + "%");


    // Troca o logo da loja no botão: Comprar na loja (configurar de acordo com a necessidade de vocês)
    $('.btn-comprar-loja').hover(function() {
        $(this).find('.logo-loja').html('<img src="assets/public/dist/img/logo-dafiti-white.png" alt="Logo Dafiti">');
    }, function() {
        $(this).find('.logo-loja').html('<img src="assets/public/dist/img/logo-dafiti.png" alt="Logo Dafiti">');
    });


});