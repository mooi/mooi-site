module.exports = function(grunt){
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
          development: {
            options: {
              paths: ['client/source/ui'],
              yuicompress: false
            },
            files: {
              "client/css/mooi.css": "client/source/ui/css/mooi.less" // destination file and source file
            }
          }
        },

        cssmin: {
            my_target: {
                files: [{
                    expand: true,
                    cwd: 'client/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'client/css',
                    ext: '.min.css'
                }]
            }
        },
        uglify: {
            my_target: {
              files: [{
                  expand: true,
                  cwd: 'client/source/ui/js', //cwd: 'assets/public/src/js/public',
                  src: '*.js',
                  dest: 'client/js',
                  ext: '.min.js'
              }]
            }
        },

        imagemin: {
           dist: {
              options: {
                optimizationLevel: 5,
                progressive: true
              },
              files: [{
                 expand: true,
                 cwd: 'client/source/ui/img',
                 src: ['**/*.{png,jpg,gif}'],
                 dest: 'client/img'
              }]
           }
        },
        watch: {
          scripts: {
            files: ['client/source/ui/js/*.js', 'client/components/ui/**/*.js'],
            tasks: ['uglify'],
            options: {
              debounceDelay: 250
            }
          }, 

          styles: {
            files: ['**/*.less'], // which files to watch
            tasks: ['less', 'cssmin'],
              options: {
                nospawn: true
              }
            }
        }


    });
    
    // Carregando nossos plugins
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    
    grunt.registerTask('default', ['less', 'cssmin', 'uglify','imagemin', 'watch']);
    
};